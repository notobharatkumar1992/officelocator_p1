package com.officelocator.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.officelocator.R;
import com.officelocator.model.AdvanceSearchResponseListData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdvanceSearchListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<AdvanceSearchResponseListData.list> mAdvanceSearchResponseList;

    // Global variable declaration
    private Activity mActivity;


    public AdvanceSearchListAdapter(Activity mActivity, ArrayList<AdvanceSearchResponseListData.list> advanceSearchResponseList) {
        this.mActivity = mActivity;
        mAdvanceSearchResponseList = advanceSearchResponseList;
    }

    @Override
    public int getCount() {
        return mAdvanceSearchResponseList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAdvanceSearchResponseList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.termslist_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.textViewBranchName = (TextView) convertView.findViewById(R.id.textViewBranchName);
            mViewHolder.textViewCompanyName = (TextView) convertView.findViewById(R.id.textViewCompanyName);
            mViewHolder.termsItemImageView = (ImageView) convertView.findViewById(R.id.termsItemImageView);
            mViewHolder.rl_item = (RelativeLayout) convertView.findViewById(R.id.rl_item);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.textViewBranchName.setText(mAdvanceSearchResponseList.get(position).name);
        mViewHolder.textViewCompanyName.setText(mAdvanceSearchResponseList.get(position).address + "," +
                mAdvanceSearchResponseList.get(position).address2);

        Picasso.with(mActivity)
                .load(mAdvanceSearchResponseList.get(position).logo)
                .error(R.drawable.sample_bg)
                .placeholder(R.drawable.sample_bg)
                .into(mViewHolder.termsItemImageView);

        switch (mAdvanceSearchResponseList.get(position).value) {
            case 0:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_1));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                break;
            case 1:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                break;
            case 2:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_3));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                break;
            case 3:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_4));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                break;
            case 4:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_5));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                break;
//            case 5:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_6));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                break;
        }

        return convertView;
    }

    public class ViewHolder {
        TextView textViewBranchName, textViewCompanyName;
        ImageView termsItemImageView;
        RelativeLayout rl_item;

       /* public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }*/
    }

   /* @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mStorePref = null;
        mVcrDatabase = null;
    }*/
}
