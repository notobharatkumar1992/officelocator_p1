package com.officelocator.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.officelocator.R;
import com.officelocator.model.AnnouncementListData;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class AnnouncementListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<AnnouncementListData.list> AnnouncementList;

    // Global variable declaration
    private Activity mActivity;


    public AnnouncementListAdapter(Activity mActivity, ArrayList<AnnouncementListData.list> announcementArrayList) {
        this.mActivity = mActivity;
        AnnouncementList = announcementArrayList;
    }

    @Override
    public int getCount() {
        return AnnouncementList.size();
    }

    @Override
    public Object getItem(int position) {
        return AnnouncementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.announcement_list_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.announcementNameTextView = (TextView)convertView.findViewById(R.id.announcementNameTextView);
            mViewHolder.announcementCommentTextView = (TextView)convertView.findViewById(R.id.announcementCommentTextView);
            mViewHolder.announcementCreatedTextView = (TextView)convertView.findViewById(R.id.announcementCreatedTextView);
            mViewHolder.announcementLogoImageView = (ImageView)convertView.findViewById(R.id.announcementLogoImageView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.announcementNameTextView.setText(AnnouncementList.get(position).name);
        mViewHolder.announcementCommentTextView.setText(AnnouncementList.get(position).comment);

        String formattedTime ="";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'To'HH:mm:ssZ",Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date mDateFromResponse =sdf.parse(AnnouncementList.get(position).created);
            sdf  = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale.ENGLISH);
            formattedTime = sdf.format(mDateFromResponse);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'To'hh:mm:ssZ");
        Date result;
        try {
            result = df.parse(AnnouncementList.get(position).created);
            System.out.println("date:" + result); //prints date in current locale
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            System.out.println(sdf.format(result)); //prints date in the format sdf
        }catch (ParseException e) {
            e.printStackTrace();
        }

        mViewHolder.announcementCreatedTextView.setText(formattedTime);

        Picasso.with(mActivity)
                .load(AnnouncementList.get(position).logo)
                .error(R.drawable.sample_bg)
                .placeholder(R.drawable.sample_bg)
                .into( mViewHolder.announcementLogoImageView);
        return convertView;
    }

    public class ViewHolder {
        TextView announcementNameTextView,announcementCommentTextView,announcementCreatedTextView;
        ImageView announcementLogoImageView;

    }
}
