package com.officelocator.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.officelocator.R;

import java.util.ArrayList;

import carbon.widget.TextView;

public class CountryListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<String> serviceNameList;

    // Global variable declaration
    private Activity mActivity;

    // Local Database declaration

    // Util classes declaration

    public CountryListAdapter(Activity mActivity, ArrayList<String> serviceName) {
        this.mActivity = mActivity;
        serviceNameList = serviceName;
    }

    @Override
    public int getCount() {
        return serviceNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.countey_list_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.title = (TextView) convertView.findViewById(R.id.title);

            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
//        mViewHolder.title.setPadding(AppDelegate.dpToPix(mActivity, 10), 0, 0, 0);
        mViewHolder.title.setText(serviceNameList.get(position));
        return convertView;
    }

    public class ViewHolder {
        TextView title;

       /* public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }*/
    }

   /* @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mStorePref = null;
        mVcrDatabase = null;
    }*/
}
