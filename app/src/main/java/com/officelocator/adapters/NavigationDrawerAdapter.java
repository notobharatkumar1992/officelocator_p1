package com.officelocator.adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.officelocator.R;
import com.officelocator.model.NavDrawerItem;

import java.util.Collections;
import java.util.List;

/**
 * Created by himanshu on 3/24/2016.
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    int[] p = {R.drawable.ic_dashboard,
            R.drawable.ic_user_profile,
            R.drawable.ic_favorite,
            R.drawable.ic_search,
            R.drawable.ic_setting,
            R.drawable.ic_contact_us,
            R.drawable.ic_announcement,
            R.drawable.ic_privacy_policy,
            R.drawable.ic_logout,};

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.drawerMenuItemImageView.setImageResource(p[position]);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView drawerMenuItemImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            drawerMenuItemImageView = (ImageView)itemView.findViewById(R.id.drawerMenuItemImageView);
        }
    }
}