package com.officelocator.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.model.BranchList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MapBrandListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<BranchList.list> mBranchListArrayList;

    // Global variable declaration
    private Activity mActivity;


    public MapBrandListAdapter(Activity mActivity, ArrayList<BranchList.list> advanceSearchResponseList) {
        this.mActivity = mActivity;
        mBranchListArrayList = advanceSearchResponseList;
    }

    @Override
    public int getCount() {
        return mBranchListArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mBranchListArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.termslist_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.textViewBranchName = (TextView) convertView.findViewById(R.id.textViewBranchName);
            mViewHolder.textViewCompanyName = (TextView) convertView.findViewById(R.id.textViewCompanyName);
            mViewHolder.termsItemImageView = (ImageView) convertView.findViewById(R.id.termsItemImageView);
            mViewHolder.rl_item = (RelativeLayout) convertView.findViewById(R.id.rl_item);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.textViewBranchName.setText(mBranchListArrayList.get(position).name);
        mViewHolder.textViewCompanyName.setText(mBranchListArrayList.get(position).address + "," +
                mBranchListArrayList.get(position).address2);

        AppDelegate.LogT("MapBrandListAdapter = " + mBranchListArrayList.get(position).logo);
        Picasso.with(mActivity)
                .load(mBranchListArrayList.get(position).logo)
                .error(R.drawable.sample_bg)
                .placeholder(R.drawable.sample_bg)
                .into(mViewHolder.termsItemImageView);

        switch (mBranchListArrayList.get(position).value) {
            case 0:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_1));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                break;
            case 1:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                break;
            case 2:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_3));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                break;
            case 3:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_4));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.tiles_color_2));
                break;
            case 4:
                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_5));
                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
                break;
//            case 5:
//                mViewHolder.rl_item.setBackgroundColor(mActivity.getResources().getColor(R.color.tiles_color_6));
//                mViewHolder.textViewBranchName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                mViewHolder.textViewCompanyName.setTextColor(mActivity.getResources().getColor(R.color.icons));
//                break;
        }

        return convertView;
    }

    public class ViewHolder {
        TextView textViewBranchName, textViewCompanyName;
        ImageView termsItemImageView;
        RelativeLayout rl_item;

       /* public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }*/
    }

   /* @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mStorePref = null;
        mVcrDatabase = null;
    }*/
}
