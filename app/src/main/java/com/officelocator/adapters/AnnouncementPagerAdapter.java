package com.officelocator.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.fragments.AnnouncementDetailFragment;
import com.officelocator.model.AnnouncementListData;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class AnnouncementPagerAdapter extends PagerAdapter {

    // ArrayList declaration
    ArrayList<AnnouncementListData.list> AnnouncementList;

    // Global variable declaration
    private Activity mActivity;
    LayoutInflater inflater;

    public AnnouncementPagerAdapter(Activity mActivity, ArrayList<AnnouncementListData.list> announcementArrayList) {
        this.inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mActivity = mActivity;
        AnnouncementList = announcementArrayList;
    }

    @Override
    public int getCount() {
        return AnnouncementList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        ((ViewPager) container).removeView((View)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position)
    {

        View itemView;
        itemView = inflater.inflate(R.layout.announcement_viewpager_list_row, container, false);

        TextView announcementNameTextView = (TextView) itemView.findViewById(R.id.announcementNameTextView);

        announcementNameTextView.setTag(position);
        announcementNameTextView.setText(AnnouncementList.get(position).name);

        announcementNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = Integer.parseInt(v.getTag().toString());
                Bundle bundle = new Bundle();
                bundle.putString("announcementId", AnnouncementList.get(pos).id);
                AnnouncementDetailFragment mAnnouncementDetailFragment = new AnnouncementDetailFragment();
                ((MainActivity) mActivity).loadChildFragment(mAnnouncementDetailFragment, bundle, "new_main");
            }
        });

        ((ViewPager) container).addView(itemView);

        return itemView;
    }
}
