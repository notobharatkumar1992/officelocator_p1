package com.officelocator.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.officelocator.R;
import com.officelocator.model.CategoryListData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import carbon.widget.TextView;

public class CategoryListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<CategoryListData.list> mCategoryArrayList;
    private int count;

    // Global variable declaration
    private Activity mActivity;


    public CategoryListAdapter(Activity mActivity, ArrayList<CategoryListData.list> CategoryArrayList) {
        this.mActivity = mActivity;
        mCategoryArrayList = CategoryArrayList;
    }

    @Override
    public int getCount() {
        return mCategoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCategoryArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.category_list_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.categoryTextView = (TextView) convertView.findViewById(R.id.categoryTextView);
            mViewHolder.categoryImageView = (ImageView) convertView.findViewById(R.id.categoryImageView);
            mViewHolder.shoppingContentRelativeLayout = (LinearLayout) convertView.findViewById(R.id.shoppingContentRelativeLayout);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }


        mViewHolder.categoryTextView.setText(mCategoryArrayList.get(position).name);

        Picasso.with(mActivity)
                .load(mCategoryArrayList.get(position).url)
                .error(R.drawable.ic_menu_gallery)
                .placeholder(R.drawable.ic_menu_gallery)
                .into(mViewHolder.categoryImageView);

        switch (mCategoryArrayList.get(position).value) {
            case 0:
                mViewHolder.shoppingContentRelativeLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.tiles_1));
                break;
            case 1:
                mViewHolder.shoppingContentRelativeLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.tiles_2));
                break;
            case 2:
                mViewHolder.shoppingContentRelativeLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.tiles_3));
                break;
            case 3:
                mViewHolder.shoppingContentRelativeLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.tiles_4));
                break;
            case 4:
                mViewHolder.shoppingContentRelativeLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.tiles_5));
                break;
//            case 5:
//                mViewHolder.shoppingContentRelativeLayout.setBackground(mActivity.getResources().getDrawable(R.drawable.tiles_6));
//                break;
        }

        return convertView;
    }

    public class ViewHolder {
        TextView categoryTextView;
        ImageView categoryImageView;
        LinearLayout shoppingContentRelativeLayout;

       /* public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }*/
    }

   /* @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mStorePref = null;
        mVcrDatabase = null;
    }*/
}
