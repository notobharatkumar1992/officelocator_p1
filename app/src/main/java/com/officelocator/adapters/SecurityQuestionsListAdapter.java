package com.officelocator.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.officelocator.R;

import java.util.ArrayList;

public class SecurityQuestionsListAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<String> serviceNameList;

    // Global variable declaration
    private Activity mActivity;

    // Local Database declaration

    // Util classes declaration

    public SecurityQuestionsListAdapter(Activity mActivity, ArrayList<String> serviceName) {
        this.mActivity = mActivity;
        serviceNameList = serviceName;
    }

    @Override
    public int getCount() {
        return serviceNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.security_questions_list_row, null);
            mViewHolder = new ViewHolder();
            mViewHolder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.title.setText(serviceNameList.get(position));
        return convertView;
    }

    public class ViewHolder {
        TextView title;

    }
}
