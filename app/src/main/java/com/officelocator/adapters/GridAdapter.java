package com.officelocator.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.officelocator.R;
import com.officelocator.model.GridItems;
import com.squareup.picasso.Picasso;

/**
 * Created by Dev51 on 4/9/2016.
 */
public class GridAdapter extends BaseAdapter {

    Context context;

    public class ViewHolder {
        public ImageView categoryImageView;
        public TextView categoryTextView;
    }

    private GridItems[] items;
    private LayoutInflater mInflater;

    public GridAdapter(Context context, GridItems[] locations) {

        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        items = locations;

    }

    public GridItems[] getItems() {
        return items;
    }

    public void setItems(GridItems[] items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        if (items != null) {
            return items.length;
        }
        return 0;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        if (items != null && position >= 0 && position < getCount()) {
            return items[position];
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (items != null && position >= 0 && position < getCount()) {
            return items[position].id;
        }
        return 0;
    }

    public void setItemsList(GridItems[] locations) {
        this.items = locations;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder viewHolder;

        if (view == null) {

            view = mInflater.inflate(R.layout.category_list_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.categoryImageView = (ImageView) view
                    .findViewById(R.id.categoryImageView);
            viewHolder.categoryTextView = (TextView) view
                    .findViewById(R.id.categoryTextView);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        GridItems gridItems = items[position];
        setCatImage(position, viewHolder, gridItems.title,gridItems.imageUrl);
        return view;
    }

    private void setCatImage(int pos, ViewHolder viewHolder, String catTitle, String imageUrl) {
        //viewHolder.imageView.setImageResource(images[pos]);
       /* String UrlPath = "http://www.vastradeal.com/image/"+ imageUrl;
        UrlPath = UrlPath.replaceAll(" ", "%20");
        Log.i("UrlPath",UrlPath + "==");*/
        Picasso.with(context)
                .load(imageUrl)
                .fit()
                .error(R.drawable.ic_menu_gallery)
                .placeholder(R.drawable.ic_menu_gallery)
                .into(viewHolder.categoryImageView);
        viewHolder.categoryTextView.setText(catTitle);
    }
}
