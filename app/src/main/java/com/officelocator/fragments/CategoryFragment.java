package com.officelocator.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.activity.HomeActivity;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.AnnouncementPagerAdapter;
import com.officelocator.adapters.CategoryListAdapter;
import com.officelocator.model.AnnouncementListData;
import com.officelocator.model.Category;
import com.officelocator.model.CategoryListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;
import com.officelocator.widget.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class CategoryFragment extends Fragment {

    private ImageView imageView2;
    //Control declaration
    private GridView categoryGridView;
    //private ViewPager categoryViewPager;
    private AutoScrollViewPager announcementViewPager;
    private ProgressDialog mProgressDialog;
    private CirclePageIndicator pagerIndicator;
    //private PagerAdapter pm;
    private AnnouncementPagerAdapter mAnnouncementPagerAdapter;

    //ArrayList declaration
    ArrayList<Category> codeCategory;
    ArrayList<CategoryListData.list> categoryArrayList;
    ArrayList<AnnouncementListData.list> AnnouncementArrayList;

    //Global variable declaration
    private String categoryId;

    Location location;
    private LocationManager locationManager;
    private static final int REQUEST_CODE_LOCATION = 2;

    private TextView txt_c_createOneTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_search, container, false);
        AppDelegate.LogT("CategoryFragment called");
        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.category_title));
        ((MainActivity) getActivity()).inVisibleBackButton();

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        imageView2 = (ImageView) rootView.findViewById(R.id.imageView2);
        txt_c_createOneTextView = (TextView) rootView.findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        announcementViewPager = (AutoScrollViewPager) rootView.findViewById(R.id.announcementViewPager);
        categoryGridView = (GridView) rootView.findViewById(R.id.categoryGridView);
        pagerIndicator = (CirclePageIndicator) rootView.findViewById(R.id.pagerIndicator);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            // Get Category API call
            getCategoriesAPICall();
        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // Request missing location permission.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
            } else {
                // Location permission has been granted, continue as usual.
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        String latitude = String.valueOf(location.getLatitude());
                        String longitude = String.valueOf(location.getLongitude());
                        StorePreferences.getInstance().setLatitude(latitude);
                        StorePreferences.getInstance().setLongitude(longitude);
                    }
                } else {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        String latitude = String.valueOf(location.getLatitude());
                        String longitude = String.valueOf(location.getLongitude());
                        StorePreferences.getInstance().setLatitude(latitude);
                        StorePreferences.getInstance().setLongitude(longitude);
                    }
                }
            }
//            StorePreferences.getInstance().setLatitude("19.36889400");
//            StorePreferences.getInstance().setLongitude("-99.1803240000000000");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    private void getCategoriesAPICall() {
        mProgressDialog.show();
        SingletonRestClient.get().getAllCategories(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(retrofit.client.Response response,
                                        retrofit.client.Response response2) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            Boolean responseStatus = obj_json.getBoolean("Status");
                            if (responseStatus) {

                                categoryArrayList = new ArrayList<CategoryListData.list>();
                                GsonBuilder builder = new GsonBuilder();
                                Gson gson = builder.create();

                                CategoryListData mCategoryListData = gson.fromJson(String.valueOf(
                                        new String(((TypedByteArray) response.getBody()).getBytes())), CategoryListData.class);

                                categoryArrayList = mCategoryListData.list;
                                int count = 0;
                                for (int i = 0; i < categoryArrayList.size(); i++) {
                                    categoryArrayList.get(i).value = count;
                                    if (count == AppDelegate.TILES_COLOR_COUNT) {
                                        count = 0;
                                    } else {
                                        count++;
                                    }
                                }

                                // Creating adapter for spinner
                                CategoryListAdapter categoryListAdapter = new CategoryListAdapter(getActivity(), categoryArrayList);

                                // attaching data adapter to spinner
                                categoryGridView.setAdapter(categoryListAdapter);

                                categoryGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        categoryId = categoryArrayList.get(position).id;
                                        Bundle bundle = new Bundle();
                                        bundle.putString("categoryId", categoryId);
                                        bundle.putInt("color", categoryArrayList.get(position).value);
                                        AppDelegate.LogT("Category Fragment to HomeActivity = " + categoryArrayList.get(position).value);
                                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                                        intent.putExtras(bundle);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                                                new Pair<>(((MainActivity) getActivity()).img_topbar, getString(R.string.top_bar)),
                                                new Pair<>(imageView2, getString(R.string.map_with_logo)));
                                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                                        startActivity(intent, transitionActivityOptions.toBundle());
                                    }
                                });

                                AppDelegate.setGridViewHeight(getActivity(), categoryGridView, categoryListAdapter, 108);

                            } else {

                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                }
        );
    }

    private void getAnnouncementApiCall() {
        SingletonRestClient.get().getAllAnnouncement(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(Response response, Response response2) {
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {
                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {

                            AnnouncementArrayList = new ArrayList<AnnouncementListData.list>();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            AnnouncementListData mAnnouncementListData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), AnnouncementListData.class);

                            AnnouncementArrayList = mAnnouncementListData.list;

                            Log.i("AnnouncementArrayList", AnnouncementArrayList.size() + "==");
                            try {
                                mAnnouncementPagerAdapter = new AnnouncementPagerAdapter(getActivity(), AnnouncementArrayList);
                                announcementViewPager.setAdapter(mAnnouncementPagerAdapter);
                                pagerIndicator.setViewPager(announcementViewPager);
                                announcementViewPager.startAutoScroll(15000);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // success!
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        String latitude = String.valueOf(location.getLatitude());
                        String longitude = String.valueOf(location.getLongitude());
                        StorePreferences.getInstance().setLatitude(latitude);
                        StorePreferences.getInstance().setLongitude(longitude);
                    } else {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            String latitude = String.valueOf(location.getLatitude());
                            String longitude = String.valueOf(location.getLongitude());
                            StorePreferences.getInstance().setLatitude(latitude);
                            StorePreferences.getInstance().setLongitude(longitude);
                        }
                    }
                } else {
                    // Permission was denied or request was cancelled
                    AppDelegate.LogE("Permission was denied or request was cancelled");
                }
            }
        }
    }
}
