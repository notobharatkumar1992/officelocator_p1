package com.officelocator.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.AnnouncementListAdapter;
import com.officelocator.model.AnnouncementListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class AnnouncementFragment extends Fragment {

    private ListView announcementListView;
    private TextView announcementErrorMessageTextView;
    private ProgressDialog mProgressDialog;

    ArrayList<AnnouncementListData.list> AnnouncementArrayList;
    AnnouncementListAdapter mAnnouncementListAdapter;

    //Global variable declaration

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_terms, container, false);

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_announcement));
        ((MainActivity) getActivity()).visibleBackButton();

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        announcementListView = (ListView) rootView.findViewById(R.id.termsListView);
        announcementErrorMessageTextView = (TextView) rootView.findViewById(R.id.termsErrorMessageTextView);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            getAnnouncementApiCall();
        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }

        return rootView;
    }

    private void getAnnouncementApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().getAllAnnouncement(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {
                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {
                            announcementListView.setVisibility(View.VISIBLE);
                            announcementErrorMessageTextView.setVisibility(View.GONE);
                            AnnouncementArrayList = new ArrayList<AnnouncementListData.list>();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            AnnouncementListData mAnnouncementListData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), AnnouncementListData.class);

                            AnnouncementArrayList = mAnnouncementListData.list;

                            Log.i("AnnouncementArrayList", AnnouncementArrayList.size() + "==");

                            mAnnouncementListAdapter = new AnnouncementListAdapter(getActivity(), AnnouncementArrayList);
                            announcementListView.setAdapter(mAnnouncementListAdapter);
                            announcementListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("announcementId", AnnouncementArrayList.get(position).id);
                                    AnnouncementDetailFragment mAnnouncementDetailFragment = new AnnouncementDetailFragment();
                                    ((MainActivity) getActivity()).loadChildFragment(mAnnouncementDetailFragment, bundle, "new_main");
                                }
                            });
                        }

                    } else {
                        String responseMessage = obj_json.getString("Message");
                        announcementListView.setVisibility(View.GONE);
                        announcementErrorMessageTextView.setVisibility(View.VISIBLE);
                        announcementErrorMessageTextView.setText(responseMessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }
}
