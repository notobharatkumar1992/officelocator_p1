package com.officelocator.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.activity.BranchDetailsActivity;
import com.officelocator.activity.HomeActivity;
import com.officelocator.model.FavoriteListData;
import com.officelocator.model.Person;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.MultiDrawable;
import com.officelocator.util.StorePreferences;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dev51 on 3/17/2015.
 */
public class FavouiteMapFragment extends com.google.android.gms.maps.SupportMapFragment implements ClusterManager.OnClusterClickListener<Person>, ClusterManager.OnClusterInfoWindowClickListener<Person>, ClusterManager.OnClusterItemClickListener<Person>, ClusterManager.OnClusterItemInfoWindowClickListener<Person> {

    private String latitude;
    private String longitude;
    GoogleMap googleMap;
    private Marker customMarker;
    private ProgressDialog mProgressDialog;

    LocationManager locationManager;
    Location location; // location

    //ArrayList declaration
    ArrayList<FavoriteListData.list> favoriteArrayList;

    private static final int REQUEST_CODE_LOCATION = 2;

    private ClusterManager<Person> mClusterManager;

    public FavouiteMapFragment() {
    }

    @SuppressLint("ValidFragment")
    public FavouiteMapFragment(ArrayList<FavoriteListData.list> favoriteArrayList) {
        this.favoriteArrayList = favoriteArrayList;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                FavouiteMapFragment.this.googleMap = googleMap;
                AppDelegate.LogT("Google Map ==" + FavouiteMapFragment.this.googleMap);
                initView();
            }
        });

    }

    private void initView() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Request missing location permission.
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION);
        } else {
            // get current location code
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                buildAlertMessageGps();
            } else {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    StorePreferences.getInstance().setLatitude(latitude);
                    StorePreferences.getInstance().setLongitude(longitude);
                }
                //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, obj_map_location_listner);

                LatLng myLocation = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                //CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 18);
                try {
                    // googleMap = getMap();
                    googleMap.getUiSettings().setZoomControlsEnabled(true);
                    googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getString(R.string.map_user_pin_title)));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                    if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                        getBranchByCategoryAPiCall();
                    } else {
                        ConnectionDetector.getInstance().show_alert(getActivity());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void buildAlertMessageGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getResources().getString(R.string.gps_disable_alert_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_disable_alert_yes_button_title), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.gps_disable_alert_no_button_title), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private Drawable userImg;

    private void getBranchByCategoryAPiCall() {

        mClusterManager = new ClusterManager<Person>(getActivity(), googleMap);
        mClusterManager.setRenderer(new PersonRenderer(googleMap));
        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnMarkerClickListener(mClusterManager);
        googleMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(FavouiteMapFragment.this);
        mClusterManager.setOnClusterInfoWindowClickListener(FavouiteMapFragment.this);
        mClusterManager.setOnClusterItemClickListener(FavouiteMapFragment.this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(FavouiteMapFragment.this);

        for (int i = 0; i < favoriteArrayList.size(); i++) {
            try {
                double lat = Double.parseDouble(favoriteArrayList.get(i).latitude);
                double lon = Double.parseDouble(favoriteArrayList.get(i).longitude);
                LatLng latlong = new LatLng(lat, lon);
                Picasso.with(getActivity())
                        .load(favoriteArrayList.get(i).logo)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                if (isAdded())
                                    userImg = new BitmapDrawable(getResources(), bitmap);

                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                if (isAdded())
                                    userImg = getActivity().getResources().getDrawable(R.drawable.sample_bg);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                if (isAdded())
                                    userImg = getActivity().getResources().getDrawable(R.drawable.sample_bg);
                            }
                        });
                mClusterManager.addItem(new Person(latlong, favoriteArrayList.get(i).id, favoriteArrayList.get(i).name, userImg, favoriteArrayList.get(i).value));
                AppDelegate.LogT("Image Path" + favoriteArrayList.get(i).logo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mClusterManager.cluster();
        AppDelegate.LogT("mClusterManager are=" + mClusterManager);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    //code here

                    // get current location code
                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        buildAlertMessageGps();
                    } else {
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            return;
                        }
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = String.valueOf(location.getLatitude());
                            longitude = String.valueOf(location.getLongitude());
                            StorePreferences.getInstance().setLatitude(latitude);
                            StorePreferences.getInstance().setLongitude(longitude);
                        }
                        //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, obj_map_location_listner);

                        getMapAsync(new OnMapReadyCallback() {
                            @Override
                            public void onMapReady(GoogleMap googleMap) {
                                FavouiteMapFragment.this.googleMap = googleMap;
                                AppDelegate.LogT("Google Map ==" + FavouiteMapFragment.this.googleMap);
                                checkpermission();
                            }
                        });
                        //CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 18);

                    }
                }
            } else {
                // Permission was denied. Display an error message.
            }
        }
    }

    private void checkpermission() {
        LatLng myLocation = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
        try {
            //((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

            //  googleMap = getMap();
//            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getString(R.string.map_user_pin_title)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                getBranchByCategoryAPiCall();
            } else {
                ConnectionDetector.getInstance().show_alert(getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    /*=============================show dialog map pin=================================*/
    private void show_pin_dialog() {
        // TODO Auto-generated method stub
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // TODO Auto-generated method stub
                View v = getActivity().getLayoutInflater().inflate(R.layout.map_pin_showinfo_row, null);
                TextView BranchId = (TextView) v.findViewById(R.id.tagIdTextView);
                BranchId.setText(marker.getTitle());
                TextView BranchName = (TextView) v.findViewById(R.id.tagBranchNameTextView);
                BranchName.setText(marker.getSnippet());
                return v;
            }
        });
    }


    @Override
    public boolean onClusterClick(Cluster<Person> cluster) {
        String firstName = cluster.getItems().iterator().next().name;
        //  Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Person> cluster) {
        if (item == null)
            return;
        String id = item.id;
        for (int i = 0; i < favoriteArrayList.size(); i++) {
            if (favoriteArrayList.get(i).id.equals(id)) {
                Bundle bundle = new Bundle();
                bundle.putString("Id", favoriteArrayList.get(i).id);
                bundle.putString("BranchId", favoriteArrayList.get(i).category_id);
                bundle.putInt("color", favoriteArrayList.get(i).value);

                Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                        new Pair<>(((HomeActivity) getActivity()).findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                intent.putExtras(bundle);
                startActivity(intent, transitionActivityOptions.toBundle());
            }
        }
    }

    public Person item;

    @Override
    public boolean onClusterItemClick(Person item) {
        this.item = item;
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Person item) {
        AppDelegate.LogT("onClusterItemInfoWindowClick => ");
        if (item != null) {
            String id = item.id;
            for (int i = 0; i < favoriteArrayList.size(); i++) {
                if (favoriteArrayList.get(i).id.equals(id)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("Id", favoriteArrayList.get(i).id);
                    bundle.putString("BranchId", favoriteArrayList.get(i).category_id);
                    bundle.putInt("color", favoriteArrayList.get(i).value);

                    Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(getActivity(), false,
                            new Pair<>(((HomeActivity) getActivity()).findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);
                    intent.putExtras(bundle);
                    startActivity(intent, transitionActivityOptions.toBundle());
                }
            }
        }
    }

    public class PersonRenderer extends DefaultClusterRenderer<Person> {
        private final IconGenerator mIconGenerator = new IconGenerator(getActivity());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getActivity());
        private final ImageView mClusterImageView;
        private final ImageView img_marker;
        private final int mDimension;

        public PersonRenderer(GoogleMap googleMap) {
            super(getActivity(), googleMap, mClusterManager);

            View multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
            View multiProfileone = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfileone);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);
            img_marker = (ImageView) multiProfile.findViewById(R.id.img_marker);

            // mImageView = new ImageView(getActivity());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            //mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            // mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(multiProfile);
        }

        @Override
        protected void onBeforeClusterItemRendered(Person person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
            mClusterImageView.setImageDrawable(person.profilePhoto);
            switch (person.value) {
                case 0:
                    img_marker.setImageResource(R.drawable.marker_1);
                    break;
                case 1:
                    img_marker.setImageResource(R.drawable.marker_2);
                    break;
                case 2:
                    img_marker.setImageResource(R.drawable.marker_3);
                    break;
                case 3:
                    img_marker.setImageResource(R.drawable.marker_4);
                    break;
                case 4:
                    img_marker.setImageResource(R.drawable.marker_5);
                    break;
                default:
                    img_marker.setImageResource(R.drawable.marker_1);
                    break;
            }
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(person.name);
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Person> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;

            for (Person p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;
                /*Drawable drawable =p.profilePhoto;
                drawable.setBounds(0, 0, width, height);
                profilePhotos.add(drawable);*/
                mClusterImageView.setImageDrawable(p.profilePhoto);
            }
            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
            multiDrawable.setBounds(0, 0, width, height);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

}
