package com.officelocator.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.SearchCategoryListAdapter;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class SearchFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Control declaration
    private EditText postalCodeEditText;
    private EditText productNameEditText;
    private Button searchButton;
    private Spinner categorySpinner;
    private ProgressDialog mProgressDialog;

    //ArrayList declaration
    ArrayList<String> categoryArrayList;
    ArrayList<String> categoryIdArrayList;

    //Global variable declaration
    private String categoryId;
    private String postalCode = "";
    private String productName = "";

    private OnFragmentInteractionListener mListener;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_search));
        ((MainActivity) getActivity()).inVisibleBackButton();

        searchButton = (Button) rootView.findViewById(R.id.searchButton);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        categorySpinner = (Spinner) rootView.findViewById(R.id.categorySpinner);
        postalCodeEditText = (EditText) rootView.findViewById(R.id.postalCodeEditText);
        productNameEditText = (EditText) rootView.findViewById(R.id.productNameEditText);

        searchButton.setOnClickListener(this);

        // Get Category API call
        getCategoriesAPICall();

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == searchButton) {
            if (localValidation()) {
                Bundle bundle = new Bundle();
                bundle.putString("categoryId", categoryId);
                bundle.putString("postalCode", postalCode);
                bundle.putString("productName", productName);
                TermsFragment mTermsFragment = new TermsFragment();
                ((MainActivity) getActivity()).loadChildFragment(mTermsFragment, bundle, "new_main");
            }
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void getCategoriesAPICall() {
        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            mProgressDialog.show();
            SingletonRestClient.get().getAllCategories(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                    mProgressDialog.dismiss();
                }

                @Override
                public void success(retrofit.client.Response response,
                                    retrofit.client.Response response2) {
                    mProgressDialog.dismiss();

                    try {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        Boolean responseStatus = obj_json.getBoolean("Status");
                        if (responseStatus) {
                            //String responseMessage = obj_json.getString("responseMessage");
                            JSONArray obJsonArray = obj_json.getJSONArray("list");

                            if (obJsonArray.length() > 0) {
                                categoryArrayList = new ArrayList<String>();
                                categoryIdArrayList = new ArrayList<String>();
                                for (int i = 0; i < obJsonArray.length(); i++) {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    categoryArrayList.add(jsonObject.getString("name"));
                                    categoryIdArrayList.add(jsonObject.getString("id"));
                                }
                            }

                            // Creating adapter for spinner
                            SearchCategoryListAdapter categoryListAdapter = new SearchCategoryListAdapter(getActivity(), categoryArrayList);
                            // attaching data adapter to spinner
                            categorySpinner.setAdapter(categoryListAdapter);

                            categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    categoryId = categoryIdArrayList.get(position);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else {

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });
        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }
    }

    /**
     * Local validation check for the isEmpty
     *
     * @return true/false according to the validations
     */
    private Boolean localValidation() {

        postalCode = postalCodeEditText.getText().toString().trim();
        productName = productNameEditText.getText().toString().trim();
        Boolean response = true;

       /* if (postalCode.equals("")) {
            postalCodeEditText.setError("Please Enter Postal Code");
            postalCodeEditText.requestFocus();
            response = false;
        }else {
            productNameEditText.setError(null);
            postalCodeEditText.setError(null);
            response = true;
        }*/
        return response;
    }
}
