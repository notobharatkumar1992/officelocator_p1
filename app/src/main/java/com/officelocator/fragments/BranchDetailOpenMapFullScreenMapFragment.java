package com.officelocator.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.officelocator.R;
import com.officelocator.util.CircleImageView;
import com.officelocator.util.StorePreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Dev51 on 3/17/2015.
 */
public class BranchDetailOpenMapFullScreenMapFragment extends com.google.android.gms.maps.SupportMapFragment  {

    private String currentLatitude;
    private String currentLongitude;
    private String destinationLatitude;
    private String destinationLongitude;
    private String pinLogo;
    GoogleMap googleMap;
    private Marker customMarker;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        currentLatitude = StorePreferences.getInstance().getLatitude();
        currentLongitude = StorePreferences.getInstance().getLongitude();

        destinationLatitude = getArguments().getString("destinationLatitude");
        destinationLongitude = getArguments().getString("destinationLongitude");
        pinLogo = getArguments().getString("pinLogo");
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                BranchDetailOpenMapFullScreenMapFragment.this.googleMap = googleMap;
                initView();
            }
        });

    }

    private void initView() {

        LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
        try {

            if (getActivity() != null) {
                // ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

                // googleMap = getMap();
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getActivity().getResources().getString(R.string.map_user_pin_title)));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

                LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));

                View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
                CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);

                Picasso.with(getActivity())
                        .load(pinLogo)
                        .error(R.drawable.sample_bg)
                        .placeholder(R.drawable.sample_bg)
                        .into(categoryImageView);

                // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
                customMarker = googleMap.addMarker(new MarkerOptions()
                        .position(destinationLocation)
                        .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));

                // m.showInfoWindow();

                PolylineOptions polylineOptions = new PolylineOptions();

                ArrayList<LatLng> points = new ArrayList<LatLng>();

                // Setting the color of the polyline
                polylineOptions.color(Color.RED);

                // Setting the width of the polyline
                polylineOptions.width(5);

                // Adding the taped point to the ArrayList
                points.add(myLocation);
                points.add(destinationLocation);

                // Setting points of polyline
                polylineOptions.addAll(points);

                // Adding the polyline to the map
                googleMap.addPolyline(polylineOptions);
            }

            final Handler handler = new Handler();
            final Runnable r = new Runnable() {
                public void run() {

                    if (getActivity() != null) {
                        googleMap.clear();
                        googleMap.getUiSettings().setZoomControlsEnabled(true);
                        LatLng myLocation = new LatLng(Double.valueOf(currentLatitude), Double.valueOf(currentLongitude));
                        googleMap.addMarker(new MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.userpin)).title(getActivity().getResources().getString(R.string.map_user_pin_title)));

                        LatLng destinationLocation = new LatLng(Double.valueOf(destinationLatitude), Double.valueOf(destinationLongitude));

                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout_demo, null);
                        CircleImageView categoryImageView = (CircleImageView) marker.findViewById(R.id.categoryRoundedImageView);

                        Picasso.with(getActivity())
                                .load(pinLogo)
                                .error(R.drawable.sample_bg)
                                .placeholder(R.drawable.sample_bg)
                                .into(categoryImageView);

                        // Bitmap bmp = getBitmapFromURL(BranchArrayList.get(i).logo);
                        customMarker = googleMap.addMarker(new MarkerOptions()
                                .position(destinationLocation)
                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker))));
                        // m.showInfoWindow();
                        PolylineOptions polylineOptions = new PolylineOptions();
                        ArrayList<LatLng> points = new ArrayList<LatLng>();
                        // Setting the color of the polyline
                        polylineOptions.color(Color.RED);
                        // Setting the width of the polyline
                        polylineOptions.width(5);
                        // Adding the taped point to the ArrayList
                        points.add(myLocation);
                        points.add(destinationLocation);
                        // Setting points of polyline
                        polylineOptions.addAll(points);
                        // Adding the polyline to the map
                        googleMap.addPolyline(polylineOptions);
                        //handler.postDelayed(this, 10000);
                    }
                }
            };
            handler.postDelayed(r, 2500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }
}
