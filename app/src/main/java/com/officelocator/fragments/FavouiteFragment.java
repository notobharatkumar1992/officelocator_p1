package com.officelocator.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.activity.BranchDetailsActivity;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.FavoriteListAdapter;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.model.FavoriteListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FavouiteFragment.OnFragmentInteractionListener} Interfaces
 * to handle interaction events.
 * Use the {@link FavouiteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavouiteFragment extends Fragment implements OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ProgressDialog mProgressDialog;

    //ArrayList declaration
    ArrayList<FavoriteListData.list> FavoriteArrayList = new ArrayList<>();

    private ListView favoriteListView;
    private TextView favoriteErrorMessageTextView;

    FavoriteListAdapter mFavoriteListAdapter;

    private OnFragmentInteractionListener mListener;

    private FrameLayout map_container;
    private LinearLayout mapDisplayLinearLayout;
    private LinearLayout listDisplayLinearLayout;
    private TextView mapLabelTextView;
    private TextView listLabelTextView;

    public FavouiteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavouiteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavouiteFragment newInstance(String param1, String param2) {
        FavouiteFragment fragment = new FavouiteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favouite, container, false);

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_my_favorite));
        ((MainActivity) getActivity()).visibleBackButton();

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        favoriteListView = (ListView) rootView.findViewById(R.id.favoriteListView);
        favoriteErrorMessageTextView = (TextView) rootView.findViewById(R.id.favoriteErrorMessageTextView);
        favoriteErrorMessageTextView.setVisibility(View.GONE);
        map_container = (FrameLayout) rootView.findViewById(R.id.map_container);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            getFavoriteListApiCall();
        } else {
            getFavoriteFromOffline();
        }

        mapDisplayLinearLayout = (LinearLayout) rootView.findViewById(R.id.mapDisplayLinearLayout);
        listDisplayLinearLayout = (LinearLayout) rootView.findViewById(R.id.listDisplayLinearLayout);
        mapLabelTextView = (TextView) rootView.findViewById(R.id.mapLabelTextView);
        listLabelTextView = (TextView) rootView.findViewById(R.id.listLabelTextView);
        mapDisplayLinearLayout.setOnClickListener(this);
        listDisplayLinearLayout.setOnClickListener(this);

        return rootView;
    }

    private void addMarkers() {
        try {
            FavouiteMapFragment mapFragment = new FavouiteMapFragment(FavoriteArrayList);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.map_container, mapFragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFavoriteListApiCall() {

        mProgressDialog.show();
        SingletonRestClient.get().getFavoriteList(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(retrofit.client.Response response,
                                retrofit.client.Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        if (OfficeLocatorDatabase.getInstance(getActivity()).IsFavoriteListExist()) {
                            OfficeLocatorDatabase.getInstance(getActivity()).deleteAllFavoriteList();
                            OfficeLocatorDatabase.getInstance(getActivity()).insertFavoriteList(obj_json.toString());
                        } else {
                            OfficeLocatorDatabase.getInstance(getActivity()).insertFavoriteList(obj_json.toString());
                        }


                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {
                            favoriteListView.setVisibility(View.VISIBLE);
                            favoriteErrorMessageTextView.setVisibility(View.GONE);
                            FavoriteArrayList = new ArrayList<FavoriteListData.list>();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            FavoriteListData mFavoriteListData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), FavoriteListData.class);

                            FavoriteArrayList = mFavoriteListData.list;

                            int count = 0;
                            for (int i = 0; i < FavoriteArrayList.size(); i++) {
                                FavoriteArrayList.get(i).value = count;
                                if (count == AppDelegate.TILES_COLOR_COUNT) {
                                    count = 0;
                                } else {
                                    count++;
                                }
                            }

                            Log.i("FavoriteListData", FavoriteArrayList.size() + "==");

                            mFavoriteListAdapter = new FavoriteListAdapter(getActivity(), FavoriteArrayList);
                            favoriteListView.setAdapter(mFavoriteListAdapter);
                            favoriteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("Id", FavoriteArrayList.get(position).id);
                                    bundle.putString("BranchId", FavoriteArrayList.get(position).category_id);
                                    Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            });
                        }

                    } else {
                        String responseMessage = obj_json.getString("Message");
                        favoriteListView.setVisibility(View.GONE);
                        favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
                        favoriteErrorMessageTextView.setText(responseMessage);
                    }
                    addMarkers();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == mapDisplayLinearLayout) {

            map_container.setVisibility(View.VISIBLE);
            favoriteListView.setVisibility(View.GONE);
            favoriteErrorMessageTextView.setVisibility(View.GONE);
            mapDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
            listDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.white));

            mapLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.white));
            listLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));

        } else if (v == listDisplayLinearLayout) {

            favoriteListView.setVisibility(View.VISIBLE);
            map_container.setVisibility(View.GONE);

            mapDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            listDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));

            mapLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            listLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.white));

            if (FavoriteArrayList.size() > 0) {

            } else {
                if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                    getFavoriteListApiCall();
                } else {
                    ConnectionDetector.getInstance().show_alert(getActivity());
                }
            }
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void getFavoriteFromOffline() {
        if (OfficeLocatorDatabase.getInstance(getActivity()).IsFavoriteListExist()) {
            String response = OfficeLocatorDatabase.getInstance(getActivity()).GetAllFavoriteList();
            Log.i("test", "local db response get favorite " + response + "=");
            try {
                JSONObject jsonObject = new JSONObject(response);

                String favoriteList = jsonObject.getString("favoriteList");
                JSONObject obj_json = new JSONObject(favoriteList);
                String responseStatus = obj_json.getString("Status");
                if (responseStatus.equals("true")) {

                    JSONArray list = new JSONArray(obj_json.getString("list"));

                    if (list.length() > 0) {
                        favoriteListView.setVisibility(View.VISIBLE);
                        favoriteErrorMessageTextView.setVisibility(View.GONE);
                        FavoriteArrayList = new ArrayList<FavoriteListData.list>();
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();

                        FavoriteListData mFavoriteListData = gson.fromJson(favoriteList, FavoriteListData.class);

                        FavoriteArrayList = mFavoriteListData.list;

                        int count = 0;
                        for (int i = 0; i < FavoriteArrayList.size(); i++) {
                            FavoriteArrayList.get(i).value = count;
                            if (count == AppDelegate.TILES_COLOR_COUNT) {
                                count = 0;
                            } else {
                                count++;
                            }
                        }

                        Log.i("FavoriteListData", FavoriteArrayList.size() + "==");

                        mFavoriteListAdapter = new FavoriteListAdapter(getActivity(), FavoriteArrayList);
                        favoriteListView.setAdapter(mFavoriteListAdapter);
                        favoriteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Bundle bundle = new Bundle();
                                bundle.putString("Id", FavoriteArrayList.get(position).id);
                                bundle.putString("BranchId", FavoriteArrayList.get(position).category_id);
                                Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                    }

                } else {
                    String responseMessage = obj_json.getString("Message");
                    favoriteListView.setVisibility(View.GONE);
                    favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
                    favoriteErrorMessageTextView.setText(responseMessage);
                }
                addMarkers();

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //AlertDialog("Server Error! Please try again.");
            }
        } else {
            favoriteListView.setVisibility(View.GONE);
            favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
            favoriteErrorMessageTextView.setText(getResources().getString(R.string.detail_branch_no_data_message));
        }
    }
}
