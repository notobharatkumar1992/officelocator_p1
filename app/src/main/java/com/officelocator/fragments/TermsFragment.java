package com.officelocator.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.R;
import com.officelocator.activity.BranchDetailsActivity;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.AdvanceSearchListAdapter;
import com.officelocator.model.AdvanceSearchResponseListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class TermsFragment extends Fragment {

    private ListView termsListView;
    private TextView termsErrorMessageTextView;
    private ProgressDialog mProgressDialog;

    ArrayList<AdvanceSearchResponseListData.list> AdvanceSearchArrayList;
    AdvanceSearchListAdapter mAdvanceSearchListAdapter;

    //Global variable declaration
    private String categoryId;
    private String postalCode;
    private String productName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_terms, container, false);

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.advance_search_title));
        ((MainActivity) getActivity()).visibleBackButton();

        categoryId = getArguments().getString("categoryId");
        postalCode = getArguments().getString("postalCode");
        productName = getArguments().getString("productName");

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        termsListView = (ListView) rootView.findViewById(R.id.termsListView);
        termsErrorMessageTextView = (TextView) rootView.findViewById(R.id.termsErrorMessageTextView);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            advanceSearchApiCall();
        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }

        return rootView;
    }

    private void advanceSearchApiCall() {
        mProgressDialog.show();
        String radius = "";
        if (!StorePreferences.getInstance().getRadius().equals("")) {
            radius = StorePreferences.getInstance().getRadius();
        } else {
            radius = "20";
        }
        SingletonRestClient.get().getAdvanceSearch(postalCode, radius,
                categoryId, productName, StorePreferences.getInstance().getLatitude(),
                StorePreferences.getInstance().getLongitude(), StorePreferences.getInstance().get_UseId(),
                StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(retrofit.client.Response response,
                                        retrofit.client.Response response2) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("Status");
                            if (responseStatus.equals("true")) {
                                JSONArray list = new JSONArray(obj_json.getString("list"));

                                if (list.length() > 0) {
                                    termsListView.setVisibility(View.VISIBLE);
                                    termsErrorMessageTextView.setVisibility(View.GONE);
                                    AdvanceSearchArrayList = new ArrayList<AdvanceSearchResponseListData.list>();
                                    GsonBuilder builder = new GsonBuilder();
                                    Gson gson = builder.create();

                                    AdvanceSearchResponseListData mAdvanceSearchResponseListData = gson.fromJson(String.valueOf(
                                            new String(((TypedByteArray) response.getBody()).getBytes())), AdvanceSearchResponseListData.class);

                                    AdvanceSearchArrayList = mAdvanceSearchResponseListData.list;

                                    Log.i("FavoriteListData", AdvanceSearchArrayList.size() + "==");

                                    mAdvanceSearchListAdapter = new AdvanceSearchListAdapter(getActivity(), AdvanceSearchArrayList);
                                    termsListView.setAdapter(mAdvanceSearchListAdapter);
                                    termsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("Id", AdvanceSearchArrayList.get(position).id);
                                            bundle.putString("BranchId", AdvanceSearchArrayList.get(position).category_id);
                                            Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                                            intent.putExtras(bundle);
                                            startActivity(intent);
                                        }
                                    });
                                }

                            } else {
                                String responseMessage = obj_json.getString("Message");
                                termsListView.setVisibility(View.GONE);
                                termsErrorMessageTextView.setVisibility(View.VISIBLE);
                                termsErrorMessageTextView.setText(responseMessage);
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });
    }
}
