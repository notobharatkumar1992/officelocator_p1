package com.officelocator.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.AnnouncementListAdapter;
import com.officelocator.model.AnnouncementListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class AnnouncementDetailFragment extends Fragment {

    private TextView announcementTitleTextView;
    private TextView announcementCommentTextView;
    private ProgressDialog mProgressDialog;

    //Global variable declaration
    private String announcementId = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_announcement_detail, container, false);

        ((MainActivity)getActivity()).setActivityName(getResources().getString(R.string.title_announcement_detail));
        ((MainActivity)getActivity()).visibleBackButton();

        announcementId = getArguments().getString("announcementId");

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        announcementTitleTextView = (TextView)rootView.findViewById(R.id.announcementTitleTextView);
        announcementCommentTextView = (TextView)rootView.findViewById(R.id.announcementCommentTextView);

        if(ConnectionDetector.getInstance().isConnectingToInternet()){
            getAnnouncementDetailsApiCall();
        }else{
            ConnectionDetector.getInstance().show_alert(getActivity());
        }

        return rootView;
    }

    private void getAnnouncementDetailsApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().getAnnouncementDetail(announcementId, StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        JSONObject output = obj_json.getJSONObject("output");
                        if (output.getString("name") != null) {
                            announcementTitleTextView.setText(output.getString("name"));
                        }
                        if (output.getString("comment") != null) {
                            announcementCommentTextView.setText(output.getString("comment"));
                        }

                    } else {

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }
}
