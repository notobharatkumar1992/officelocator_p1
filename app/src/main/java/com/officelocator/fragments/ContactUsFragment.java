package com.officelocator.fragments;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import static android.Manifest.permission.READ_CONTACTS;

public class ContactUsFragment extends Fragment implements  View.OnClickListener/*,LoaderManager.LoaderCallbacks<Cursor> */{

   private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    private EditText emailEditText;
    private EditText subjectEditText;
    private EditText commentEditText;
    private Button contactSubmitButton;
    private ProgressDialog mProgressDialog;

    // Global variable declaration
    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // for email validation
    public Pattern pattern;
    public Matcher matcher;

    private String email;
    private String subject;
    private String comment;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_contact_us));
        ((MainActivity)getActivity()).visibleBackButton();
        // Inflate the layout for this fragment

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        emailEditText = (EditText)rootView.findViewById(R.id.emailEditText);
        subjectEditText = (EditText)rootView.findViewById(R.id.subjectEditText);
        commentEditText = (EditText)rootView.findViewById(R.id.commentEditText);
        contactSubmitButton = (Button)rootView.findViewById(R.id.contactSubmitButton);

        contactSubmitButton.setOnClickListener(this);

        if(!StorePreferences.getInstance().getEmailId().equals("")){
            emailEditText.setText(StorePreferences.getInstance().getEmailId());
        }

        //populateAutoComplete();

        return rootView;
    }

 /*   // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
       // mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v == contactSubmitButton){
            if(localValidation()){
                if(isValidEmail()){
                    if(ConnectionDetector.getInstance().isConnectingToInternet()) {
                        SendContactUsAPICall();
                    }else {
                        ConnectionDetector.getInstance().show_alert(getActivity());
                    }
                }
            }
        }
    }

    private void SendContactUsAPICall() {
        mProgressDialog.show();
        SingletonRestClient.get().sendContactUs(StorePreferences.getInstance().get_UseId(), email, subject, comment, new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(retrofit.client.Response response,
                                retrofit.client.Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("response");
                    if (responseStatus.equals("1")) {
                        emailEditText.setText("");
                        subjectEditText.setText("");
                        commentEditText.setText("");
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(),responseMessage,Toast.LENGTH_LONG).show();
                    }else{
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(),responseMessage,Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });

    }

    /**
     * Local validation check for the isEmpty
     *
     * @return true/false according to the validations
     */
    private Boolean localValidation() {

        email = emailEditText.getText().toString().trim();
        subject = subjectEditText.getText().toString().trim();
        comment = commentEditText.getText().toString().trim();
        Boolean response = false;

        if (email.equals("")) {
            emailEditText.setError(getResources().getString(R.string.contact_email_local_validation_message));
            emailEditText.requestFocus();
            response = false;
        } else if (subject.equals("")){
            emailEditText.setError(null);
            subjectEditText.setError(getResources().getString(R.string.contact_subject_local_validation_message));
            subjectEditText.requestFocus();
            response = false;
        }else if (comment.equals("")) {
            emailEditText.setError(null);
            subjectEditText.setError(null);
            commentEditText.setError(getResources().getString(R.string.contact_comment_validation_message));
            commentEditText.requestFocus();
            response = false;
        }else {

            response = true;
        }
        return response;
    }

    /**
     * To check that email address is valid or not
     *
     * @return true/false according to the validations
     */
    private boolean isValidEmail() {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        if(!email.equals("")) {
            if (matcher.matches()) {
                emailEditText.setError(null);
                return true;
            } else {
                emailEditText.setError(getResources().getString(R.string.contact_email_local_validation_message));
                emailEditText.requestFocus();
                return false;
            }
        }else{
            return true;
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
   /* public Interfaces OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/

   /* private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(emailEditText, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    *//**
     * Callback received when a permissions request has been completed.
     *//*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(getActivity(),
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        //addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
*/
   /* private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        emailEditText.setAdapter(adapter);
    }


    private Interfaces ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }*/

}
