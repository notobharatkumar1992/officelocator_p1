package com.officelocator.fragments;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.activity.BranchDetailLocationActivity;
import com.officelocator.activity.MainActivity;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.FlowLayout;
import com.officelocator.util.StorePreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetailsFragment.OnFragmentInteractionListener} Interfaces
 * to handle interaction events.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView BrandNameTextView;
    private TextView categoryNameTextView;
    private TextView mapAddressTextView;
    private TextView phoneNumberTextView;
    private TextView workingHoursTextView;
    private ImageView detailItemImageView;
    private ImageView favoriteImageView;
    private FlowLayout servicesFlowLayout;
    private FlowLayout productFlowLayout;
    private FrameLayout location_direction_map_container;
    private ProgressDialog mProgressDialog;

    //ArrayList declaration

    private String Id;
    private String BranchId;
    private String favorite_status;
    private String latitude;
    private String longitude;
    private String pinLogo;
    private Boolean favoriteStatus = false;

    private OnFragmentInteractionListener mListener;

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        //((MainActivity)getActivity()).setActivityName("Branch Name");
        ((MainActivity) getActivity()).visibleBackButton();

        Id = getArguments().getString("Id");
        BranchId = getArguments().getString("BranchId");

        BrandNameTextView = (TextView) rootView.findViewById(R.id.BrandNameTextView);
        categoryNameTextView = (TextView) rootView.findViewById(R.id.categoryNameTextView);
        mapAddressTextView = (TextView) rootView.findViewById(R.id.mapAddressTextView);
        phoneNumberTextView = (TextView) rootView.findViewById(R.id.phoneNumberTextView);
        workingHoursTextView = (TextView) rootView.findViewById(R.id.workingHoursTextView);
        detailItemImageView = (ImageView) rootView.findViewById(R.id.detailItemImageView);
        favoriteImageView = (ImageView) rootView.findViewById(R.id.favoriteImageView);
        servicesFlowLayout = (FlowLayout) rootView.findViewById(R.id.servicesFlowLayout);
        productFlowLayout = (FlowLayout) rootView.findViewById(R.id.productFlowLayout);
        location_direction_map_container = (FrameLayout) rootView.findViewById(R.id.location_direction_map_container);
        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            getBranchDetailApiCall();
        } else {
            //ConnectionDetector.getInstance().show_alert(getActivity());
            GetDetailFromLocalDataBase();
        }

        location_direction_map_container.setOnClickListener(this);

        return rootView;
    }

    private void getBranchDetailApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().getBranchDetail(Id, StorePreferences.getInstance().get_UseId(), StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response, Response response2) {

                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        if (OfficeLocatorDatabase.getInstance(getActivity()).IsFavoriteDetailExist(Id)) {
                            OfficeLocatorDatabase.getInstance(getActivity()).DeleteFavoriteDetailByFavoriteId(Id);
                            OfficeLocatorDatabase.getInstance(getActivity()).insertFavoriteData(Id, obj_json.toString());
                        } else {
                            OfficeLocatorDatabase.getInstance(getActivity()).insertFavoriteData(Id, obj_json.toString());
                        }

                        JSONObject list = obj_json.getJSONObject("list");
                            /*GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            BranchDetailData.list mBranchDetailData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), BranchDetailData.list.class);
*/
                        ((MainActivity) getActivity()).setActivityName(list.getString("name"));
                        BrandNameTextView.setText(list.getString("name"));
                        categoryNameTextView.setText("(" + list.getString("category_name") + ")");
                        if (list.getString("address2") != null) {
                            mapAddressTextView.setText(list.getString("address2"));
                        }

                        latitude = list.getString("latitude");
                        longitude = list.getString("longitude");
                        pinLogo = list.getString("logo");

                        if (list.has("schedule")) {
                            String schedule = list.getString("schedule").toString().replace("[", "").replace("]", "").replace("\"", "");
                            String[] scheduleDate = schedule.split(",");
                            String schedule_Date = "";
                            /*for (int k =0; k<scheduleDate.length;k++){
                                Date startDate = null;
                                DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                                try {
                                    startDate = df.parse(scheduleDate[k]);
                                    String newDateString = df.format(startDate);
                                    System.out.println(newDateString);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Locale spanish = new Locale("es", "ES");
                                schedule_Date += formatTime(startDate,spanish) +"\n";
                            }*/
                            for (int k = 0; k < scheduleDate.length; k++) {
                                schedule_Date += scheduleDate[k] + "\n";
                            }
                            workingHoursTextView.setText(schedule_Date);
                        }

                        String contact_no = list.getString("contact_no").toString().replace("[", "").replace("]", "").replace("\"", "");
                        String[] contactNo = contact_no.split(",");
                        String contactNumber = "";
                        for (int k = 0; k < contactNo.length; k++) {
                            contactNumber += contactNo[k] + " / ";
                        }
                        phoneNumberTextView.setText(contactNumber);
                        Linkify.addLinks(phoneNumberTextView, Patterns.PHONE, "tel:");
                        phoneNumberTextView.setLinkTextColor(getResources().getColor(R.color.primary_text));

                        Picasso.with(getActivity())
                                .load(list.getString("logo"))
                                .error(R.drawable.sample_bg)
                                .placeholder(R.drawable.sample_bg)
                                .into(detailItemImageView);
                        favorite_status = list.getString("favorite_status");

                        if (favorite_status.equals("0")) {
                            favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_unlike));
                            favoriteStatus = false;
                        } else {
                            favoriteStatus = true;
                            favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_like));
                        }

                        favoriteImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!favoriteStatus) {
                                    favorite_status = "1";
                                    favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_like));
                                } else {
                                    favorite_status = "0";
                                    favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_unlike));
                                }

                                if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                                    UpdateFavoriteStatusApiCall();
                                } else {
                                    ConnectionDetector.getInstance().show_alert(getActivity());
                                }

                            }
                        });

                        JSONArray product = list.getJSONArray("product");
                        if (product.length() > 0) {
                            Log.i("product", "=" + product.length());

                            ArrayList<String> productName = new ArrayList<String>();
                            for (int j = 0; j < product.length(); j++) {
                                JSONObject jsonObject = product.getJSONObject(j);
                                productName.add(jsonObject.getString("name"));
                            }

                            try {
                                for (int l = 0; l < productName.size(); l++) {
                                    StringBuilder mStrBuilder = new StringBuilder();
                                    mStrBuilder.append(" ");
                                    mStrBuilder.append(productName.get(l));
                                    mStrBuilder.append(" ");
                                    //newValue=newValue+mStrBuilder;
                                    final TextView tt = new TextView(getActivity());
                                    tt.setText(mStrBuilder.toString());
                                    tt.setTextColor(Color.WHITE);
                                    tt.setTypeface(null, Typeface.BOLD);
                                    //tt.setTypeface("",);
                                    tt.setTextSize(18);
                                    tt.setPadding(5, 5, 5, 5);
                                    tt.setBackgroundColor(getActivity().getResources().getColor(R.color.orange));
                                    //tt.setBackgroundDrawable(background);(R.drawable.shape_edittext_home);
                                /*if (Build.VERSION.SDK_INT >= 16) {

                                    tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));

                                } else {
                                    tt.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_edittext_home));
                                }*/
                                    //tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));
                                    //tt.setId(i-1);
                                    tt.setClickable(true);
                                /*tt.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        //mFlowLayout.removeView(v);
                                    }
                                });*/
                                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    params.rightMargin = 10;
                                    params.setMargins(5, 5, 5, 5);
                                    tt.setLayoutParams(params);
                                    productFlowLayout.addView(tt);
                                    //i=i+1;
                                    //mTextValue="";
                                    //mText.setText("");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString("destinationLatitude", latitude);
                            bundle.putString("destinationLongitude", longitude);
                            bundle.putString("pinLogo", list.getString("logo"));
                            AppDelegate.LogT("BranchDetailOpenMapFullFragment => map transaction");
                            BranchDetailLocationDirectionMapFragment mBranchDetailLocationDirectionMapFragment = new BranchDetailLocationDirectionMapFragment();
                            mBranchDetailLocationDirectionMapFragment.setArguments(bundle);
                            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                            transaction.add(R.id.location_direction_map_container, mBranchDetailLocationDirectionMapFragment).commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        JSONArray service = list.getJSONArray("service");
                        if (service.length() > 0) {
                            Log.i("service", "=" + service.length());

                            ArrayList<String> serviceName = new ArrayList<String>();
                            for (int j = 0; j < service.length(); j++) {
                                JSONObject jsonObject = service.getJSONObject(j);
                                serviceName.add(jsonObject.getString("name"));
                            }

                            try {
                                for (int k = 0; k < serviceName.size(); k++) {
                                    StringBuilder mStrBuilder = new StringBuilder();
                                    mStrBuilder.append(" ");
                                    mStrBuilder.append(serviceName.get(k));
                                    mStrBuilder.append(" ");
                                    //newValue=newValue+mStrBuilder;
                                    final TextView tt = new TextView(getActivity());
                                    tt.setText(mStrBuilder.toString());
                                    tt.setTextColor(Color.WHITE);
                                    tt.setTypeface(null, Typeface.BOLD);
                                    //tt.setTypeface("",);
                                    tt.setTextSize(18);
                                    tt.setPadding(5, 5, 5, 5);
                                    tt.setBackgroundColor(getActivity().getResources().getColor(R.color.orange));
                                    //tt.setBackgroundDrawable(background);(R.drawable.shape_edittext_home);
                                /*if (Build.VERSION.SDK_INT >= 16) {

                                    tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));

                                } else {
                                    tt.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_edittext_home));
                                }*/
                                    //tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));
                                    //tt.setId(i-1);
                                    tt.setClickable(true);
                                /*tt.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        //mFlowLayout.removeView(v);
                                    }
                                });*/
                                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                    params.rightMargin = 10;
                                    params.setMargins(5, 5, 5, 5);
                                    tt.setLayoutParams(params);
                                    servicesFlowLayout.addView(tt);
                                    //i=i+1;
                                    //mTextValue="";
                                    //mText.setText("");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void UpdateFavoriteStatusApiCall() {

        mProgressDialog.show();//"26.849088", "75.804443"
        SingletonRestClient.get().setFavoriteUpdate(Id, StorePreferences.getInstance().get_UseId(),
                latitude, longitude, favorite_status, new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(retrofit.client.Response response,
                                        retrofit.client.Response response2) {
                        mProgressDialog.dismiss();

                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("response");
                            if (responseStatus.equals("1")) {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                            } else {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == location_direction_map_container) {
            try {
                Intent intent = new Intent(getActivity(), BranchDetailLocationActivity.class);
                Log.d("Map", "Map clicked");
                Bundle bundle = new Bundle();
                bundle.putString("Id", "");
                bundle.putString("destinationLatitude", latitude);
                bundle.putString("destinationLongitude", longitude);
                bundle.putString("pinLogo", pinLogo);
                intent.putExtras(bundle);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            Bundle bundle = new Bundle();
//            bundle.putString("Id", "");
//            BranchDetailOpenMapFullFragment mBranchDetailOpenMapFullFragment = new BranchDetailOpenMapFullFragment();
//            ((MainActivity) getActivity()).loadChildFragment(mBranchDetailOpenMapFullFragment, bundle, "new_main");
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void GetDetailFromLocalDataBase() {
        if (OfficeLocatorDatabase.getInstance(getActivity()).IsFavoriteDetailExist(Id)) {
            String response = OfficeLocatorDatabase.getInstance(getActivity()).GetFavoriteDetailByFavoriteId(Id);
            try {
                JSONObject jsonObject1 = new JSONObject(response);
                String favoriteData = jsonObject1.getString("favoriteData");
                JSONObject obj_json = new JSONObject(favoriteData);
                String responseStatus = obj_json.getString("Status");
                if (responseStatus.equals("true")) {

                    JSONObject list = obj_json.getJSONObject("list");
                            /*GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            BranchDetailData.list mBranchDetailData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), BranchDetailData.list.class);
*/
                    ((MainActivity) getActivity()).setActivityName(list.getString("name"));
                    BrandNameTextView.setText(list.getString("name"));
                    categoryNameTextView.setText("(" + list.getString("category_name") + ")");
                    if (list.getString("address2") != null) {
                        mapAddressTextView.setText(list.getString("address2"));
                    }
                    latitude = list.getString("latitude");
                    longitude = list.getString("longitude");

                    if (list.has("schedule")) {
                        String schedule = list.getString("schedule").toString().replace("[", "").replace("]", "").replace("\"", "");
                        String[] scheduleDate = schedule.split(",");
                        String schedule_Date = "";
                        for (int k = 0; k < scheduleDate.length; k++) {
                            schedule_Date += scheduleDate[k] + "\n";
                        }
                        workingHoursTextView.setText(schedule_Date);
                    }

                    String contact_no = list.getString("contact_no").toString().replace("[", "").replace("]", "").replace("\"", "");
                    String[] contactNo = contact_no.split(",");
                    String contactNumber = "";
                    for (int k = 0; k < contactNo.length; k++) {
                        contactNumber += contactNo[k] + " / ";
                    }
                    phoneNumberTextView.setText(contactNumber);
                    Linkify.addLinks(phoneNumberTextView, Patterns.PHONE, "tel:");
                    phoneNumberTextView.setLinkTextColor(getResources().getColor(R.color.primary_text));
                    Picasso.with(getActivity())
                            .load(list.getString("logo"))
                            .error(R.drawable.sample_bg)
                            .placeholder(R.drawable.sample_bg)
                            .into(detailItemImageView);
                    favorite_status = list.getString("favorite_status");

                    if (favorite_status.equals("0")) {
                        favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_unlike));
                        favoriteStatus = false;
                    } else {
                        favoriteStatus = true;
                        favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_like));
                    }

                    favoriteImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!favoriteStatus) {
                                favorite_status = "1";
                                favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_like));
                            } else {
                                favorite_status = "0";
                                favoriteImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_unlike));
                            }

                            if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                                UpdateFavoriteStatusApiCall();
                            } else {
                                ConnectionDetector.getInstance().show_alert(getActivity());
                            }

                        }
                    });

                    JSONArray product = list.getJSONArray("product");
                    if (product.length() > 0) {
                        Log.i("product", "=" + product.length());

                        ArrayList<String> productName = new ArrayList<String>();
                        for (int j = 0; j < product.length(); j++) {
                            JSONObject jsonObject = product.getJSONObject(j);
                            productName.add(jsonObject.getString("name"));
                        }

                        try {
                            for (int l = 0; l < productName.size(); l++) {
                                StringBuilder mStrBuilder = new StringBuilder();
                                mStrBuilder.append(" ");
                                mStrBuilder.append(productName.get(l));
                                mStrBuilder.append(" ");
                                //newValue=newValue+mStrBuilder;
                                final TextView tt = new TextView(getActivity());
                                tt.setText(mStrBuilder.toString());
                                tt.setTextColor(Color.WHITE);
                                tt.setTypeface(null, Typeface.BOLD);
                                //tt.setTypeface("",);
                                tt.setTextSize(18);
                                tt.setPadding(5, 5, 5, 5);
                                tt.setBackgroundColor(getActivity().getResources().getColor(R.color.orange));
                                //tt.setBackgroundDrawable(background);(R.drawable.shape_edittext_home);
                                /*if (Build.VERSION.SDK_INT >= 16) {

                                    tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));

                                } else {
                                    tt.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_edittext_home));
                                }*/
                                //tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));
                                //tt.setId(i-1);
                                tt.setClickable(true);
                                /*tt.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        //mFlowLayout.removeView(v);
                                    }
                                });*/
                                FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                params.rightMargin = 10;
                                params.setMargins(5, 5, 5, 5);
                                tt.setLayoutParams(params);
                                productFlowLayout.addView(tt);
                                //i=i+1;
                                //mTextValue="";
                                //mText.setText("");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        Bundle bundle = new Bundle();
                        bundle.putString("destinationLatitude", latitude);
                        bundle.putString("destinationLongitude", longitude);
                        bundle.putString("pinLogo", list.getString("logo"));
                        BranchDetailLocationDirectionMapFragment mBranchDetailLocationDirectionMapFragment = new BranchDetailLocationDirectionMapFragment();
                        mBranchDetailLocationDirectionMapFragment.setArguments(bundle);
                        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                        transaction.add(R.id.location_direction_map_container, mBranchDetailLocationDirectionMapFragment).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    JSONArray service = list.getJSONArray("service");
                    if (service.length() > 0) {
                        Log.i("service", "=" + service.length());

                        ArrayList<String> serviceName = new ArrayList<String>();
                        for (int j = 0; j < service.length(); j++) {
                            JSONObject jsonObject = service.getJSONObject(j);
                            serviceName.add(jsonObject.getString("name"));
                        }

                        try {
                            for (int k = 0; k < serviceName.size(); k++) {
                                StringBuilder mStrBuilder = new StringBuilder();
                                mStrBuilder.append(" ");
                                mStrBuilder.append(serviceName.get(k));
                                mStrBuilder.append(" ");
                                //newValue=newValue+mStrBuilder;
                                final TextView tt = new TextView(getActivity());
                                tt.setText(mStrBuilder.toString());
                                tt.setTextColor(Color.WHITE);
                                tt.setTypeface(null, Typeface.BOLD);
                                //tt.setTypeface("",);
                                tt.setTextSize(18);
                                tt.setPadding(5, 5, 5, 5);
                                tt.setBackgroundColor(getActivity().getResources().getColor(R.color.orange));
                                //tt.setBackgroundDrawable(background);(R.drawable.shape_edittext_home);
                                /*if (Build.VERSION.SDK_INT >= 16) {

                                    tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));

                                } else {
                                    tt.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_edittext_home));
                                }*/
                                //tt.setBackground(getResources().getDrawable(R.drawable.shape_edittext_home));
                                //tt.setId(i-1);
                                tt.setClickable(true);
                                /*tt.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        //mFlowLayout.removeView(v);
                                    }
                                });*/
                                FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
                                params.rightMargin = 10;
                                params.setMargins(5, 5, 5, 5);
                                tt.setLayoutParams(params);
                                servicesFlowLayout.addView(tt);
                                //i=i+1;
                                //mTextValue="";
                                //mText.setText("");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                } else {
                    String responseMessage = obj_json.getString("Message");
                    Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //AlertDialog("Server Error! Please try again.");
            }

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.detail_branch_no_data_message), Toast.LENGTH_LONG).show();
        }
    }

   /* public static String formatTime(Date time, Locale locale){
        String timeFormat = " dd/MM/yyyy hh:mm:ss";

        SimpleDateFormat formatter;

        try {
            formatter = new SimpleDateFormat(timeFormat, locale);
        } catch(Exception e) {
            formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss", locale);
        }
        return formatter.format(time);
    }*/
}
