package com.officelocator.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.activity.BranchDetailsActivity;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.MapBrandListAdapter;
import com.officelocator.model.BranchList;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by himanshu on 3/24/2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private static Double latitude = 21.170240, longitude = 72.831062;
    private static String categoryId;

    private LinearLayout mapDisplayLinearLayout;
    private LinearLayout listDisplayLinearLayout;
    private TextView mapLabelTextView;
    private TextView listLabelTextView;
    private ListView BranchListView;
    private FrameLayout map_container;
    private ProgressDialog mProgressDialog;

    //ArrayList declaration
    ArrayList<BranchList.list> BranchArrayList;
    MapBrandListAdapter mapBrandListAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        categoryId = getArguments().getString("categoryId");
        Log.i("categoryId", categoryId + "+");

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_nearby_branches));
        ((MainActivity) getActivity()).visibleBackButton();
        // Inflate the layout for this fragment

        // Initialization
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);
        BranchArrayList = new ArrayList<BranchList.list>();


        mapDisplayLinearLayout = (LinearLayout) rootView.findViewById(R.id.mapDisplayLinearLayout);
        listDisplayLinearLayout = (LinearLayout) rootView.findViewById(R.id.listDisplayLinearLayout);
        mapLabelTextView = (TextView) rootView.findViewById(R.id.mapLabelTextView);
        listLabelTextView = (TextView) rootView.findViewById(R.id.listLabelTextView);
        BranchListView = (ListView) rootView.findViewById(R.id.BranchListView);
        map_container = (FrameLayout) rootView.findViewById(R.id.map_container);
        mapDisplayLinearLayout.setOnClickListener(this);
        listDisplayLinearLayout.setOnClickListener(this);

        try {
            Bundle bundle = new Bundle();
            bundle.putString("categoryId", categoryId);
            CustomMapFragment mapFragment = new CustomMapFragment();
            mapFragment.setArguments(bundle);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.map_container, mapFragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        if (v == mapDisplayLinearLayout) {

            map_container.setVisibility(View.VISIBLE);
            BranchListView.setVisibility(View.GONE);

            mapDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
            listDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.white));

            mapLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.white));
            listLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));

        } else if (v == listDisplayLinearLayout) {

            BranchListView.setVisibility(View.VISIBLE);
            map_container.setVisibility(View.GONE);

            mapDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            listDisplayLinearLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));

            mapLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            listLabelTextView.setTextColor(getActivity().getResources().getColor(R.color.white));

            if (BranchArrayList.size() > 0) {

            } else {
                if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                    getBranchByCategoryAPiCall();
                } else {
                    ConnectionDetector.getInstance().show_alert(getActivity());
                }
            }
        }
    }

    private void getBranchByCategoryAPiCall() {
        mProgressDialog.show();//"26.849088", "75.804443"
        String radius = "";
        if (!StorePreferences.getInstance().getRadius().equals("")) {
            radius = StorePreferences.getInstance().getRadius();
        } else {
            radius = "20";
        }

        SingletonRestClient.get().getBranchByCategory(categoryId, StorePreferences.getInstance().getLatitude(), StorePreferences.getInstance().getLongitude(), radius, StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(retrofit.client.Response response,
                                retrofit.client.Response response2) {

                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {
                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            BranchList mBranchList = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), BranchList.class);

                            BranchArrayList = mBranchList.list;

                            int count = 0;
                            for (int i = 0; i < BranchArrayList.size(); i++) {
                                BranchArrayList.get(i).value = count;
                                if (count == AppDelegate.TILES_COLOR_COUNT) {
                                    count = 0;
                                } else {
                                    count++;
                                }
                            }

                            Log.i("BranchArrayList", BranchArrayList.size() + "==");
                            mapBrandListAdapter = new MapBrandListAdapter(getActivity(), BranchArrayList);
                            BranchListView.setAdapter(mapBrandListAdapter);

                            BranchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("Id", BranchArrayList.get(position).id);
                                    bundle.putString("BranchId", BranchArrayList.get(position).category_id);

                                    Intent intent = new Intent(getActivity(), BranchDetailsActivity.class);
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            });
                        }
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }
}
