package com.officelocator.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.adapters.CountryListAdapter;
import com.officelocator.adapters.SecurityQuestionsListAdapter;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.ImageUtils;
import com.officelocator.util.StorePreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileFragment.OnFragmentInteractionListener} Interfaces
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Spinner securityQuestionSpinner;
    private Spinner countrySpinner;
    private ImageView userImageView;
    private ImageView userPhotoSelectionImageView;
    private Button updateProfileButton;
    private EditText userNameEditText;
    private EditText nameEditText;
    private EditText emailEditText;
    private EditText addressEditText;
    private EditText cityEditText;
    //private EditText countryEditText;
    private EditText contactNoEditText;
    private EditText currentPasswordEditText;
    private EditText newPasswordEditText;
    private EditText securityQuestionAnswerEditText;

    private ProgressDialog mProgressDialog;

    private String userName = "";
    private String email = "";
    private String newPassword = "";
    private String currentPassword = "";
    private String name = "";
    private String address = "";
    private String city = "";
    private String contactNo = "";
    private String countryName = "";
    private String securityQuestionAnswer;
    private String securityQuestion;
    private String securityQuestionId = "0";
    private String latitude = "";
    private String longitude = "";

    private Uri mImageCaptureUri;
    public static String mCurrentPhotoPath = "";

    //ArrayList declaration
    ArrayList<String> countryArrayList;
    ArrayList<String> securityQuestionArrayList;
    ArrayList<String> securityQuestionIdArrayList;

    private OnFragmentInteractionListener mListener;

    // Global variable declaration
    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String Password_PATTERN =
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";

    // for email validation
    public Pattern pattern;
    public Matcher matcher;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavouiteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_my_profile));
        ((MainActivity) getActivity()).visibleBackButton();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        userImageView = (ImageView) rootView.findViewById(R.id.userImageView);
        userPhotoSelectionImageView = (ImageView) rootView.findViewById(R.id.userPhotoSelectionImageView);

        securityQuestionSpinner = (Spinner) rootView.findViewById(R.id.securityQuestionSpinner);
        countrySpinner = (Spinner) rootView.findViewById(R.id.countrySpinner);
        updateProfileButton = (Button) rootView.findViewById(R.id.updateProfileButton);
        userNameEditText = (EditText) rootView.findViewById(R.id.userNameEditText);
        nameEditText = (EditText) rootView.findViewById(R.id.nameEditText);
        emailEditText = (EditText) rootView.findViewById(R.id.emailEditText);
        addressEditText = (EditText) rootView.findViewById(R.id.addressEditText);
        cityEditText = (EditText) rootView.findViewById(R.id.cityEditText);
        contactNoEditText = (EditText) rootView.findViewById(R.id.contactNoEditText);
        currentPasswordEditText = (EditText) rootView.findViewById(R.id.currentPasswordEditText);
        newPasswordEditText = (EditText) rootView.findViewById(R.id.newPasswordEditText);
        securityQuestionAnswerEditText = (EditText) rootView.findViewById(R.id.securityQuestionAnswerEditText);

        userNameEditText.setEnabled(false);

        cityEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                    textView.clearFocus();
                    countrySpinner.requestFocus();
                    countrySpinner.performClick();
                }
                return true;
            }
        });

        newPasswordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                    textView.clearFocus();
                    securityQuestionSpinner.requestFocus();
                    securityQuestionSpinner.performClick();
                }
                return true;
            }
        });

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {

            getUserProfileDataApiCall();

        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }

        userPhotoSelectionImageView.setOnClickListener(this);
        updateProfileButton.setOnClickListener(this);

        return rootView;
    }

    private void getUserProfileDataApiCall() {

        mProgressDialog.show();
        SingletonRestClient.get().getUserProfile(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(retrofit.client.Response response,
                                retrofit.client.Response response2) {
                mProgressDialog.dismiss();

                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseCountry = obj_json.getString("response");
                    if (responseCountry.equals("1")) {
                        //String responseMessage = obj_json.getString("responseMessage");
                        JSONObject output = obj_json.getJSONObject("output");
                        if (output.getString("username") != null) {
                            userNameEditText.setText(output.getString("username"));
                        }
                        if (output.getString("first_name") != null && output.getString("last_name") != null) {
                            nameEditText.setText(output.getString("first_name") + " " + output.getString("last_name"));
                        }
                        if (output.getString("email") != null) {
                            emailEditText.setText(output.getString("email"));
                        }
                        if (output.getString("address") != null) {
                            addressEditText.setText(output.getString("address"));
                        }
                        if (output.getString("city") != null) {
                            cityEditText.setText(output.getString("city"));
                        }
                        if (output.getString("country") != null) {
                            countryName = output.getString("country");
                            //countryEditText.setText(output.getString("country"));
                        }
                        if (output.getString("contact_no") != null) {
                            contactNoEditText.setText(output.getString("contact_no"));
                        }
                        if (output.getString("question_id") != null) {
                            securityQuestionId = output.getString("question_id");
                        }
                        if (output.getString("answer") != null) {
                            securityQuestionAnswerEditText.setText(output.getString("answer"));
                        }
                        if (output.getString("latitude") != null) {
                            latitude = output.getString("latitude");
                        }
                        if (output.getString("longitude") != null) {
                            longitude = output.getString("longitude");
                        }
                        if (output.getString("logo") != null) {
                            StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));
                            ((MainActivity) getActivity()).userImageLogoRefresh();
                            Picasso.with(getActivity())
                                    .load(output.getString("logo"))
                                    .error(R.drawable.sample_bg)
                                    .placeholder(R.drawable.sample_bg)
                                    .into(userImageView);
                        }

                        // Get Hints Question API call
                        getHintsQuestionsAPICall();

                        // Get Country API call
                        getCountriesAPICall();

                    } else {

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }

            }
        });
    }

    private void getCountriesAPICall() {
        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            SingletonRestClient.get().getCountry(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {

                }

                @Override
                public void success(retrofit.client.Response response,
                                    retrofit.client.Response response2) {

                    try {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        String responseCountry = obj_json.getString("response");
                        if (responseCountry.equals("1")) {
                            //String responseMessage = obj_json.getString("responseMessage");
                            JSONArray obJsonArray = obj_json.getJSONArray("output");

                            if (obJsonArray.length() > 0) {
                                countryArrayList = new ArrayList<String>();
                                countryArrayList.add(getResources().getString(R.string.select_country_hint));
                                for (int i = 0; i < obJsonArray.length(); i++) {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    countryArrayList.add(jsonObject.getString("country"));
                                }
                            }


                            // Creating adapter for spinner
                            CountryListAdapter countryListAdapter = new CountryListAdapter(getActivity(), countryArrayList);

                            // attaching data adapter to spinner
                            countrySpinner.setAdapter(countryListAdapter);

                            Log.e("countryName location", countryName);


                            countrySpinner.setSelection(getCategoryPos(countryName));

                            countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    countryName = countryArrayList.get(position).toString();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else {

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });
        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }
    }

    private void getHintsQuestionsAPICall() {

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {

            SingletonRestClient.get().getHints(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {

                }

                @Override
                public void success(retrofit.client.Response response,
                                    retrofit.client.Response response2) {

                    try {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        Boolean responseStatus = obj_json.getBoolean("Status");
                        if (!responseStatus) {
                            //String responseMessage = obj_json.getString("responseMessage");

                        } else {
                            String Message = obj_json.getString("Message");
                            Log.i("Message", Message + "==");

                            JSONArray obJsonArray = obj_json.getJSONArray("list");

                            if (obJsonArray.length() > 0) {
                                securityQuestionArrayList = new ArrayList<String>();
                                securityQuestionIdArrayList = new ArrayList<String>();
                                securityQuestionArrayList.add(getActivity().getResources().getString(R.string.select_questions_hint));
                                securityQuestionIdArrayList.add("0");
                                for (int i = 0; i < obJsonArray.length(); i++) {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    securityQuestionArrayList.add(jsonObject.getString("name"));
                                    securityQuestionIdArrayList.add(jsonObject.getString("id"));
                                }
                            }

                            // Creating adapter for spinner
                            SecurityQuestionsListAdapter securityQuestionAdapter = new SecurityQuestionsListAdapter(getActivity(), securityQuestionArrayList);

                            // attaching data adapter to spinner
                            securityQuestionSpinner.setAdapter(securityQuestionAdapter);
                            securityQuestionSpinner.setSelection(getSecurityQuestionPos(securityQuestionId));

                            securityQuestionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    securityQuestionId = securityQuestionIdArrayList.get(position).toString();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });


                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });

        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == userPhotoSelectionImageView) {
            final String[] items = new String[]{getResources().getString(R.string.image_selection_alert_camera),
                    getResources().getString(R.string.image_selection_alert_gallery)};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, items);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            builder.setTitle(getResources().getString(R.string.image_selection_alert_title));
            builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) { //pick from camera
                    if (item == 0) {
                        /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
                                "tmp_profile_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        try {
                            intent.putExtra("return-data", true);
                            startActivityForResult(intent, PICK_FROM_CAMERA);
                        } catch (ActivityNotFoundException e) {
                            e.printStackTrace();
                        }*/
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        // Ensure that there's a camera activity to handle the intent
                        if (cameraIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            // Create the File where the photo should go
                            File photoFile = null;
                            try {
                                photoFile = createImageFile();
                                //Log.i("########photoFile",photoFile+"==");
                                //Log.i("########photoFile PATH",photoFile.getAbsolutePath()+"==");
                            } catch (IOException ex) {
                                // Error occurred while creating the File
                                ex.printStackTrace();
                                // Log.i("######## cameraIntent ERROR LOG","File Not Created");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Continue only if the File was successfully created
                            if (photoFile != null) {
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                getActivity().startActivityForResult(cameraIntent, MainActivity.PICK_FROM_CAMERA);
                            } else {
                                Log.e("cameraIntent ERROR LOG", "File Not Created");
                            }
                        }
                    } else { //pick from file
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_PICK);
                        getActivity().startActivityForResult(Intent.createChooser(intent, "Complete action using"), MainActivity.PICK_FROM_FILE);
                    }
                }
            });

            final AlertDialog dialog = builder.create();
            dialog.show();
        } else if (v == updateProfileButton) {
            Log.e("latitude", latitude + "=");
            Log.e("longitude", longitude + "=");
            if (local_validation()) {
                if (isValidEmail()) {
                    if (isValidPassword()) {
                        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                            updateMyProfileApiCall();
                        } else {
                            ConnectionDetector.getInstance().show_alert(getActivity());
                        }
                    }
                }
            }
        }
    }

    private void updateMyProfileApiCall() {
        mProgressDialog.show();
        if (newPassword.equals("")) {
            newPassword = StorePreferences.getInstance().getUserPassword();
        }
        if (currentPassword.equals("")) {
            currentPassword = StorePreferences.getInstance().getUserPassword();
        }
        TypedFile typedFileCurrentPhotoPath;
        if (MainActivity.finalFile != null) {
            typedFileCurrentPhotoPath = new TypedFile("multipart/form-data",
                    new File(String.valueOf(MainActivity.finalFile)));
            SingletonRestClient.get().sendUserEditProfile(address, securityQuestionAnswer, city, contactNo,
                    countryName, email, name, latitude, longitude, newPassword, currentPassword, securityQuestionId,
                    userName, StorePreferences.getInstance().get_UseId(), typedFileCurrentPhotoPath, new Callback<Response>() {
                        @Override
                        public void failure(RestError restError) {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void success(Response response, Response response2) {
                            mProgressDialog.dismiss();
                            try {
                                JSONObject obj_json = new JSONObject(
                                        new String(((TypedByteArray) response.getBody()).getBytes()));
                                String responseCountry = obj_json.getString("response");
                                if (responseCountry.equals("1")) {
                                    StorePreferences.getInstance().setUserName(userName);
                                    StorePreferences.getInstance().setEmailId(email);
                                    //if (StorePreferences.getInstance().getIsRememberMe()) {
                                    StorePreferences.getInstance().setUserPassword(newPassword);
                                    // }
                                    JSONObject output = obj_json.getJSONObject("output");
                                    if (output.getString("logo") != null) {
                                        StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));
                                        ((MainActivity) getActivity()).userImageLogoRefresh();
                                    }
                                    if (OfficeLocatorDatabase.getInstance(getActivity()).IsUserInfoExist()) {
                                        OfficeLocatorDatabase.getInstance(getActivity()).deleteAllUserInfo();
                                        OfficeLocatorDatabase.getInstance(getActivity()).insertUserInfo(userName,
                                                newPassword, StorePreferences.getInstance().get_UseId());
                                    } else {
                                        OfficeLocatorDatabase.getInstance(getActivity()).insertUserInfo(userName,
                                                newPassword, StorePreferences.getInstance().get_UseId());
                                    }
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                                    getFragmentManager().popBackStack();
                                } else {
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                //AlertDialog("Server Error! Please try again.");
                            }
                        }
                    });
        } else {

            SingletonRestClient.get().sendUserEditProfileWithoutImage(address, securityQuestionAnswer, city, contactNo,
                    countryName, email, name, latitude, longitude, newPassword, currentPassword, securityQuestionId,
                    userName, StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
                        @Override
                        public void failure(RestError restError) {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void success(Response response, Response response2) {
                            mProgressDialog.dismiss();
                            try {
                                JSONObject obj_json = new JSONObject(
                                        new String(((TypedByteArray) response.getBody()).getBytes()));
                                String responseCountry = obj_json.getString("response");
                                if (responseCountry.equals("1")) {
                                    StorePreferences.getInstance().setUserName(userName);
                                    StorePreferences.getInstance().setEmailId(email);
                                    //if (StorePreferences.getInstance().getIsRememberMe()) {
                                    StorePreferences.getInstance().setUserPassword(newPassword);
                                    // }
                                    JSONObject output = obj_json.getJSONObject("output");
                                    if (output.getString("logo") != null) {
                                        StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));
                                        ((MainActivity) getActivity()).userImageLogoRefresh();
                                    }
                                    if (OfficeLocatorDatabase.getInstance(getActivity()).IsUserInfoExist()) {
                                        OfficeLocatorDatabase.getInstance(getActivity()).deleteAllUserInfo();
                                        OfficeLocatorDatabase.getInstance(getActivity()).insertUserInfo(userName,
                                                newPassword, StorePreferences.getInstance().get_UseId());
                                    } else {
                                        OfficeLocatorDatabase.getInstance(getActivity()).insertUserInfo(userName,
                                                newPassword, StorePreferences.getInstance().get_UseId());
                                    }
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                                } else {
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                //AlertDialog("Server Error! Please try again.");
                            }
                        }
                    });
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        File image = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "tmp_profile_" + timeStamp + "_";

        File storageDir = getActivity().getExternalFilesDir("OfficeLocatorProfilePicture");
        if (storageDir.exists()) {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } else {
            try {
                if (storageDir.mkdir()) {
                    System.out.println("Directory created");
                    image = File.createTempFile(
                            imageFileName,  /* prefix */
                            ".jpg",         /* suffix */
                            storageDir      /* directory */
                    );
                } else {
                    Log.e("storageDir.mkdir", "Directory is not created");
                }
            } catch (Exception e) {
                Log.e("File create err", e + "");
                e.printStackTrace();
            }

        }

        if (image != null) {
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = image.getAbsolutePath();
//            MainActivity.finalFile = image;
        } else
            Log.e("photoFile", "file not dound");

        Log.i("########photoFile", mCurrentPhotoPath + "==");

        return image;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!MainActivity.send_image.equals("")) {
            setFiletoImageview(userImageView, mCurrentPhotoPath);
            mCurrentPhotoPath = "";
        }
    }

    public void setFiletoImageview(ImageView mImageThumbnail, String filePath) {
        try {
            if (!filePath.equals(""))
                Log.e("filePath", filePath + "=");
            mImageThumbnail.setImageBitmap(ImageUtils.decodeFile(filePath));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private int getCategoryPos(String category) {
        return countryArrayList.indexOf(category);
    }

    private int getSecurityQuestionPos(String SecurityQuestionId) {
        return securityQuestionIdArrayList.indexOf(SecurityQuestionId);
    }

    /**
     * Local validation check for the isEmpty
     *
     * @return true/false according to the validations
     */
    private Boolean local_validation() {

        userName = userNameEditText.getText().toString().trim();
        email = emailEditText.getText().toString().trim();
        newPassword = newPasswordEditText.getText().toString().trim();
        currentPassword = currentPasswordEditText.getText().toString().trim();
        name = nameEditText.getText().toString().trim();
        address = addressEditText.getText().toString().trim();
        city = cityEditText.getText().toString().trim();
        contactNo = contactNoEditText.getText().toString().trim();
        securityQuestionAnswer = securityQuestionAnswerEditText.getText().toString().trim();

        Boolean response = false;

        if (userName.equals("")) {
            userNameEditText.setError(getResources().getString(R.string.profile_user_name_local_validation_message));
            userNameEditText.requestFocus();
            response = false;
        }/* else if (email.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError("Please Enter Email");
            emailEditText.requestFocus();
            response = false;
        } else if (password.equals("")){
            userNameEditText.setError(null);
            //emailEditText.setError(null);
            currentPasswordEditText.setError("Please Enter Password");
            currentPasswordEditText.requestFocus();
            response = false;
        }else if (confirmPassword.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            currentPasswordEditText.setError(null);
            newPasswordEditText.setError("Please Enter Confirm Password");
            newPasswordEditText.requestFocus();
            response = false;
        }*/ else if (name.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            currentPasswordEditText.setError(null);
            newPasswordEditText.setError(null);
            nameEditText.setError(getResources().getString(R.string.profile_name_local_validation_message));
            nameEditText.requestFocus();
            response = false;
        } else if (address.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            currentPasswordEditText.setError(null);
            newPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(getResources().getString(R.string.profile_address_local_validation_message));
            addressEditText.requestFocus();
            response = false;
        } else if (city.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            currentPasswordEditText.setError(null);
            newPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(null);
            cityEditText.setError(getResources().getString(R.string.profile_city_local_validation_message));
            cityEditText.requestFocus();
            response = false;
        } else if (contactNo.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            currentPasswordEditText.setError(null);
            newPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(null);
            cityEditText.setError(null);
            contactNoEditText.setError(getResources().getString(R.string.profile_contact_no_local_validation_message));
            contactNoEditText.requestFocus();
            response = false;
        } else if (countryName.equals(getResources().getString(R.string.select_country_hint))) {
            Toast.makeText(getActivity(), getResources().getString(R.string.profile_select_country_local_validation_message), Toast.LENGTH_LONG).show();
            response = false;

        } else if (securityQuestionId.equals("0")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.profile_select_security_question_local_validation_message), Toast.LENGTH_LONG).show();
            response = false;

        } else if (securityQuestionAnswer.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            currentPasswordEditText.setError(null);
            newPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(null);
            cityEditText.setError(null);
            contactNoEditText.setError(null);
            securityQuestionAnswerEditText.setError(getResources().getString(R.string.security_question_answer_local_validation_message));
            securityQuestionAnswerEditText.requestFocus();
            response = false;
        } else {

            response = true;
        }
        return response;
    }

    /**
     * To check that email address is valid or not
     *
     * @return true/false according to the validations
     */
    private boolean isValidEmail() {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        if (!email.equals("")) {
            if (matcher.matches()) {
                emailEditText.setError(null);
                return true;
            } else {
                emailEditText.setError(getResources().getString(R.string.email_local_validation_message));
                emailEditText.requestFocus();
                return false;
            }
        } else {
            return true;
        }
    }

    private boolean isValidPassword() {
        if (!newPassword.equals("") || !currentPassword.equals("")) {
            if (currentPassword.length() < 6) {
                currentPasswordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
                currentPasswordEditText.requestFocus();
                return false;
            } else if (newPassword.length() < 6) {
                currentPasswordEditText.setError(null);
                newPasswordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
                newPasswordEditText.requestFocus();
                return false;
            } else if (!isMatchCurrentPassword()) {
                newPasswordEditText.setError(null);
                currentPasswordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
                currentPasswordEditText.requestFocus();
                return false;
            } else if (!isMatchNewPassword()) {
                currentPasswordEditText.setError(null);
                newPasswordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
                newPasswordEditText.requestFocus();
                return false;
            } else if (!isMatchCurrentPasswordWithStorePassword()) {
                currentPasswordEditText.setError(getResources().getString(R.string.profile_select_password_match_local_validation_message));
                currentPasswordEditText.requestFocus();
                return false;
            } else {
                currentPasswordEditText.setError(null);
                newPasswordEditText.setError(null);
                return true;
            }
        } else {
            return true;
        }
    }

    private Boolean isMatchCurrentPasswordWithStorePassword() {

        if (!currentPassword.equals("")) {
            if (currentPassword.equals(StorePreferences.getInstance().getUserPassword())) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private Boolean isMatchCurrentPassword() {
        pattern = Pattern.compile(Password_PATTERN);
        matcher = pattern.matcher(currentPassword);

        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private Boolean isMatchNewPassword() {
        pattern = Pattern.compile(Password_PATTERN);
        matcher = pattern.matcher(newPassword);

        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }
}
