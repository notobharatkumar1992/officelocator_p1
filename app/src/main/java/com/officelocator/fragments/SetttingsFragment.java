package com.officelocator.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.officelocator.R;
import com.officelocator.activity.MainActivity;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SetttingsFragment.OnFragmentInteractionListener} Interfaces
 * to handle interaction events.
 * Use the {@link SetttingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SetttingsFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Control declaration
    private Switch soundSwitch;
    private Switch pushNotificationSwitch;
    private Switch vibrationSwitch;
    private TextView radiusMinimumLabelTextView;
    private TextView radiusSelectedValueTextView;
    private TextView radiusMaximumLabelTextView;
    private SeekBar radiusSeekBar;
    private RadioGroup languageRadioGroup;
    private Button settingApplyButton;
    private ProgressDialog mProgressDialog;
    private RadioButton englishRadioButton;
    private RadioButton spanishRadioButton;
    private RadioButton maxicoRadioButton;

    private String push_notification;
    private String sound;
    private String vibration;
    private String lang = "sp";
    private String latitude;
    private String longitude;
    private String radius;


    private OnFragmentInteractionListener mListener;

    public SetttingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SetttingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SetttingsFragment newInstance(String param1, String param2) {
        SetttingsFragment fragment = new SetttingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_setttings, container, false);

        ((MainActivity) getActivity()).setActivityName(getResources().getString(R.string.title_settings));
        ((MainActivity) getActivity()).visibleBackButton();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        soundSwitch = (Switch) rootView.findViewById(R.id.soundSwitch);
        pushNotificationSwitch = (Switch) rootView.findViewById(R.id.pushNotificationSwitch);
        vibrationSwitch = (Switch) rootView.findViewById(R.id.vibrationSwitch);
        radiusMinimumLabelTextView = (TextView) rootView.findViewById(R.id.radiusMinimumLabelTextView);
        radiusSelectedValueTextView = (TextView) rootView.findViewById(R.id.radiusSelectedValueTextView);
        radiusMaximumLabelTextView = (TextView) rootView.findViewById(R.id.radiusMaximumLabelTextView);
        radiusSeekBar = (SeekBar) rootView.findViewById(R.id.radiusSeekBar);
        settingApplyButton = (Button) rootView.findViewById(R.id.settingApplyButton);
        languageRadioGroup = (RadioGroup) rootView.findViewById(R.id.languageRadioGroup);

        englishRadioButton = (RadioButton) rootView.findViewById(R.id.englishRadioButton);
        spanishRadioButton = (RadioButton) rootView.findViewById(R.id.spanishRadioButton);
        maxicoRadioButton = (RadioButton) rootView.findViewById(R.id.maxicoRadioButton);
        settingApplyButton.setOnClickListener(this);

        radiusSeekBar.setMax(100);
        if (StorePreferences.getInstance().getRadius().equals("")) {
            radiusSeekBar.setProgress(5);
            radiusSelectedValueTextView.setText(5 + " KM");
        } else {
            radiusSeekBar.setProgress(Integer.parseInt(StorePreferences.getInstance().getRadius()));
            radiusSelectedValueTextView.setText(StorePreferences.getInstance().getRadius() + " KM");
        }

        radius = String.valueOf(radiusSeekBar.getProgress());
        radiusMinimumLabelTextView.setText("5 KM");

        radiusMaximumLabelTextView.setText("100 KM");

        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (progress >= 5) {
                    radiusSelectedValueTextView.setText(String.valueOf(progress) + " KM");
                    StorePreferences.getInstance().setRadius(String.valueOf(progress));
                } else {
                    if (StorePreferences.getInstance().getRadius().equals("")) {
                        radiusSeekBar.setProgress(5);
                        radiusSelectedValueTextView.setText(5 + " KM");
                        radius = String.valueOf(radiusSeekBar.getProgress());
                    } else {

                        radiusSeekBar.setProgress(Integer.parseInt(StorePreferences.getInstance().getRadius()));
                        radiusSelectedValueTextView.setText(StorePreferences.getInstance().getRadius() + " KM");
                        radius = String.valueOf(radiusSeekBar.getProgress());
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (StorePreferences.getInstance().getRadius().equals("")) {
                    radiusSeekBar.setProgress(5);
                    radius = String.valueOf(radiusSeekBar.getProgress());
                    radiusSelectedValueTextView.setText(5 + " KM");
                    radius = String.valueOf(radiusSeekBar.getProgress());
                } else {

                    radiusSeekBar.setProgress(Integer.parseInt(StorePreferences.getInstance().getRadius()));
                    radiusSelectedValueTextView.setText(StorePreferences.getInstance().getRadius() + " KM");
                    radius = String.valueOf(radiusSeekBar.getProgress());
                }

            }
        });

        if (StorePreferences.getInstance().getNotificationSound()) {
            sound = "1";
            soundSwitch.setChecked(true);
        }
        if (StorePreferences.getInstance().getPushNotificationShow()) {
            push_notification = "1";
            pushNotificationSwitch.setChecked(true);
        }
        if (StorePreferences.getInstance().getIsVibration()) {
            vibration = "1";
            vibrationSwitch.setChecked(true);
        }

        soundSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sound = "1";
                    StorePreferences.getInstance().setNotificationSound(true);
                } else {
                    sound = "0";
                    StorePreferences.getInstance().setNotificationSound(false);
                }
            }
        });
        pushNotificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    push_notification = "1";
                    StorePreferences.getInstance().setPushNotificationShow(true);
                } else {
                    push_notification = "0";
                    StorePreferences.getInstance().setPushNotificationShow(false);
                }
            }
        });
        vibrationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    vibration = "1";
                    StorePreferences.getInstance().setIsVibration(true);
                } else {
                    vibration = "0";
                    StorePreferences.getInstance().setIsVibration(false);
                }
            }
        });

        if (StorePreferences.getInstance().getLanguage().equals("")) {
            spanishRadioButton.setChecked(true);
        } else {
            if (StorePreferences.getInstance().getLanguage().equals("en")) {
                englishRadioButton.setChecked(true);
            } else if (StorePreferences.getInstance().getLanguage().equals("es-MX")) {
                maxicoRadioButton.setChecked(true);
            } else {
                spanishRadioButton.setChecked(true);
            }
        }

        languageRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.englishRadioButton) {
                    lang = "en";
                    StorePreferences.getInstance().setLanguage(lang);
                } else if (checkedId == R.id.spanishRadioButton) {
                    lang = "es";
                    StorePreferences.getInstance().setLanguage(lang);
                } else if (checkedId == R.id.maxicoRadioButton) {
                    lang = "es-MX";
                    StorePreferences.getInstance().setLanguage(lang);
                }
            }
        });


        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            getSettingsApiCall();
        } else {
            ConnectionDetector.getInstance().show_alert(getActivity());
        }

        return rootView;
    }

    private void getSettingsApiCall() {

        mProgressDialog.show();//27
        SingletonRestClient.get().getSettingDetails(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(retrofit.client.Response response,
                                retrofit.client.Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    Boolean responseStatus = obj_json.getBoolean("Status");
                    if (responseStatus) {

                        JSONObject list = obj_json.getJSONObject("list");
                        String lang = list.getString("lang");
                        String radius = list.getString("radius");
                        String vibration = list.getString("vibration");
                        String sound = list.getString("sound");
                        String push_notification = list.getString("push_notification");

                        if (Integer.parseInt(push_notification) == 1) {
                            pushNotificationSwitch.setChecked(true);
                            StorePreferences.getInstance().setPushNotificationShow(true);
                        } else {
                            pushNotificationSwitch.setChecked(false);
                            StorePreferences.getInstance().setPushNotificationShow(false);
                        }

                        if (Integer.parseInt(sound) == 1) {
                            soundSwitch.setChecked(true);
                            StorePreferences.getInstance().setNotificationSound(true);
                        } else {
                            soundSwitch.setChecked(false);
                            StorePreferences.getInstance().setNotificationSound(false);
                        }

                        if (Integer.parseInt(vibration) == 1) {
                            vibrationSwitch.setChecked(true);
                            StorePreferences.getInstance().setIsVibration(true);
                        } else {
                            vibrationSwitch.setChecked(false);
                            StorePreferences.getInstance().setIsVibration(false);
                        }

                        if (lang.equals("en")) {
                            englishRadioButton.setChecked(true);
                        } else if (lang.equals("es-MX")) {
                            maxicoRadioButton.setChecked(true);
                        } else {
                            spanishRadioButton.setChecked(true);
                        }
                        radiusSeekBar.setProgress(Integer.parseInt(radius));
                        radiusSelectedValueTextView.setText(radius + " KM");

                        StorePreferences.getInstance().setLanguage(lang);
                        StorePreferences.getInstance().setRadius(radius);

                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == settingApplyButton) {
            if (ConnectionDetector.getInstance().isConnectingToInternet()) {

                applySettingApiCall();

            } else {
                ConnectionDetector.getInstance().show_alert(getActivity());
            }
        }
    }

    private void applySettingApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().sendSettings(StorePreferences.getInstance().get_UseId(), push_notification, sound, vibration, lang, StorePreferences.getInstance().getLatitude(),
                StorePreferences.getInstance().getLongitude(), radius, new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(retrofit.client.Response response,
                                        retrofit.client.Response response2) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseStatus = obj_json.getString("response");
                            if (responseStatus.equals("1")) {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                                ((MainActivity) getActivity()).restartActivity();
                            } else {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(getActivity(), responseMessage, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
