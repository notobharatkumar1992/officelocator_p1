package com.officelocator.net;

import com.officelocator.AppDelegate;

import retrofit.RetrofitError;

public abstract class Callback<T> implements retrofit.Callback<T> {
    public abstract void failure(RestError restError);

    @Override
    public void failure(RetrofitError error) {
        try {
            RestError restError = (RestError) error.getBodyAs(RestError.class);
            if (restError != null)
                failure(restError);
            else {
                failure(new RestError(error.getMessage()));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
<<<<<<< HEAD
=======
            failure(new RestError("Please try again later."));
>>>>>>> 274629d5482fca419086c85583e33b08048b0213
        }
    }
}