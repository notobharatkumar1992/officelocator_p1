package com.officelocator.net;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public interface OfficeLocatorAPIInterface {

    @GET("/getHints")
    void getHints(@Query("lang") String lang,
                  Callback<Response> responseCallback);

    @GET("/country")
    void getCountry(@Query("lang") String lang,
                    Callback<Response> responseCallback);

    @GET("/getallCategories")
    void getAllCategories(@Query("lang") String lang,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/userLogin")
    void userLogin(@Field("username") String username,
                   @Field("password") String password,
                   @Field("device_id") String device_id,
                   @Field("device_type") String device_type,
                   Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/userRegistration")
    void userRegistration(@Field("email") String email,
                          @Field("username") String username,
                          @Field("password") String password,
                          @Field("first_name") String first_name,
                          @Field("contact_no") String contact_no,
                          @Field("country") String country,
                          @Field("city") String city,
                          @Field("address") String address,
                          @Field("latitude") String latitude,
                          @Field("longitude") String longitude,
                          @Field("question_id") String question_id,
                          @Field("device_id") String device_id,
                          @Field("device_type") String device_type,
                          @Field("answer") String answer,
                          @Field("password_confirm") String password_confirm,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/forget_password")
    void userForgetPassword(@Field("user_name") String username,
                            @Field("lang") String lang,
                            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/resetpassword")
    void userChangePassword(@Field("user_id") String user_id,
                            @Field("password") String password,
                            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/check_answer")
    void checkUserSecurityAnswer(@Field("user_id") String user_id,
                                 @Field("question_id") String question_id,
                                 @Field("answer") String answer,
                                 Callback<Response> responseCallback);

    @GET("/getBrachviaCategoryId")
    void getBranchByCategory(@Query("category_id") String Category_id,
                             @Query("latitude") String latitude,
                             @Query("longitude") String longitude,
                             @Query("radius") String radius,
                             @Query("lang") String lang,
                             Callback<Response> responseCallback);

    @GET("/getBranchDetail")
    void getBranchDetail(@Query("id") String id,
                         @Query("user_id") String user_id,
                         @Query("lang") String lang,
                         Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/favoriteUpdate")
    void setFavoriteUpdate(@Field("branch_id") String branch_id,
                           @Field("user_id") String user_id,
                           @Field("latitude") String latitude,
                           @Field("longitude") String longitude,
                           @Field("status") String status,
                           Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/getFavorite")
    void getFavoriteList(@Field("user_id") String username,
                         Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/advanceSearch")
    void getAdvanceSearch(@Field("zip") String zip,
                          @Field("radius") String radius,
                          @Field("category_id") String category_id,
                          @Field("name") String name,
                          @Field("latitude") String latitude,
                          @Field("longitude") String longitude,
                          @Field("User_id") String User_id,
                          @Field("lang") String lang,
                          Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/contactUs")
    void sendContactUs(@Field("user_id") String user_id,
                       @Field("email") String email,
                       @Field("subject") String subject,
                       @Field("message") String message,
                       Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/settings")
    void sendSettings(@Field("user_id") String user_id,
                      @Field("push_notification") String push_notification,
                      @Field("sound") String sound,
                      @Field("vibration") String vibration,
                      @Field("lang") String lang,
                      @Field("latitude") String latitude,
                      @Field("longitude") String longitude,
                      @Field("radius") String radius,
                      Callback<Response> responseCallback);

    @GET("/getSettings")
    void getSettingDetails(@Query("user_id") String user_id,
                           Callback<Response> responseCallback);

    @GET("/getallAnnouncement")
    void getAllAnnouncement(@Query("lang") String lang,
                            Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/announcementDetail")
    void getAnnouncementDetail(@Field("id") String id,
                               @Query("lang") String lang,
                               Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/userProfile")
    void getUserProfile(@Field("user_id") String user_id,
                        Callback<Response> responseCallback);

    @Multipart
    @POST("/userEditProfile")
    void sendUserEditProfile(@Part("address") String address,
                             @Part("answer") String answer,
                             @Part("city") String city,
                             @Part("contact_no") String contact_no,
                             @Part("country") String country,
                             @Part("email") String email,
                             @Part("first_name") String first_name,
                             @Part("latitude") String latitude,
                             @Part("longitude") String longitude,
                             @Part("password") String password,
                             @Part("password_confirm") String password_confirm,
                             @Part("question_id") String question_id,
                             @Part("username") String username,
                             @Part("user_id") String user_id,
                             @Part("uploadedfile0") TypedFile ProfilePicture,
                             Callback<Response> responseCallback);


    @FormUrlEncoded
    @POST("/userEditProfile")
    void sendUserEditProfileWithoutImage(@Field("address") String address,
                                         @Field("answer") String answer,
                                         @Field("city") String city,
                                         @Field("contact_no") String contact_no,
                                         @Field("country") String country,
                                         @Field("email") String email,
                                         @Field("first_name") String first_name,
                                         @Field("latitude") String latitude,
                                         @Field("longitude") String longitude,
                                         @Field("password") String password,
                                         @Field("password_confirm") String password_confirm,
                                         @Field("question_id") String question_id,
                                         @Field("username") String username,
                                         @Field("user_id") String user_id,
                                         Callback<Response> responseCallback);

    @FormUrlEncoded
    @POST("/getNearByLocation")
    void sendUpdateLocation(@Field("latitude") String latitude,
                            @Field("longitude") String longitude,
                            @Field("device_id") String device_id,
                            @Field("device_type") String device_type,
                            @Field("user_id") String user_id,
                            @Field("apikey") String apikey,
                            @Field("lang") String lang,
                            Callback<Response> responseCallback);
}

