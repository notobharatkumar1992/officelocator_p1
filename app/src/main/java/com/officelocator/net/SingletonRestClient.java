package com.officelocator.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Dev on 9/15/2015.
 */
public class SingletonRestClient {
    private static OfficeLocatorAPIInterface REST_CLIENT;
    private static final String API_DATE_FORMAT = "yyyy-MM-dd hh:mm:ss";

    private static String ROOT = "http://200.57.131.169/officelocator/user_webservices";
//    private static String ROOT = "http://192.168.100.81/newapp/user_webservices/";

    static {
        setupRestClient();
    }

    private SingletonRestClient() {
    }

    public static OfficeLocatorAPIInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {

        Gson gson = new GsonBuilder()
                .setDateFormat(API_DATE_FORMAT)
                .serializeNulls()
                .create();

        /*RequestInterceptor authenticateInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                String cookieHeader = StorePreferences.getInstance().get_Header();
                if (cookieHeader != null && cookieHeader.length() > 0) {
                    request.addHeader("Authorization", cookieHeader);
                }
            }
        };*/

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                //.setRequestInterceptor(authenticateInterceptor)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(OfficeLocatorAPIInterface.class);
    }
}
