package com.officelocator.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.officelocator.R;
import com.officelocator.util.ConnectionDetector;


/**
 * Created by himanshu on 3/24/2016.
 */
public class PrivacyPolicyActivity extends AppCompatActivity {

    private WebView privacyPolicyWebView;
    private ProgressDialog mProgressDialog;

    //Global variable declaration

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_privacy_policy_activity);
        initView();
    }

    private void initView() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        privacyPolicyWebView = (WebView) findViewById(R.id.privacyPolicyWebView);
        privacyPolicyWebView.getSettings().setJavaScriptEnabled(true);
        privacyPolicyWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

//        mProgressDialog.show();

        privacyPolicyWebView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressDialog.show();
            }

            public void onPageFinished(WebView view, String url) {
                mProgressDialog.dismiss();
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(PrivacyPolicyActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
                mProgressDialog.dismiss();
            }
        });

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            privacyPolicyWebView.loadUrl(getResources().getString(R.string.privacy_policy_url));
        } else {
            ConnectionDetector.getInstance().show_alert(this);
        }
    }
}
