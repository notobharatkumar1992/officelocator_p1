package com.officelocator.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.SearchCategoryListAdapter;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //Control declaration
    private EditText postalCodeEditText;
    private EditText productNameEditText;
    private Spinner categorySpinner;
    private ProgressDialog mProgressDialog;

    //ArrayList declaration
    ArrayList<String> categoryArrayList;
    ArrayList<String> categoryIdArrayList;

    //Global variable declaration
    private String categoryId;
    private String postalCode = "";
    private String productName = "";

    private OnFragmentInteractionListener mListener;
    public Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.new_search_activity);
        mActivity = this;
        initView();
    }


    private void initView() {
        mProgressDialog = new ProgressDialog(SearchActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        postalCodeEditText = (EditText) findViewById(R.id.postalCodeEditText);
        productNameEditText = (EditText) findViewById(R.id.productNameEditText);
        findViewById(R.id.txt_c_search).setOnClickListener(this);

        // Get Category API call
        getCategoriesAPICall();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mListener = null;
        mActivity = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_search:
                if (localValidation()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("categoryId", categoryId);
                    bundle.putString("postalCode", postalCode);
                    bundle.putString("productName", productName);
                    Intent intent = new Intent(SearchActivity.this, TermsSearchActivity.class);
                    intent.putExtras(bundle);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SearchActivity.this, false,
                            new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                            new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchActivity.this, pairs);
                    SearchActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                }
                break;
        }
    }

    /**
     * This Interfaces must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void getCategoriesAPICall() {
        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            mProgressDialog.show();
            SingletonRestClient.get().getAllCategories(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                    mProgressDialog.dismiss();
                }

                @Override
                public void success(Response response, Response response2) {
                    if (mActivity == null)
                        return;
                    mProgressDialog.dismiss();
                    try {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        Boolean responseStatus = obj_json.getBoolean("Status");
                        if (responseStatus) {
                            //String responseMessage = obj_json.getString("responseMessage");
                            JSONArray obJsonArray = obj_json.getJSONArray("list");

                            if (obJsonArray.length() > 0) {
                                categoryArrayList = new ArrayList<String>();
                                categoryIdArrayList = new ArrayList<String>();
                                for (int i = 0; i < obJsonArray.length(); i++) {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    categoryArrayList.add(jsonObject.getString("name"));
                                    categoryIdArrayList.add(jsonObject.getString("id"));
                                }
                            }

                            // Creating adapter for spinner
                            SearchCategoryListAdapter categoryListAdapter = new SearchCategoryListAdapter(SearchActivity.this, categoryArrayList);
                            // attaching data adapter to spinner
                            categorySpinner.setAdapter(categoryListAdapter);

                            categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    categoryId = categoryIdArrayList.get(position);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else {

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });
        } else {
            ConnectionDetector.getInstance().show_alert(SearchActivity.this);
        }
    }

    /**
     * Local validation check for the isEmpty
     *
     * @return true/false according to the validations
     */
    private Boolean localValidation() {

        postalCode = postalCodeEditText.getText().toString().trim();
        productName = productNameEditText.getText().toString().trim();
        Boolean response = true;

       /* if (postalCode.equals("")) {
            postalCodeEditText.setError("Please Enter Postal Code");
            postalCodeEditText.requestFocus();
            response = false;
        }else {
            productNameEditText.setError(null);
            postalCodeEditText.setError(null);
            response = true;
        }*/
        return response;
    }
}
