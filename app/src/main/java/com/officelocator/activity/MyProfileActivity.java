package com.officelocator.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.Interfaces.OnPictureResult;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.CountryListAdapter;
import com.officelocator.adapters.SecurityQuestionsListAdapter;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.service.LocationUpdateService;
import com.officelocator.service.ServiceStatus;
import com.officelocator.util.CircleImageView;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import carbon.widget.ImageView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedFile;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener, OnPictureResult {

    private Spinner securityQuestionSpinner;
    private Spinner countrySpinner;

    private ProgressDialog mProgressDialog;
    private EditText userNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private EditText nameEditText;
    private EditText addressEditText;
    private EditText cityEditText;
    //private EditText countryEditText;
    private EditText contactNoEditText;
    private EditText securityQuestionAnswerEditText;

    // Global variable declaration
    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String Password_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";

    // for email validation
    public Pattern pattern;
    public Matcher matcher;

    private String userName;
    private String email = "";
    private String password;
    private String confirmPassword;
    private String name;
    private String address;
    private String city = "";
    private String contactNo;
    private String securityQuestionAnswer;
    private String securityQuestionId = "0";
    private String countryName = "";
    private String latitude = "";
    private String longitude = "";

    //ArrayList declaration
    ArrayList<String> countryArrayList;
    ArrayList<String> securityQuestionArrayList;
    ArrayList<String> securityQuestionIdArrayList;
    // Util classes declaration
    private ConnectionDetector mConnectionDetector;
    private ServiceStatus mServiceStatus;

    LocationManager locationManager;
    MyLocationListener obj_map_location_listner;
    Location location; // location

    private static final int REQUEST_CODE_LOCATION = 2, REQUEST_CODE_DATA = 4, REQUEST_CODE_GALLERY = 5;

    private ImageView img_c_loading;
    private CircleImageView cimg_user;
    private Handler mHandler;

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_my_profile_activity);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        // Initialization
        obj_map_location_listner = new MyLocationListener();
        mConnectionDetector = ConnectionDetector.getInstance();
        mProgressDialog = new ProgressDialog(MyProfileActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);
        mServiceStatus = new ServiceStatus(MyProfileActivity.this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initView();
                setHandler();
                getUserProfileDataApiCall();
            }
        }, 300);
    }

    private void initView() {
        securityQuestionSpinner = (Spinner) findViewById(R.id.securityQuestionSpinner);
        countrySpinner = (Spinner) findViewById(R.id.countrySpinner);

        // attaching data adapter to spinner

        //countryEditText = (EditText) findViewById(R.id.countryEditText);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);

        img_c_loading = (ImageView) findViewById(R.id.img_c_loading);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);

        userNameEditText = (EditText) findViewById(R.id.userNameEditText);
        userNameEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        emailEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        passwordEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        confirmPasswordEditText = (EditText) findViewById(R.id.confirmPasswordEditText);
        confirmPasswordEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        nameEditText = (EditText) findViewById(R.id.nameEditText);
        nameEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        addressEditText = (EditText) findViewById(R.id.addressEditText);
        addressEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        cityEditText = (EditText) findViewById(R.id.cityEditText);
        cityEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        contactNoEditText = (EditText) findViewById(R.id.contactNoEditText);
        contactNoEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        securityQuestionAnswerEditText = (EditText) findViewById(R.id.securityQuestionAnswerEditText);
        securityQuestionAnswerEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));

        contactNoEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                    textView.clearFocus();
                    securityQuestionSpinner.requestFocus();
                    securityQuestionSpinner.performClick();
                }
                return true;
            }
        });

        cityEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                    textView.clearFocus();
                    countrySpinner.requestFocus();
                    countrySpinner.performClick();
                }
                return true;
            }
        });

        if (ActivityCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Request missing location permission.
            ActivityCompat.requestPermissions(MyProfileActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_LOCATION);
        } else {
            // get current location code
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                buildAlertMessageGps();
            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    StorePreferences.getInstance().setLatitude(latitude);
                    StorePreferences.getInstance().setLongitude(longitude);
                    //new getMyLocationAddress().execute();
                    try {
                        double lat = Double.parseDouble(latitude);
                        double lon = Double.parseDouble(longitude);
                    /*String url = "http://maps.google.com/maps/api/geocode/json?latlng=" + new DecimalFormat("###.####").format(lat) + "," + new DecimalFormat("###.####").format(lon) + "&sensor=true";
                    Log.i("get address from map", url +"===");*/
                        Geocoder gcd = new Geocoder(MyProfileActivity.this, Locale.getDefault());
                        List<Address> addresses = gcd.getFromLocation(lat, lon, 1);
                        if (addresses.size() > 0)
                            System.out.println(addresses.get(0).getLocality());
                        System.out.println(addresses.get(0).getCountryName());
                        city = addresses.get(0).getLocality();
                        countryName = addresses.get(0).getCountryName();

                        cityEditText.setText(city);
                        // countryEditText.setText(countryName);

                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, obj_map_location_listner);
                //new getMyLocationAddress().execute();
                // Get Hints Question API call
                getHintsQuestionsAPICall();
                // Get Country API call
                getCountriesAPICall();
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyProfileActivity.this);
                        break;

                    case 11:
                        AppDelegate.hideProgressDialog(MyProfileActivity.this);
                        break;
                }
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    // get current location code
                    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        buildAlertMessageGps();
                    } else {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = String.valueOf(location.getLatitude());
                            longitude = String.valueOf(location.getLongitude());
                            StorePreferences.getInstance().setLatitude(latitude);
                            StorePreferences.getInstance().setLongitude(longitude);
                            //new getMyLocationAddress().execute();

                            try {
                                double lat = Double.parseDouble(latitude);
                                double lon = Double.parseDouble(longitude);
                    /*String url = "http://maps.google.com/maps/api/geocode/json?latlng=" + new DecimalFormat("###.####").format(lat) + "," + new DecimalFormat("###.####").format(lon) + "&sensor=true";
                    Log.i("get address from map", url +"===");*/
                                Geocoder gcd = new Geocoder(MyProfileActivity.this, Locale.getDefault());
                                List<Address> addresses = gcd.getFromLocation(lat, lon, 1);
                                if (addresses.size() > 0)
                                    System.out.println(addresses.get(0).getLocality());
                                System.out.println(addresses.get(0).getCountryName());
                                city = addresses.get(0).getLocality();
                                countryName = addresses.get(0).getCountryName();

                                cityEditText.setText(city);
                                // countryEditText.setText(countryName);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, obj_map_location_listner);
                        //new getMyLocationAddress().execute();
                        // Get Hints Question API call
                        getHintsQuestionsAPICall();
                        // Get Country API call
                        getCountriesAPICall();
                    }
                }
            } else {
                // Permission was denied. Display an error message.
            }
        } else if (requestCode == REQUEST_CODE_DATA) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.WRITE_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    new OpenCamera().execute();
                }
            } else {
                // Permission was denied. Display an error message.
            }
        } else if (requestCode == REQUEST_CODE_GALLERY) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.WRITE_EXTERNAL_STORAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                }
            } else {
                // Permission was denied. Display an error message.
            }
        }
    }

    private void buildAlertMessageGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.gps_disable_alert_message))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_disable_alert_yes_button_title), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.gps_disable_alert_no_button_title), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void getUserProfileDataApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().getUserProfile(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response, Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseCountry = obj_json.getString("response");
                    if (responseCountry.equals("1")) {
                        AppDelegate.LogUR(obj_json.toString());
                        //String responseMessage = obj_json.getString("responseMessage");
                        JSONObject output = obj_json.getJSONObject("output");
                        if (output.getString("username") != null) {
                            userNameEditText.setText(output.getString("username"));
                        }
                        if (output.getString("first_name") != null && output.getString("last_name") != null) {
                            nameEditText.setText(output.getString("first_name") + " " + output.getString("last_name"));
                        }
                        if (output.getString("email") != null) {
                            emailEditText.setText(output.getString("email"));
                        }
                        if (output.getString("address") != null) {
                            addressEditText.setText(output.getString("address"));
                        }
                        if (output.getString("city") != null) {
                            cityEditText.setText(output.getString("city"));
                        }
                        if (output.getString("country") != null) {
                            countryName = output.getString("country");
                            //countryEditText.setText(output.getString("country"));
                        }
                        if (output.getString("contact_no") != null) {
                            contactNoEditText.setText(output.getString("contact_no"));
                        }
                        if (output.getString("question_id") != null) {
                            securityQuestionId = output.getString("question_id");
                        }
                        if (output.getString("answer") != null) {
                            securityQuestionAnswerEditText.setText(output.getString("answer"));
                        }
                        if (output.getString("latitude") != null) {
                            latitude = output.getString("latitude");
                        }
                        if (output.getString("longitude") != null) {
                            longitude = output.getString("longitude");
                        }
                        if (output.getString("logo") != null) {
                            StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));

//                            ((MainActivity) MyProfileActivity.this).userImageLogoRefresh();
//                            Picasso.with(MyProfileActivity.this)
//                                    .load(output.getString("logo"))
//                                    .error(R.drawable.sample_bg)
//                                    .placeholder(R.drawable.sample_bg)
//                                    .into(cimg_user);

                            img_c_loading.setVisibility(View.VISIBLE);
                            AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading.getDrawable();
                            frameAnimation.setCallback(img_c_loading);
                            frameAnimation.setVisible(true, true);
                            frameAnimation.start();

                            imageLoader.loadImage(output.getString("logo"), options, new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.sample_bg));
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                    cimg_user.setImageBitmap(bitmap);
                                    img_c_loading.setVisibility(View.GONE);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {

                                }
                            });

                        }

                        // Get Hints Question API call
                        getHintsQuestionsAPICall();

                        // Get Country API call
                        getCountriesAPICall();

                    } else {

                    }
                } catch (JSONException e) {
                    AppDelegate.LogE(e);
                }
            }
        });
    }

    private void getCountriesAPICall() {
        if (mConnectionDetector.isConnectingToInternet()) {
            SingletonRestClient.get().getCountry(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        String responseCountry = obj_json.getString("response");
                        if (responseCountry.equals("1")) {
                            //String responseMessage = obj_json.getString("responseMessage");
                            JSONArray obJsonArray = obj_json.getJSONArray("output");

                            if (obJsonArray.length() > 0) {
                                countryArrayList = new ArrayList<String>();
                                countryArrayList.add(getResources().getString(R.string.select_country_hint));
                                for (int i = 0; i < obJsonArray.length(); i++) {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    Log.d("test", "country => " + jsonObject.getString("country"));
                                    countryArrayList.add(jsonObject.getString("country"));
                                }
                            }


                            // Creating adapter for spinner
                            CountryListAdapter countryListAdapter = new CountryListAdapter(MyProfileActivity.this, countryArrayList);

                            // attaching data adapter to spinner
                            countrySpinner.setAdapter(countryListAdapter);

                            Log.e("countryName location", countryName);

                            countrySpinner.setSelection(getCategoryPos(countryName));

                            countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    countryName = countryArrayList.get(position).toString();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else {

                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });
        } else {
            mConnectionDetector.show_alert(MyProfileActivity.this);
        }
    }

    private void getHintsQuestionsAPICall() {
        if (mConnectionDetector.isConnectingToInternet()) {
            SingletonRestClient.get().getHints(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
                @Override
                public void failure(RestError restError) {
                }

                @Override
                public void success(Response response, Response response2) {
                    try {
                        JSONObject obj_json = new JSONObject(
                                new String(((TypedByteArray) response.getBody()).getBytes()));
                        Boolean responseStatus = obj_json.getBoolean("Status");
                        if (!responseStatus) {
                            //String responseMessage = obj_json.getString("responseMessage");

                        } else {
                            String Message = obj_json.getString("Message");
                            Log.i("Message", Message + "==");

                            JSONArray obJsonArray = obj_json.getJSONArray("list");

                            if (obJsonArray.length() > 0) {
                                securityQuestionArrayList = new ArrayList<String>();
                                securityQuestionIdArrayList = new ArrayList<String>();
                                securityQuestionArrayList.add(getResources().getString(R.string.select_questions_hint));
                                securityQuestionIdArrayList.add("0");
                                for (int i = 0; i < obJsonArray.length(); i++) {
                                    JSONObject jsonObject = obJsonArray.getJSONObject(i);
                                    securityQuestionArrayList.add(jsonObject.getString("name"));
                                    securityQuestionIdArrayList.add(jsonObject.getString("id"));
                                }
                            }

                            // Creating adapter for spinner
                            SecurityQuestionsListAdapter securityQuestionAdapter = new SecurityQuestionsListAdapter(MyProfileActivity.this, securityQuestionArrayList);

                            // attaching data adapter to spinner
                            securityQuestionSpinner.setAdapter(securityQuestionAdapter);
                            securityQuestionSpinner.setSelection(getSecurityQuestionPos(securityQuestionId));

                            securityQuestionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    securityQuestionId = securityQuestionIdArrayList.get(position).toString();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });


                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        //AlertDialog("Server Error! Please try again.");
                    }
                }
            });

        } else {
            mConnectionDetector.show_alert(MyProfileActivity.this);
        }
    }

    private int getSecurityQuestionPos(String SecurityQuestionId) {
        return securityQuestionIdArrayList.indexOf(SecurityQuestionId);
    }

    /**
     * Local validation check for the isEmpty
     *
     * @return true/false according to the validations
     */
    private Boolean local_validation() {

        userName = userNameEditText.getText().toString().trim();
        email = emailEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();
        confirmPassword = confirmPasswordEditText.getText().toString().trim();
        name = nameEditText.getText().toString().trim();
        address = addressEditText.getText().toString().trim();
        city = cityEditText.getText().toString().trim();
        contactNo = contactNoEditText.getText().toString().trim();
        securityQuestionAnswer = securityQuestionAnswerEditText.getText().toString().trim();

        Boolean response = false;

        if (userName.equals("")) {
            userNameEditText.setError(getResources().getString(R.string.profile_user_name_local_validation_message));
            userNameEditText.requestFocus();
            response = false;
        }/* else if (email.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError("Please Enter Email");
            emailEditText.requestFocus();
            response = false;
        }  else if (password.equals("")) {
            userNameEditText.setError(null);
            //emailEditText.setError(null);
            passwordEditText.setError(getResources().getString(R.string.password_local_validation_message));
            passwordEditText.requestFocus();
            response = false;
        } else if (confirmPassword.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(getResources().getString(R.string.confirm_password_local_validation_message));
            confirmPasswordEditText.requestFocus();
            response = false;
        } */ else if (name.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(null);
            nameEditText.setError(getResources().getString(R.string.profile_name_local_validation_message));
            nameEditText.requestFocus();
            response = false;
        } else if (address.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(getResources().getString(R.string.profile_address_local_validation_message));
            addressEditText.requestFocus();
            response = false;
        } else if (city.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(null);
            cityEditText.setError(getResources().getString(R.string.profile_city_local_validation_message));
            cityEditText.requestFocus();
            response = false;
        } else if (contactNo.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(null);
            cityEditText.setError(null);
            contactNoEditText.setError(getResources().getString(R.string.profile_contact_no_local_validation_message));
            contactNoEditText.requestFocus();
            response = false;
        } else if (countryName.equals(getResources().getString(R.string.select_country_hint))) {
            Toast.makeText(MyProfileActivity.this, getResources().getString(R.string.profile_select_country_local_validation_message), Toast.LENGTH_LONG).show();
            response = false;

        } else if (securityQuestionId.equals("0")) {
            Toast.makeText(MyProfileActivity.this, getResources().getString(R.string.profile_select_security_question_local_validation_message), Toast.LENGTH_LONG).show();
            response = false;

        } else if (securityQuestionAnswer.equals("")) {
            userNameEditText.setError(null);
            emailEditText.setError(null);
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(null);
            nameEditText.setError(null);
            addressEditText.setError(null);
            cityEditText.setError(null);
            contactNoEditText.setError(null);
            securityQuestionAnswerEditText.setError(getResources().getString(R.string.security_question_answer_local_validation_message));
            securityQuestionAnswerEditText.requestFocus();
            response = false;
        } else {

            response = true;
        }
        return response;
    }

    /**
     * To check that email address is valid or not
     *
     * @return true/false according to the validations
     */
    private boolean isValidEmail() {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        if (!email.equals("")) {
            if (matcher.matches()) {
                emailEditText.setError(null);
                return true;
            } else {
                emailEditText.setError(getResources().getString(R.string.email_local_validation_message));
                emailEditText.requestFocus();
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * To check that password and Confirm Password is matched or not
     *
     * @return true/false according to the validations
     */
    private boolean isPasswordMatch() {
        if (password.equals(confirmPassword)) {
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(null);
            return true;
        } else {
            confirmPasswordEditText.setError(getResources().getString(R.string.password_no_match_local_validation_message));
            confirmPasswordEditText.requestFocus();
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                Log.e("latitude", latitude + "=");
                Log.e("longitude", longitude + "=");
                if (local_validation()) {
                    if (isValidEmail()) {
                        if (isValidPassword()) {
                            if (isPasswordMatch()) {
                                if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                                    updateMyProfileApiCall();
                                } else {
                                    ConnectionDetector.getInstance().show_alert(MyProfileActivity.this);
                                }
                            }
                        }
                    }
                }
                break;

            case R.id.cimg_user:
                showImageSelectorList();
                break;
        }
    }

    private void updateMyProfileApiCall() {
        mProgressDialog.show();
        if (password.equals("")) {
            password = StorePreferences.getInstance().getUserPassword();
        }
        if (confirmPassword.equals("")) {
            confirmPassword = StorePreferences.getInstance().getUserPassword();
        }
        TypedFile typedFileCurrentPhotoPath;
        if (capturedFile != null) {
            typedFileCurrentPhotoPath = new TypedFile("multipart/form-data", new File(String.valueOf(capturedFile)));
            SingletonRestClient.get().sendUserEditProfile(address, securityQuestionAnswer, city, contactNo,
                    countryName, email, name, latitude, longitude, password, confirmPassword, securityQuestionId,
                    userName, StorePreferences.getInstance().get_UseId(), typedFileCurrentPhotoPath, new Callback<Response>() {
                        @Override
                        public void failure(RestError restError) {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void success(Response response, Response response2) {
                            mProgressDialog.dismiss();
                            try {
                                JSONObject obj_json = new JSONObject(
                                        new String(((TypedByteArray) response.getBody()).getBytes()));
                                String responseCountry = obj_json.getString("response");
                                if (responseCountry.equals("1")) {
                                    StorePreferences.getInstance().setUserName(userName);
                                    StorePreferences.getInstance().setEmailId(email);
                                    //if (StorePreferences.getInstance().getIsRememberMe()) {
                                    StorePreferences.getInstance().setUserPassword(password);
                                    // }
                                    JSONObject output = obj_json.getJSONObject("output");
                                    if (output.getString("logo") != null) {
                                        StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));
//                                        ((MainActivity) MyProfileActivity.this).userImageLogoRefresh();
                                    }
                                    if (OfficeLocatorDatabase.getInstance(MyProfileActivity.this).IsUserInfoExist()) {
                                        OfficeLocatorDatabase.getInstance(MyProfileActivity.this).deleteAllUserInfo();
                                        OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertUserInfo(userName,
                                                password, StorePreferences.getInstance().get_UseId());
                                    } else {
                                        OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertUserInfo(userName,
                                                password, StorePreferences.getInstance().get_UseId());
                                    }
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(MyProfileActivity.this, responseMessage, Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(MyProfileActivity.this, MainActivity.class);
                                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(MyProfileActivity.this, false,
                                            new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MyProfileActivity.this, pairs);
                                    MyProfileActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                                    finish();
                                } else {
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(MyProfileActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } else {

            SingletonRestClient.get().sendUserEditProfileWithoutImage(address, securityQuestionAnswer, city, contactNo,
                    countryName, email, name, latitude, longitude, password, confirmPassword, securityQuestionId,
                    userName, StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
                        @Override
                        public void failure(RestError restError) {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void success(Response response, Response response2) {
                            mProgressDialog.dismiss();
                            try {
                                JSONObject obj_json = new JSONObject(
                                        new String(((TypedByteArray) response.getBody()).getBytes()));
                                String responseCountry = obj_json.getString("response");
                                if (responseCountry.equals("1")) {
                                    StorePreferences.getInstance().setUserName(userName);
                                    StorePreferences.getInstance().setEmailId(email);
                                    //if (StorePreferences.getInstance().getIsRememberMe()) {
                                    StorePreferences.getInstance().setUserPassword(confirmPassword);
                                    // }
                                    JSONObject output = obj_json.getJSONObject("output");
                                    if (output.getString("logo") != null) {
                                        StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));
//                                        ((MainActivity) MyProfileActivity.this).userImageLogoRefresh();
                                    }
                                    if (OfficeLocatorDatabase.getInstance(MyProfileActivity.this).IsUserInfoExist()) {
                                        OfficeLocatorDatabase.getInstance(MyProfileActivity.this).deleteAllUserInfo();
                                        OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertUserInfo(userName,
                                                confirmPassword, StorePreferences.getInstance().get_UseId());
                                    } else {
                                        OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertUserInfo(userName,
                                                confirmPassword, StorePreferences.getInstance().get_UseId());
                                    }
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(MyProfileActivity.this, responseMessage, Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(MyProfileActivity.this, MainActivity.class);
                                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(MyProfileActivity.this, false,
                                            new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MyProfileActivity.this, pairs);
                                    MyProfileActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
//                                LoginActivity.finishActivity();
                                    finish();

                                } else {
                                    String responseMessage = obj_json.getString("Message");
                                    Toast.makeText(MyProfileActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                //AlertDialog("Server Error! Please try again.");
                            }
                        }
                    });
        }
    }

    private boolean isValidPassword() {
        if (password.length() < 6) {
            passwordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
            passwordEditText.requestFocus();
            return false;
        } else if (confirmPassword.length() < 6) {
            passwordEditText.setError(null);
            confirmPasswordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
            confirmPasswordEditText.requestFocus();
            return false;
        } else if (!isMatchPassword()) {
            passwordEditText.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
            passwordEditText.requestFocus();
            return false;
        } else {
            confirmPasswordEditText.setError(null);
            return true;
        }
    }

    private Boolean isMatchPassword() {
        pattern = Pattern.compile(Password_PATTERN);
        matcher = pattern.matcher(password);

        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private void userRegistrationApiCall() {
        mProgressDialog.show();

        String device_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        AppDelegate.LogT("device_id => " + device_id);
        SingletonRestClient.get().userRegistration(email, userName, password, name, contactNo, countryName,
                city, address, latitude, longitude, securityQuestionId, device_id, "A", securityQuestionAnswer, confirmPassword, new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(Response response,
                                        Response response2) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            String responseCountry = obj_json.getString("response");
                            AppDelegate.LogT("Sign up Response==" + responseCountry);
                            if (responseCountry.equals("1")) {
                                JSONObject output = obj_json.getJSONObject("output");
                                String userID = output.getString("id");
                                String email = output.getString("email");
                                String username = output.getString("username");
                                StorePreferences.getInstance().set_UserId(userID);
                                StorePreferences.getInstance().setEmailId(email);
                                StorePreferences.getInstance().setUserName(username);
                                StorePreferences.getInstance().setUserPassword(password);

                                //local database implement
                                getFavoriteListApiCall();
                                if (OfficeLocatorDatabase.getInstance(MyProfileActivity.this).IsUserInfoExist()) {
                                    OfficeLocatorDatabase.getInstance(MyProfileActivity.this).deleteAllUserInfo();
                                    OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertUserInfo(userName, password, userID);
                                } else {
                                    OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertUserInfo(userName, password, userID);
                                }

                                if (!mServiceStatus.isLocationUpdateServiceRunning()) {
                                    startService(new Intent(MyProfileActivity.this, LocationUpdateService.class));
                                }

                                Intent intent = new Intent(MyProfileActivity.this, MainActivity.class);
                                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(MyProfileActivity.this, false,
                                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MyProfileActivity.this, pairs);
                                MyProfileActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
//                                LoginActivity.finishActivity();
                                finish();

                            } else {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(MyProfileActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            AppDelegate.LogE(e);
                        }
                    }
                });
    }

    //get android device Mac address
    public String getMacAddress() {
        String macAddress = "";
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        macAddress = wInfo.getMacAddress();
        if (macAddress == null) {
            macAddress = "";
        }
        return macAddress;
    }

    public class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location loc) {

            latitude = String.valueOf(loc.getLatitude());
            longitude = String.valueOf(loc.getLongitude());
            //locationManager.removeUpdates(obj_map_location_listner);

        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private int getCategoryPos(String category) {
        return countryArrayList.indexOf(category);
    }

    //get favorite api call
    private void getFavoriteListApiCall() {
        SingletonRestClient.get().getFavoriteList(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(Response response,
                                Response response2) {
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        if (OfficeLocatorDatabase.getInstance(MyProfileActivity.this).IsFavoriteListExist()) {
                            OfficeLocatorDatabase.getInstance(MyProfileActivity.this).deleteAllFavoriteList();
                            OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertFavoriteList(obj_json.toString());
                        } else {
                            OfficeLocatorDatabase.getInstance(MyProfileActivity.this).insertFavoriteList(obj_json.toString());
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void set_locale(String lan) {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources()
                .updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }


    /* image selection*/
    public static File capturedFile;
    public static Uri imageURI = null;

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(MyProfileActivity.this);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MyProfileActivity.this);
        ListView modeList = new ListView(MyProfileActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(MyProfileActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        switch (i) {
                            case 0:
                                dialog.dismiss();
                                if (ActivityCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    // Request missing external storage permission.
                                    ActivityCompat.requestPermissions(MyProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_DATA);
                                } else {
                                    new OpenCamera().execute();
                                }
                                break;
                            case 1:
                                dialog.dismiss();
                                if (ActivityCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    // Request missing external storage permission.
                                    ActivityCompat.requestPermissions(MyProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_GALLERY);
                                } else {
                                    openGallery();
                                }
                                break;
                            case 2:
                                dialog.dismiss();
                                break;
                        }
                    }
                }

        );
        dialog.show();
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                Bitmap OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                cimg_user.setImageBitmap(OriginalPhoto);
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile(MyProfileActivity.this);
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(MyProfileActivity.this, getString(R.string.file_not_created));
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public static String getNewFile(Context mContext) {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getString(R.string.app_name));
        } else {
            directoryFile = mContext.getDir(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppDelegate.SELECT_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(MyProfileActivity.this, data.getData());
                } else {
                    Toast.makeText(MyProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(MyProfileActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        capturedFile = new File(mActivity.getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(capturedFile));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }


    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(00);

//        if (SellItemFragment.onPictureResult != null) {
//            options.setHideBottomControls(false);
//            options.setFreeStyleCropEnabled(false);
//        } else {
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
//        }


        return uCrop.withOptions(options);
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void writeImageFile(Bitmap bitmap, String filePath) {
        if (AppDelegate.isValidString(filePath)) {
            capturedFile = new File(filePath);
        } else {
            return;
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }
}
