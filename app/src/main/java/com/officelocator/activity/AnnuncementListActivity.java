package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.Constants.Tags;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.AnnuncementHorizontalAdapter;
import com.officelocator.adapters.PagerAdapter;
import com.officelocator.fragments.BannerHomeFragment;
import com.officelocator.model.AnnouncementListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.MyViewPager;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class AnnuncementListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    public Handler mHandler;

    private ProgressDialog mProgressDialog;
    ArrayList<AnnouncementListData.list> AnnouncementArrayList = new ArrayList<>();

    RecyclerView horizontal_recycler_view;
    AnnuncementHorizontalAdapter horizontalAdapter;

    PagerAdapter bannerPagerAdapter;
    private carbon.widget.ImageView img_c_arrow_left, img_c_arrow_right;
    private MyViewPager view_pager_banner;
    private ArrayList<Fragment> bannerFragment = new ArrayList<>();
    private android.widget.LinearLayout pager_indicator;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_anauncement_list_activity);
        // Initialization
        initView();
        setHandler();
        getAnnouncementApiCall();
    }

    private void initView() {
        ((TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        img_c_arrow_left = (carbon.widget.ImageView) findViewById(R.id.img_c_arrow_left);
        img_c_arrow_left.setOnClickListener(this);
        img_c_arrow_right = (carbon.widget.ImageView) findViewById(R.id.img_c_arrow_right);
        img_c_arrow_right.setOnClickListener(this);
        findViewById(R.id.txt_c_next).setOnClickListener(this);
        mProgressDialog = new ProgressDialog(AnnuncementListActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);
        pager_indicator = (android.widget.LinearLayout) findViewById(R.id.pager_indicator);
        view_pager_banner = (MyViewPager) findViewById(R.id.view_pager_banner);
        setUiPageViewController();
        view_pager_banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                item_position = position;
                try {
                    switchBannerPage(position % bannerFragment.size());
                    horizontal_recycler_view.smoothScrollToPosition(position);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_arrow_left:
                view_pager_banner.setCurrentItem(item_position - 1);
                break;
            case R.id.img_c_arrow_right:
                view_pager_banner.setCurrentItem(item_position + 1);
                break;
            case R.id.txt_c_next:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.slider_id, AnnouncementArrayList.get(item_position));
                bundle.putString("announcementId", AnnouncementArrayList.get(item_position).id);
                Intent intent = new Intent(AnnuncementListActivity.this, AnnouncementDetailActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(AnnuncementListActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.txt_c_createOneTextView), getString(R.string.top_title)),
                        new Pair<>(view_pager_banner, getString(R.string.image)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(AnnuncementListActivity.this, pairs);
                intent.putExtras(bundle);
                AnnuncementListActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                break;
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        break;
                    case 11:
                        break;
                    case 1:
                        for (int i = 0; i < AnnouncementArrayList.size(); i++) {
                            Fragment fragment = new BannerHomeFragment();
                            Bundle bundle = new Bundle();
                            bundle.putParcelable(Tags.slider_id, AnnouncementArrayList.get(i));
                            fragment.setArguments(bundle);
                            bannerFragment.add(fragment);
                        }
                        setProductImageInView(AnnouncementArrayList);
                        bannerPagerAdapter = new PagerAdapter(getSupportFragmentManager(), bannerFragment);
                        view_pager_banner.setAdapter(bannerPagerAdapter);
                        setUiPageViewController();
                        break;
                }
            }
        };
    }

    private ImageView[] dots;

    private void switchBannerPage(int position) {
        try {
            if (dotsCount > 0) {
                for (int i = 0; i < dotsCount; i++) {
                    if (dots != null && dots.length > i)
                        dots[i].setImageResource(R.drawable.white_radius_square);
                }
                if (dots != null && dots.length > position)
                    dots[position].setImageResource(R.drawable.yellow_radius_square);
                AppDelegate.LogT("switchBannerPage => position => " + position + ", dotsCount => " + dotsCount);
                img_c_arrow_left.setVisibility(position == 0 ? View.GONE : View.VISIBLE);
                AppDelegate.LogT("right => " + (position + 1) + " == " + dotsCount);

                img_c_arrow_right.setVisibility(position + 1 == dotsCount ? View.GONE : View.VISIBLE);
//                if (position + 1 == dotsCount) {
//                    img_c_arrow_right.setVisibility(View.GONE);
//                    img_c_arrow_left.setVisibility(View.VISIBLE);
//                } else if (position == 0) {
//                    img_c_arrow_right.setVisibility(View.VISIBLE);
//                    img_c_arrow_left.setVisibility(View.GONE);
//                }
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(AppDelegate.dpToPix(AnnuncementListActivity.this, 10), AppDelegate.dpToPix(AnnuncementListActivity.this, 10));
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(AppDelegate.dpToPix(AnnuncementListActivity.this, 6), AppDelegate.dpToPix(AnnuncementListActivity.this, 6));
                    params.setMargins(AppDelegate.dpToPix(AnnuncementListActivity.this, 4), 0, AppDelegate.dpToPix(AnnuncementListActivity.this, 4), 0);
                } else
                    params.setMargins(AppDelegate.dpToPix(AnnuncementListActivity.this, 3), 0, AppDelegate.dpToPix(AnnuncementListActivity.this, 3), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }
    }


    private void setProductImageInView(ArrayList<AnnouncementListData.list> arrayList) {
        horizontal_recycler_view = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        horizontalAdapter = new AnnuncementHorizontalAdapter(arrayList, getApplication(), this);
        horizontal_recycler_view.setLayoutManager(new LinearLayoutManager(AnnuncementListActivity.this, LinearLayoutManager.HORIZONTAL, false));
        horizontal_recycler_view.setAdapter(horizontalAdapter);
    }

    private void getAnnouncementApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().getAllAnnouncement(StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {
                        JSONArray list = new JSONArray(obj_json.getString("list"));
                        if (list.length() > 0) {
                            AnnouncementArrayList.clear();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            AnnouncementListData mAnnouncementListData = gson.fromJson(String.valueOf(new String(((TypedByteArray) response.getBody()).getBytes())), AnnouncementListData.class);

                            AnnouncementArrayList = mAnnouncementListData.list;
                            AnnouncementArrayList.addAll(mAnnouncementListData.list);
                            AnnouncementArrayList.addAll(mAnnouncementListData.list);
                            AnnouncementArrayList.addAll(mAnnouncementListData.list);
                            mHandler.sendEmptyMessage(1);
                            Log.i("AnnouncementArrayList", AnnouncementArrayList.size() + "==");
                        }
                    } else {
                        String responseMessage = obj_json.getString("Message");
//                        announcementListView.setVisibility(View.GONE);
//                        announcementErrorMessageTextView.setVisibility(View.VISIBLE);
//                        announcementErrorMessageTextView.setText(responseMessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void set_locale(String lan) {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        view_pager_banner.setCurrentItem(position);
    }
}
