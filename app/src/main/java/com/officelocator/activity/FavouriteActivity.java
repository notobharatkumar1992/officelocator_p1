package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.adapters.FavoriteListAdapter;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.fragments.FavouiteMapFragment;
import com.officelocator.model.FavoriteListData;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class FavouriteActivity extends AppCompatActivity implements OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ProgressDialog mProgressDialog;

    //ArrayList declaration
    ArrayList<FavoriteListData.list> FavoriteArrayList = new ArrayList<>();

    private ListView favoriteListView;
    private TextView txt_c_createOneTextView, favoriteErrorMessageTextView;

    FavoriteListAdapter mFavoriteListAdapter;

    private FrameLayout map_container;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        AppDelegate.set_locale(this, StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_favourite_activity);
        initView();
    }

    private void initView() {
        mProgressDialog = new ProgressDialog(FavouriteActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        favoriteListView = (ListView) findViewById(R.id.favoriteListView);

        txt_c_createOneTextView = (TextView) findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);
        favoriteErrorMessageTextView = (TextView) findViewById(R.id.favoriteErrorMessageTextView);
        favoriteErrorMessageTextView.setVisibility(View.GONE);
        map_container = (FrameLayout) findViewById(R.id.map_container);

        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
            getFavoriteListApiCall();
        } else {
            getFavoriteFromOffline();
        }

        findViewById(R.id.txt_c_list).setOnClickListener(this);
        findViewById(R.id.txt_c_map).setOnClickListener(this);

//        mapDisplayLinearLayout = (LinearLayout) findViewById(R.id.mapDisplayLinearLayout);
//        listDisplayLinearLayout = (LinearLayout) findViewById(R.id.listDisplayLinearLayout);
//        mapLabelTextView = (TextView) findViewById(R.id.mapLabelTextView);
//        listLabelTextView = (TextView) findViewById(R.id.listLabelTextView);
//        mapDisplayLinearLayout.setOnClickListener(this);
//        listDisplayLinearLayout.setOnClickListener(this);

    }

    private void addMarkers() {
        try {
            FavouiteMapFragment mapFragment = new FavouiteMapFragment(FavoriteArrayList);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.map_container, mapFragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getFavoriteListApiCall() {

        mProgressDialog.show();
        SingletonRestClient.get().getFavoriteList(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        if (OfficeLocatorDatabase.getInstance(FavouriteActivity.this).IsFavoriteListExist()) {
                            OfficeLocatorDatabase.getInstance(FavouriteActivity.this).deleteAllFavoriteList();
                            OfficeLocatorDatabase.getInstance(FavouriteActivity.this).insertFavoriteList(obj_json.toString());
                        } else {
                            OfficeLocatorDatabase.getInstance(FavouriteActivity.this).insertFavoriteList(obj_json.toString());
                        }


                        JSONArray list = new JSONArray(obj_json.getString("list"));

                        if (list.length() > 0) {
                            favoriteListView.setVisibility(View.VISIBLE);
                            favoriteErrorMessageTextView.setVisibility(View.GONE);
                            FavoriteArrayList = new ArrayList<FavoriteListData.list>();
                            GsonBuilder builder = new GsonBuilder();
                            Gson gson = builder.create();

                            FavoriteListData mFavoriteListData = gson.fromJson(String.valueOf(
                                    new String(((TypedByteArray) response.getBody()).getBytes())), FavoriteListData.class);

                            FavoriteArrayList = mFavoriteListData.list;

                            Log.i("FavoriteListData", FavoriteArrayList.size() + "==");

                            int count = 0;
                            for (int i = 0; i < FavoriteArrayList.size(); i++) {
                                FavoriteArrayList.get(i).value = count;
                                if (count == AppDelegate.TILES_COLOR_COUNT) {
                                    count = 0;
                                } else {
                                    count++;
                                }
                            }

                            mFavoriteListAdapter = new FavoriteListAdapter(FavouriteActivity.this, FavoriteArrayList);
                            favoriteListView.setAdapter(mFavoriteListAdapter);
                            favoriteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("Id", FavoriteArrayList.get(position).id);
                                    bundle.putString("BranchId", FavoriteArrayList.get(position).category_id);
                                    bundle.putInt("color", FavoriteArrayList.get(position).value);

                                    Intent intent = new Intent(FavouriteActivity.this, BranchDetailsActivity.class);
                                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(FavouriteActivity.this, false,
                                            new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(FavouriteActivity.this, pairs);
                                    intent.putExtras(bundle);
                                    startActivity(intent, transitionActivityOptions.toBundle());
                                }
                            });
                        }

                    } else {
                        String responseMessage = obj_json.getString("Message");
                        favoriteListView.setVisibility(View.GONE);
                        favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
                        favoriteErrorMessageTextView.setText(responseMessage);
                    }
                    addMarkers();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txt_c_map) {
            map_container.setVisibility(View.VISIBLE);
            favoriteListView.setVisibility(View.GONE);
            favoriteErrorMessageTextView.setVisibility(View.GONE);

//            mapDisplayLinearLayout.setBackgroundColor(FavouriteActivity.this.getResources().getColor(R.color.colorPrimary));
//            listDisplayLinearLayout.setBackgroundColor(FavouriteActivity.this.getResources().getColor(R.color.white));
//
//            mapLabelTextView.setTextColor(FavouriteActivity.this.getResources().getColor(R.color.white));
//            listLabelTextView.setTextColor(FavouriteActivity.this.getResources().getColor(R.color.colorPrimary));

        } else if (v.getId() == R.id.txt_c_list) {
            favoriteListView.setVisibility(View.VISIBLE);
            map_container.setVisibility(View.GONE);

//            mapDisplayLinearLayout.setBackgroundColor(FavouriteActivity.this.getResources().getColor(R.color.white));
//            listDisplayLinearLayout.setBackgroundColor(FavouriteActivity.this.getResources().getColor(R.color.colorPrimary));
//
//            mapLabelTextView.setTextColor(FavouriteActivity.this.getResources().getColor(R.color.colorPrimary));
//            listLabelTextView.setTextColor(FavouriteActivity.this.getResources().getColor(R.color.white));

            if (FavoriteArrayList.size() > 0) {

            } else {
                if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                    getFavoriteListApiCall();
                } else {
                    ConnectionDetector.getInstance().show_alert(FavouriteActivity.this);
                }
            }
        }
    }


    private void getFavoriteFromOffline() {
        if (OfficeLocatorDatabase.getInstance(FavouriteActivity.this).IsFavoriteListExist()) {
            String response = OfficeLocatorDatabase.getInstance(FavouriteActivity.this).GetAllFavoriteList();
            Log.i("test", "local db response get favorite " + response + "=");
            try {
                JSONObject jsonObject = new JSONObject(response);

                String favoriteList = jsonObject.getString("favoriteList");
                JSONObject obj_json = new JSONObject(favoriteList);
                String responseStatus = obj_json.getString("Status");
                if (responseStatus.equals("true")) {

                    JSONArray list = new JSONArray(obj_json.getString("list"));

                    if (list.length() > 0) {
                        favoriteListView.setVisibility(View.VISIBLE);
                        favoriteErrorMessageTextView.setVisibility(View.GONE);
                        FavoriteArrayList = new ArrayList<FavoriteListData.list>();
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();

                        FavoriteListData mFavoriteListData = gson.fromJson(favoriteList, FavoriteListData.class);

                        FavoriteArrayList = mFavoriteListData.list;

                        Log.i("FavoriteListData", FavoriteArrayList.size() + "==");

                        mFavoriteListAdapter = new FavoriteListAdapter(FavouriteActivity.this, FavoriteArrayList);
                        favoriteListView.setAdapter(mFavoriteListAdapter);
                        favoriteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                Bundle bundle = new Bundle();
                                bundle.putString("Id", FavoriteArrayList.get(position).id);
                                bundle.putString("BranchId", FavoriteArrayList.get(position).category_id);
                                bundle.putInt("color", FavoriteArrayList.get(position).value);

                                Intent intent = new Intent(FavouriteActivity.this, BranchDetailsActivity.class);
                                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(FavouriteActivity.this, false,
                                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(FavouriteActivity.this, pairs);
                                intent.putExtras(bundle);
                                startActivity(intent, transitionActivityOptions.toBundle());

                            }
                        });
                    }

                } else {
                    String responseMessage = obj_json.getString("Message");
                    favoriteListView.setVisibility(View.GONE);
                    favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
                    favoriteErrorMessageTextView.setText(responseMessage);
                }
                addMarkers();

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            favoriteListView.setVisibility(View.GONE);
            favoriteErrorMessageTextView.setVisibility(View.VISIBLE);
            favoriteErrorMessageTextView.setText(getResources().getString(R.string.detail_branch_no_data_message));
        }
    }
}
