package com.officelocator.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.fragments.CategoryFragment;
import com.officelocator.fragments.FragmentDrawer;
import com.officelocator.fragments.MyProfileFragment;
import com.officelocator.fragments.NewFragmentDrawer;
import com.officelocator.service.LocationUpdateService;
import com.officelocator.service.ServiceStatus;
import com.officelocator.util.ImageUtils;
import com.officelocator.util.StorePreferences;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, OnClickListener {

    public carbon.widget.ImageView img_c_menu;
    private NewFragmentDrawer drawerFragment;
    private DrawerLayout mDrawerLayout;
    private Boolean isHomeFragment = false;

    public ImageView img_topbar;

    public static String send_image = "";
    public static File finalFile;
    public static final int PICK_FROM_CAMERA = 1;
    public static final int PICK_FROM_FILE = 2;

    private ServiceStatus mServiceStatus;

    public static Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.activity_main);

        //============== Define a Custom Header for Navigation drawer=================//
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.actionbarlayout, null);
        mServiceStatus = new ServiceStatus(MainActivity.this);

        img_c_menu = (carbon.widget.ImageView) findViewById(R.id.img_c_menu);
        img_c_menu.setOnClickListener(this);
        img_topbar = (ImageView) findViewById(R.id.img_topbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerFragment = (NewFragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
        drawerFragment.setDrawerListener(this);

        displayView(0);

        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        break;
                    case 11:
                        break;
                    case 1:
                        break;

                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        userImageLogoRefresh();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
    }

    @Override
    public void onBackPressed() {
        Log.e("baxk stak count", getSupportFragmentManager().getBackStackEntryCount() + "=");
        Log.e("get_FragmentTag", get_FragmentTag() + "=");
        if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        } else if (get_FragmentTag().equals("new_main") || get_FragmentTag().equals("main")) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                super.onBackPressed();
            } else {
                if (!isHomeFragment) {
                    isHomeFragment = true;
                    loadHomeFragment(getString(R.string.title_dashboard));
                } else {
                    finish();
                }
            }
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onDrawerItemSelected(View view, final int position) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                displayView(position);
            }
        }, 400);
    }

    public Intent intent;
    public Pair<View, String>[] pairs;
    public ActivityOptionsCompat transitionActivityOptions;

    private void displayView(int position) {
        FragmentManager mFragmentManager = getSupportFragmentManager();

        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();

        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                //fragment = new CategoryFragment();
                //fragment = new DashboardFragment();
                title = getString(R.string.title_dashboard);
                loadHomeFragment(title);
                isHomeFragment = true;
                break;
            case 1:
//                fragment = new MyProfileFragment();
//                title = getString(R.string.title_my_profile);
//                isHomeFragment = false;
                intent = new Intent(MainActivity.this, MyProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;
                break;
            case 2:
//                fragment = new FavouiteFragment();
//                title = getString(R.string.title_saved_fav_list);
//                isHomeFragment = false;
                intent = new Intent(MainActivity.this, FavouriteActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;
                break;

            case 3:
//                fragment = new SearchFragment();
//                title = getString(R.string.title_search);
//                isHomeFragment = false;
                intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;
                break;
            case 4:
//                fragment = new SetttingsFragment();
//                title = getString(R.string.title_settings);
//                isHomeFragment = false;
                intent = new Intent(MainActivity.this, NewSettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;
                break;

            case 5:
                intent = new Intent(MainActivity.this, ContactUsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;
                break;
            case 6:
                intent = new Intent(MainActivity.this, AnnuncementListActivity.class);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;
                break;
            case 7:
                intent = new Intent(MainActivity.this, PrivacyPolicyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pairs = TransitionHelper.createSafeTransitionParticipants(MainActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this, pairs);
                MainActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
                fragment = null;
                isHomeFragment = false;

                break;
            case 8:
                String selected_lang = StorePreferences.getInstance().getLanguage();
                StorePreferences.getInstance().Clear_data();
                if (!StorePreferences.getInstance().getIsRememberMe()) {
                    StorePreferences.getInstance().setUserName("");
                    StorePreferences.getInstance().setUserPassword("");
                    StorePreferences.getInstance().setIsRememberMe(false);
                }
                if (mServiceStatus.isLocationUpdateServiceRunning()) {
                    stopService(new Intent(MainActivity.this, LocationUpdateService.class));
                }
                StorePreferences.getInstance().setLanguage(selected_lang);
                finish();
                Intent loginActivityIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginActivityIntent);

                isHomeFragment = false;
                break;
            default:
                break;
        }

        if (!isHomeFragment) {
            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment, "main");
                fragmentTransaction.addToBackStack("main");
                fragmentTransaction.commit();

                // set the toolbar title
//                getSupportActionBar().setTitle(title);
            }
        }
    }

    /**
     * Display Child Fragment
     *
     * @param fragment fragment to be replace
     * @param args     bundle argument if any
     */
    public void loadChildFragment(Fragment fragment, Bundle args, String tag) {
        isHomeFragment = false;
        if (fragment != null) {
            FragmentManager mFragmentManager = getSupportFragmentManager();
            if (args != null) {
                fragment.setArguments(args);
            }
            mFragmentManager.beginTransaction()
                    .replace(R.id.container_body, fragment, tag)
                    .addToBackStack(tag).commit();
        }
    }

    public void setActivityName(String title) {
//        actionbarTitle.setText(title);
    }

    public void fragmentBack() {
        Log.e("baxk stak count", getSupportFragmentManager().getBackStackEntryCount() + "=");
        Log.e("get_FragmentTag", get_FragmentTag() + "=");
        if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        } else if (get_FragmentTag().equals("new_main") || get_FragmentTag().equals("main")) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                super.onBackPressed();
            } else {
                if (!isHomeFragment) {
                    isHomeFragment = true;
                    loadHomeFragment(getString(R.string.title_dashboard));
                } else {
                    finish();
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    /**
     * For getting current fragment tag
     *
     * @return current fragment tag
     */
    public String get_FragmentTag() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = getSupportFragmentManager().getBackStackEntryAt(
                getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return tag;
    }

    /**
     * This function used for load default fragment when application is open
     */
    public void loadHomeFragment(String title) {
        try {
            CategoryFragment mCategoryFragment = new CategoryFragment();
            FragmentManager mFragmentManager = getSupportFragmentManager();
            FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
            if (mCategoryFragment != null) {
                mFragmentTransaction.replace(R.id.container_body, mCategoryFragment, "exit");
                mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mFragmentTransaction.commit();
            }

            // set the toolbar title
//            getSupportActionBar().setTitle(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void inVisibleBackButton() {
//        backButton.setVisibility(View.GONE);
    }

    public void visibleBackButton() {
//        backButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_FROM_CAMERA) {
                try {
                    //Log.i("&&&&&","call the set pic");
                    Log.i("File Image Path", MyProfileFragment.mCurrentPhotoPath + "==");
                    File capturedPic = new File(MyProfileFragment.mCurrentPhotoPath);
                    Bitmap mCapturedImageBitmap = ImageUtils.decodeFile(MyProfileFragment.mCurrentPhotoPath);
                    String mCapturedImageBase64 = ImageUtils.encodeTobase64(mCapturedImageBitmap);
                    send_image = mCapturedImageBase64;
                    ImageUtils.savebitmap(MyProfileFragment.mCurrentPhotoPath);
                    mCapturedImageBitmap.recycle();
                    mCapturedImageBase64 = null;
                    // ImageUtils.savebitmap(AuditATMCvr.mCurrentPhotoPath);
//                    imageBitmap = mBitmap;

//                    setPic();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == PICK_FROM_FILE) {

                Uri selected_ImageUri = null;
                if (data.getData() != null) {
                    selected_ImageUri = data.getData();
                    Log.d("selectedPath1 : ", selected_ImageUri + "=");
                } else {
                    Log.d("selectedPath1 : ", "Came here its null !");
                    //Toast.makeText(getActivity(), "failed to get Image!", Toast.LENGTH_LONG).show();
                }

                Bitmap bitmap = null;
                try {
                    bitmap = BitmapFactory.decodeStream(this.getContentResolver().openInputStream(selected_ImageUri));
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bao);
                    byte[] ba = bao.toByteArray();
                    send_image = Base64.encodeToString(ba, 1);
                    MyProfileFragment.mCurrentPhotoPath = getPath(selected_ImageUri);
                    finalFile = new File(getRealPathFromURI(selected_ImageUri));
                    Log.i("File Image Path", MyProfileFragment.mCurrentPhotoPath + "==");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*==========================get path from uri pass function Call=============================================*/
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public void userImageLogoRefresh() {
        if (drawerFragment != null)
            drawerFragment.updateUserImageLogo();
    }

    private void set_locale(String lan) {
        if (lan.equalsIgnoreCase("es-MX")) {
            lan = "mx";
        }
        Log.d("test", "Applies lang => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources()
                .updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_menu:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
                    mDrawerLayout.closeDrawer(GravityCompat.END);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.END);
                }
                break;
        }
    }
}
