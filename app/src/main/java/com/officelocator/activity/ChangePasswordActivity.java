package com.officelocator.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText newPasswordEditTextView;
    private ProgressDialog mProgressDialog;

    // Global variable declaration
    private String user_id;
    private String newPassword;
    private String Language = "";

    public static final String Password_PATTERN =
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";

    public Pattern pattern;
    public Matcher matcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getTempLanguage());
        setContentView(R.layout.new_change_password_activity);

        ((carbon.widget.TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);
        // Initialization
        mProgressDialog = new ProgressDialog(ChangePasswordActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        user_id = getIntent().getStringExtra("user_id");
        Language = getIntent().getStringExtra("Language");

        newPasswordEditTextView = (EditText) findViewById(R.id.newPasswordEditTextView);

        findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPassword = newPasswordEditTextView.getText().toString().trim();
                if (newPassword.equals("")) {
                    newPasswordEditTextView.setError(getResources().getString(R.string.password_local_validation_message));
                } else {
                    if (isValidPassword()) {
                        newPasswordEditTextView.setError(null);
                        if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                            checkUserSecurityAnswerAPICall();
                        } else {
                            ConnectionDetector.getInstance().show_alert(ChangePasswordActivity.this);
                        }
                    }
                }
            }
        });
    }

    private void checkUserSecurityAnswerAPICall() {
        mProgressDialog.show();
        SingletonRestClient.get().userChangePassword(user_id,
                newPassword, new Callback<Response>() {
                    @Override
                    public void failure(RestError restError) {
                        mProgressDialog.dismiss();
                    }

                    @Override
                    public void success(Response response,
                                        Response response2) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject obj_json = new JSONObject(
                                    new String(((TypedByteArray) response.getBody()).getBytes()));
                            Log.d("test", "success responseStatus => " + obj_json);
                            boolean responseStatus = obj_json.getBoolean("Status");
                            Log.d("test", "responseStatus => " + responseStatus);
                            if (responseStatus) {
                                StorePreferences.getInstance().set_UserId(user_id);
                                StorePreferences.getInstance().setUserPassword(newPassword);
                                StorePreferences.getInstance().setLanguage(Language);
                                StorePreferences.getInstance().setTempLanguage("");
                                //update local database user info
                                if (OfficeLocatorDatabase.getInstance(ChangePasswordActivity.this).IsUserInfoExist()) {
                                    OfficeLocatorDatabase.getInstance(ChangePasswordActivity.this).deleteAllUserInfo();
                                    OfficeLocatorDatabase.getInstance(ChangePasswordActivity.this).insertUserInfo(StorePreferences.getInstance().getUserName()
                                            , newPassword, user_id);
                                } else {
                                    OfficeLocatorDatabase.getInstance(ChangePasswordActivity.this).insertUserInfo(StorePreferences.getInstance().getUserName()
                                            , newPassword, user_id);
                                }

                                Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(ChangePasswordActivity.this, false,
                                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(ChangePasswordActivity.this, pairs);
                                ChangePasswordActivity.this.startActivity(intent, transitionActivityOptions.toBundle());

                                LoginActivity.finishActivity();
                                ForgotPasswordActivity.finishActivity();
                                SecurityQuestionValidationActivity.finishActivity();
                                finish();
                            } else {
                                String responseMessage = obj_json.getString("Message");
                                Toast.makeText(ChangePasswordActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //AlertDialog("Server Error! Please try again.");
                        }
                    }
                });
    }

    private boolean isValidPassword() {

        if (newPassword.length() < 6) {
            newPasswordEditTextView.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
            newPasswordEditTextView.requestFocus();
            return false;
        } else if (!isMatchPassword()) {
            newPasswordEditTextView.setError(getResources().getString(R.string.profile_select_password_local_validation_message));
            newPasswordEditTextView.requestFocus();
            return false;
        }
        return true;
    }

    private Boolean isMatchPassword() {
        pattern = Pattern.compile(Password_PATTERN);
        matcher = pattern.matcher(newPassword);

        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private void set_locale(String lan) {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources()
                .updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
