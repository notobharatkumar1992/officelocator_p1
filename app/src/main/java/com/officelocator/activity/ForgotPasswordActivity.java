package com.officelocator.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.officelocator.AppDelegate;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ForgotPasswordActivity extends AppCompatActivity {

    public static Activity mActivity;

    private LinearLayout userNameLinearLayout;
    private LinearLayout securityQuestionLinearLayout;
    private EditText userNameEditText;
    private ProgressDialog mProgressDialog;

    // Global variable declaration
    private String userName;
    private String id;
    private String question_id;
    private String question;
    private String securityAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.new_forgot_password_activity);
        mActivity = this;

        ((carbon.widget.TextView) findViewById(R.id.txt_c_createOneTextView)).setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

        // Initialization
        mProgressDialog = new ProgressDialog(ForgotPasswordActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);

        userNameLinearLayout = (LinearLayout) findViewById(R.id.userNameLinearLayout);
        securityQuestionLinearLayout = (LinearLayout) findViewById(R.id.securityQuestionLinearLayout);
        userNameEditText = (EditText) findViewById(R.id.userNameEditText);
        userNameEditText.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));

        findViewById(R.id.txt_c_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userName = userNameEditText.getText().toString().trim();
                if (userName.equals("")) {
                    AppDelegate.showToast(ForgotPasswordActivity.this, getResources().getString(R.string.profile_user_name_local_validation_message));
                } else {
                    if (ConnectionDetector.getInstance().isConnectingToInternet()) {
                        userForgetPasswordApiCall();
                    } else {
                        ConnectionDetector.getInstance().show_alert(ForgotPasswordActivity.this);
                    }
                }
            }
        });
    }

    private void userForgetPasswordApiCall() {
        mProgressDialog.show();
        SingletonRestClient.get().userForgetPassword(userName, StorePreferences.getInstance().getLanguage(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    Boolean responseStatus = obj_json.getBoolean("Status");
                    if (responseStatus) {
                        JSONObject list = new JSONObject(obj_json.getString("list"));
                        id = list.getString("id");
                        question_id = list.getString("question_id");
                        question = list.getString("question");
                        if (list.getString("lang") != null) {
                            StorePreferences.getInstance().setTempLanguage(list.getString("lang"));
                        }
                        StorePreferences.getInstance().setUserName(userName);
                        Log.i("id", id + "=");
                        Log.i("question_id", question_id + "=");
                        Log.i("question", question + "=");

                        Intent mainActivityIntent = new Intent(ForgotPasswordActivity.this, SecurityQuestionValidationActivity.class);
                        mainActivityIntent.putExtra("id", id);
                        mainActivityIntent.putExtra("question_id", question_id);
                        mainActivityIntent.putExtra("question", question);
                        mainActivityIntent.putExtra("Language", StorePreferences.getInstance().getTempLanguage());

                        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(ForgotPasswordActivity.this, false,
                                new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                                new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(ForgotPasswordActivity.this, pairs);
                        startActivity(mainActivityIntent, transitionActivityOptions.toBundle());
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(ForgotPasswordActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void set_locale(String lan) {
        Log.d("test", "set_locale => " + lan);
        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources()
                .updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    public static boolean finishActivity() {
        try {
            if (mActivity != null) {
                mActivity.finish();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            return false;
        }
    }
}
