package com.officelocator.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.officelocator.AppDelegate;
import com.officelocator.GCMClientManager;
import com.officelocator.R;
import com.officelocator.TransitionHelper;
import com.officelocator.database.OfficeLocatorDatabase;
import com.officelocator.net.Callback;
import com.officelocator.net.RestError;
import com.officelocator.net.SingletonRestClient;
import com.officelocator.service.LocationUpdateService;
import com.officelocator.service.ServiceStatus;
import com.officelocator.util.ConnectionDetector;
import com.officelocator.util.StorePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    public static Activity mActivity;

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // Control declaration
    private AutoCompleteTextView et_autocomplete_email;
    private EditText et_password;
    private View mProgressView;
    private View mLoginFormView;
    private TextView txt_c_createOneTextView, txt_c_forgotDetailButton;
    private ImageView img_c_check;

    private ProgressDialog mProgressDialog;

    // Util classes declaration
    private ConnectionDetector mConnectionDetector;
    private ServiceStatus mServiceStatus;

    // Global variable declaration
    private String userName;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().getDecorView().setSystemUiVisibility(/*View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | */
//                View.SYSTEM_UI_FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.bg_bottom);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        set_locale(StorePreferences.getInstance().getLanguage());
        setContentView(R.layout.activity_login);
        mActivity = this;

        //set_locale(StorePreferences.getInstance().getLanguage());
        // Initialization
        mConnectionDetector = ConnectionDetector.getInstance();
        mProgressDialog = new ProgressDialog(LoginActivity.this);
        mProgressDialog.setMessage(getResources().getString(R.string.progress_dialog_message));
        mProgressDialog.setCancelable(false);
        mServiceStatus = new ServiceStatus(LoginActivity.this);

        if (!StorePreferences.getInstance().get_UseId().equals("") && StorePreferences.getInstance().getIsRememberMe()) {

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginActivity.this, false,
                    new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                    new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
            ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginActivity.this, pairs);
            startActivity(intent, transitionActivityOptions.toBundle());
            finish();
        }

        // Set up the login form.
        et_autocomplete_email = (AutoCompleteTextView) findViewById(R.id.et_autocomplete_email);
        et_autocomplete_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));
        populateAutoComplete();

        et_password = (EditText) findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)));

        findViewById(R.id.txt_c_signin).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (local_validation()) {
                    if (mConnectionDetector.isConnectingToInternet()) {
                        LoginApiCall();
                    } else {
                        offlineLoginValidation();
                    }
                }
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        txt_c_forgotDetailButton = (TextView) findViewById(R.id.txt_c_forgotDetailButton);
        img_c_check = (ImageView) findViewById(R.id.img_c_check);
        txt_c_createOneTextView = (TextView) findViewById(R.id.txt_c_createOneTextView);
        txt_c_createOneTextView.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_name)), Typeface.BOLD_ITALIC);

//        txt_c_forgotDetailButton.setPaintFlags(txt_c_forgotDetailButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//        txt_c_createOneTextView.setPaintFlags(txt_c_createOneTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txt_c_forgotDetailButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginActivity.this, pairs);
                LoginActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
            }
        });

        txt_c_createOneTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginActivity.this, false,
                        new Pair<>(findViewById(R.id.img_topbar), getString(R.string.top_bar)),
                        new Pair<>(findViewById(R.id.imageView2), getString(R.string.map_with_logo)));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginActivity.this, pairs);
                LoginActivity.this.startActivity(intent, transitionActivityOptions.toBundle());
            }
        });
        img_c_check.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!img_c_check.isSelected()) {
                    img_c_check.setSelected(true);
                    StorePreferences.getInstance().setIsRememberMe(true);
                } else {
                    img_c_check.setSelected(false);
                    StorePreferences.getInstance().setIsRememberMe(false);
                }
            }
        });

        if (StorePreferences.getInstance().getIsRememberMe()) {
            et_autocomplete_email.setText(StorePreferences.getInstance().getUserName());
            et_password.setText(StorePreferences.getInstance().getUserPassword());
            img_c_check.setSelected(true);
        }
        LoginActivity.initGCM(this);
    }


    public static void initGCM(final Activity mActivity) {
        try {
            if (!AppDelegate.isValidString(StorePreferences.getInstance().get_GCM_RegistrationID())) {
                new GCMClientManager(mActivity).registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                    @Override
                    public void onSuccess(final String regId, boolean isNewRegistration) {
                        if (AppDelegate.isValidString(regId)) {
                            AppDelegate.LogGC("registrationId = " + regId + ", isNewRegistration = " + isNewRegistration);
                            StorePreferences.getInstance().set_GCM_RegistrationID(regId);
                        } else {
                            initGCM(mActivity);
                        }
                    }

                    @Override
                    public void onFailure(String ex) {
                        super.onFailure(ex);
                        AppDelegate.LogE("GCMClientManager onFailure = " + ex);
                    }
                });
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void LoginApiCall() {
        mProgressDialog.show();
        String device_id = StorePreferences.getInstance().get_GCM_RegistrationID();
        SingletonRestClient.get().userLogin(userName, password, device_id, "A", new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
                mProgressDialog.dismiss();
                //Toast.makeText(LoginActivity.this, "Invalid username or password", Toast.LENGTH_LONG).show();
            }

            @Override
            public void success(Response response,
                                Response response2) {
                mProgressDialog.dismiss();
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseCountry = obj_json.getString("response");
                    if (responseCountry.equals("1")) {
                        String userID = obj_json.getString("userID");
                        String email = obj_json.getString("email");
                        try {
                            if (AppDelegate.isValidString(obj_json.optString("logo"))) {
                                StorePreferences.getInstance().setUserProfileImage(obj_json.getString("logo"));
                            }
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                        try {
                            if (AppDelegate.isValidString(obj_json.optString("lang"))) {
                                StorePreferences.getInstance().setLanguage(obj_json.getString("lang"));
                            }
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                        StorePreferences.getInstance().set_UserId(userID);
                        StorePreferences.getInstance().setEmailId(email);
                        StorePreferences.getInstance().setUserName(userName);
                        StorePreferences.getInstance().setUserPassword(password);
                        //local database implement

                        /*if(StorePreferences.getInstance().getIsRememberMe()){
                            StorePreferences.getInstance().setUserPassword(password);
                        }*/

                        getFavoriteListApiCall();
                        //getUserProfileDataApiCall();

                        if (OfficeLocatorDatabase.getInstance(LoginActivity.this).IsUserInfoExist()) {
                            OfficeLocatorDatabase.getInstance(LoginActivity.this).deleteAllUserInfo();
                            OfficeLocatorDatabase.getInstance(LoginActivity.this).insertUserInfo(userName, password, userID);
                        } else {
                            OfficeLocatorDatabase.getInstance(LoginActivity.this).insertUserInfo(userName, password, userID);
                        }

                        if (!mServiceStatus.isLocationUpdateServiceRunning()) {
                            startService(new Intent(LoginActivity.this, LocationUpdateService.class));
                        }

                        Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(mainActivityIntent);
                        finish();
                    } else {
                        String responseMessage = obj_json.getString("Message");
                        Toast.makeText(LoginActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });

    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(et_autocomplete_email, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Called when user click on the Login Button for Local validation check
     *
     * @return true/false according to the validation criteria
     */
    private Boolean local_validation() {
        // TODO Auto-generated method stub
        userName = et_autocomplete_email.getText().toString();
        password = et_password.getText().toString();
        Boolean response = false;

        if (userName.equals("")) {
            et_autocomplete_email.setError(getResources().getString(R.string.login_user_name_local_validation_message));
            response = false;
        } else if (password.equals("")) {
            et_password.setError(getResources().getString(R.string.password_local_validation_message));
            response = false;
        } else {
            et_autocomplete_email.setError(null);
            et_password.setError(null);
            response = true;
        }
        return response;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        et_autocomplete_email.setError(null);
        et_password.setError(null);

        // Store values at the time of the login attempt.
        String email = et_autocomplete_email.getText().toString();
        String password = et_password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            et_password.setError(getString(R.string.error_invalid_password));
            focusView = et_password;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            et_autocomplete_email.setError(getString(R.string.error_field_required));
            focusView = et_autocomplete_email;
            cancel = true;
        } /*else if (!isEmailValid(email)) {
            et_autocomplete_email.setError(getString(R.string.error_invalid_email));
            focusView = et_autocomplete_email;
            cancel = true;
        }*/

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        et_autocomplete_email.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            /*for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            }*/

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(mainActivityIntent);
                finish();
            } else {
                et_password.setError(getString(R.string.error_incorrect_password));
                et_password.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void offlineLoginValidation() {

        if (OfficeLocatorDatabase.getInstance(LoginActivity.this).IsUserInfoExist()) {
            if (OfficeLocatorDatabase.getInstance(LoginActivity.this).IsValidUserExist(userName, password)) {
                String response = OfficeLocatorDatabase.getInstance(LoginActivity.this).GetUserInfo(userName, password);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String userID = jsonObject.getString("userId");
                    StorePreferences.getInstance().set_UserId(userID);

                    Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(mainActivityIntent);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.offline_login_response_message), Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.offline_login_response_message), Toast.LENGTH_LONG).show();
        }

    }

    private void getFavoriteListApiCall() {
        SingletonRestClient.get().getFavoriteList(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(Response response,
                                Response response2) {
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseStatus = obj_json.getString("Status");
                    if (responseStatus.equals("true")) {

                        if (OfficeLocatorDatabase.getInstance(LoginActivity.this).IsFavoriteListExist()) {
                            OfficeLocatorDatabase.getInstance(LoginActivity.this).deleteAllFavoriteList();
                            OfficeLocatorDatabase.getInstance(LoginActivity.this).insertFavoriteList(obj_json.toString());
                        } else {
                            OfficeLocatorDatabase.getInstance(LoginActivity.this).insertFavoriteList(obj_json.toString());
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void getUserProfileDataApiCall() {
        SingletonRestClient.get().getUserProfile(StorePreferences.getInstance().get_UseId(), new Callback<Response>() {
            @Override
            public void failure(RestError restError) {
            }

            @Override
            public void success(Response response,
                                Response response2) {
                try {
                    JSONObject obj_json = new JSONObject(
                            new String(((TypedByteArray) response.getBody()).getBytes()));
                    String responseCountry = obj_json.getString("response");
                    if (responseCountry.equals("1")) {
                        JSONObject output = obj_json.getJSONObject("output");
                        if (output.getString("logo") != null) {
                            StorePreferences.getInstance().setUserProfileImage(output.getString("logo"));
                        }
                    } else {
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //AlertDialog("Server Error! Please try again.");
                }
            }
        });
    }

    private void set_locale(String lan) {
        Log.d("test", "set_locale => " + lan);

        Locale locale = new Locale(lan);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources()
                .updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    public static boolean finishActivity() {
        try {
            if (mActivity != null) {
                mActivity.finish();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            return false;
        }
    }
}

