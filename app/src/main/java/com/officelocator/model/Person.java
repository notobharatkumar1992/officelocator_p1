/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.officelocator.model;

import android.graphics.drawable.Drawable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class Person implements ClusterItem {
    public final String id;
    public int value = 0;
    public final String name;
    public final Drawable profilePhoto;
    private final LatLng mPosition;

    public Person(LatLng position, String id, String name, Drawable pictureResource, int value) {
        this.id = id;
        this.name = name;
        profilePhoto = pictureResource;
        mPosition = position;
        this.value = value;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

}
