package com.officelocator.model;

public class Category {
    public int id;
    public String name;
    public String imageUrl;
    public String categoryId;

    public Category(int id, String name,String imageUrl,String CategoryId) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.categoryId = CategoryId;
    }

    public Category() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String CategoryId) {
        this.categoryId = CategoryId;
    }
}
