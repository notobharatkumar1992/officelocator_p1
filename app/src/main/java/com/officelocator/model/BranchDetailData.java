package com.officelocator.model;

import java.util.ArrayList;

public class BranchDetailData {

	public  class list
	{
		public String id;
		public ArrayList<product> product;
		public ArrayList<service> service;
		public class product
		{
			public String id;
			public String name;
			public String name_spanish;
			public String status;
			public String created;
			public String modified;
		}
		public class service
		{
			public String id;
			public String name;
			public String name_spanish;
			public String status;
			public String created;
			public String modified;
		}

		public String category_id;
		public String category_name;
		public String name;
		public String type;
		public String created;
		public String modified;
		public String status;
		public String logo;
		public String country;
		public String state;
		public String city;
		public String zip;
		public String address;
		public String address2;
		public String landmark;
		public String latitude;
		public String longitude;
		public String start_date;
		public String end_date;
		public String favorite;
		public String favorite_id;
		public String favorite_status;
		public String[] contact_no;
	}
}
