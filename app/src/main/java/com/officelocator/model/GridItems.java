package com.officelocator.model;

public class GridItems {
    public int id;
    public String title;
    public String imageUrl;
    public String categoryId;

    public GridItems(int id, String address,String imageUrl,String categoryId) {
        this.id = id;
        this.title = address;
        this.imageUrl = imageUrl;
        this.categoryId = categoryId;
    }
}
