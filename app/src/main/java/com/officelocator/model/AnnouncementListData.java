package com.officelocator.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class AnnouncementListData implements Parcelable {

    public ArrayList<list> list;
    public Boolean Status;
    public String Message;

    protected AnnouncementListData(Parcel in) {
        Message = in.readString();
    }

    public static final Creator<AnnouncementListData> CREATOR = new Creator<AnnouncementListData>() {
        @Override
        public AnnouncementListData createFromParcel(Parcel in) {
            return new AnnouncementListData(in);
        }

        @Override
        public AnnouncementListData[] newArray(int size) {
            return new AnnouncementListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Message);
    }

    public static class list implements Parcelable {
        public String id;
        public String name;
        public String comment;
        public String created;
        public String logo;
        public String thumb;

        protected list(Parcel in) {
            id = in.readString();
            name = in.readString();
            comment = in.readString();
            created = in.readString();
            logo = in.readString();
            thumb = in.readString();
        }

        public static final Creator<list> CREATOR = new Creator<list>() {
            @Override
            public list createFromParcel(Parcel in) {
                return new list(in);
            }

            @Override
            public list[] newArray(int size) {
                return new list[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(comment);
            dest.writeString(created);
            dest.writeString(logo);
            dest.writeString(thumb);
        }
    }
}
