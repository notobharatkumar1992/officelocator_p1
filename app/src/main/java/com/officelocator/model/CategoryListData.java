package com.officelocator.model;

import java.util.ArrayList;

public class CategoryListData {
    public ArrayList<list> list;

    public class list {
        public String id;
        public String name;
        public String url;
        public int value;
    }
}
