package com.officelocator.util;

import android.content.SharedPreferences;

import com.officelocator.AppDelegate;

public class StorePreferences {

    private static SharedPreferences pref_master;
    SharedPreferences.Editor editor_pref_master;

    private static StorePreferences mStore_Pref = new StorePreferences();

    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private StorePreferences() {
        pref_master = AppDelegate.getApplicationContext.getSharedPreferences("OfficeLocatorPreferences", 0);
    }

    /* Static 'instance' method */
    public static StorePreferences getInstance() {
        if (mStore_Pref == null) {
            mStore_Pref = new StorePreferences();
        }
        return mStore_Pref;
    }

    public void open_editor() {
        // TODO Auto-generated method stub
        editor_pref_master = pref_master.edit();
    }

    public String get_UseId() {
        return pref_master.getString("UserId", "");
    }

    public void set_UserId(String UserId) {
        open_editor();
        editor_pref_master.putString("UserId", UserId);
        editor_pref_master.commit();
    }

    public String getEmailId() {
        return pref_master.getString("EmailId", "");
    }

    public void setEmailId(String EmailId) {
        open_editor();
        editor_pref_master.putString("EmailId", EmailId);
        editor_pref_master.commit();
    }

    public String getUserName() {
        return pref_master.getString("UserName", "");
    }

    public void setUserName(String userName) {
        open_editor();
        editor_pref_master.putString("UserName", userName);
        editor_pref_master.commit();
    }

    public String getUserPassword() {
        return pref_master.getString("UserPassword", "");
    }

    public void setUserPassword(String UserPassword) {
        open_editor();
        editor_pref_master.putString("UserPassword", UserPassword);
        editor_pref_master.commit();
    }

    public String getUserProfileImage() {
        return pref_master.getString("ProfileImage", "");
    }

    public void setUserProfileImage(String ProfileImage) {
        open_editor();
        editor_pref_master.putString("ProfileImage", ProfileImage);
        editor_pref_master.commit();
    }

    public boolean getNotificationSound() {
        return pref_master.getBoolean("NotificationSound", true);
    }

    public void setNotificationSound(boolean b) {
        open_editor();
        editor_pref_master.putBoolean("NotificationSound", b);
        editor_pref_master.commit();
    }

    public boolean getPushNotificationShow() {
        return pref_master.getBoolean("PushNotification", true);
    }

    public void setPushNotificationShow(boolean b) {
        open_editor();
        editor_pref_master.putBoolean("PushNotification", b);
        editor_pref_master.commit();
    }

    public boolean getIsVibration() {
        return pref_master.getBoolean("Vibration", true);
    }

    public void setIsVibration(boolean b) {
        open_editor();
        editor_pref_master.putBoolean("Vibration", b);
        editor_pref_master.commit();
    }

    public String getRadius() {
        return pref_master.getString("radius", "");
    }

    public void setRadius(String radius) {
        open_editor();
        editor_pref_master.putString("radius", radius);
        editor_pref_master.commit();
    }

    public String getLatitude() {
        return pref_master.getString("latitude", "0.0");
    }

    public void setLatitude(String latitude) {
        open_editor();
        editor_pref_master.putString("latitude", latitude);
        editor_pref_master.commit();
    }

    public String getLongitude() {
        return pref_master.getString("longitude", "0.0");
    }

    public void setLongitude(String longitude) {
        open_editor();
        editor_pref_master.putString("longitude", longitude);
        editor_pref_master.commit();
    }

    public boolean getIsRememberMe() {
        return pref_master.getBoolean("RememberMe", false);
    }

    public void setIsRememberMe(boolean b) {
        open_editor();
        editor_pref_master.putBoolean("RememberMe", b);
        editor_pref_master.commit();
    }

    public String getLanguage() {
        String lang = pref_master.getString("language", "es");
//        Log.d("test", "getLanguage => " + lang);
        return lang;
    }

    public void setLanguage(String language) {
        open_editor();
        editor_pref_master.putString("language", language);
        editor_pref_master.commit();
    }

    public String get_GCM_RegistrationID() {
        return pref_master.getString("GCM_RegistrationID", "");
    }

    public void set_GCM_RegistrationID(String gCM_RegistrationID) {
        open_editor();
        editor_pref_master.putString("GCM_RegistrationID", gCM_RegistrationID);
        editor_pref_master.commit();
    }

    public void set_AppVersion(Integer AppVersion) {
        open_editor();
        editor_pref_master.putInt("AppVersion", AppVersion);
        editor_pref_master.commit();
    }

    public String getTempLanguage() {
        return pref_master.getString("TempLanguage", "es");
    }

    public void setTempLanguage(String TempLanguage) {
        open_editor();
        editor_pref_master.putString("TempLanguage", TempLanguage);
        editor_pref_master.commit();
    }

    public Integer get_AppVersion() {
        return pref_master.getInt("AppVersion", 0);
    }

    public void Clear_data() {
        open_editor();
        editor_pref_master.putString("UserId", "");
        editor_pref_master.putString("EmailId", "");
        editor_pref_master.putString("latitude", "");
        editor_pref_master.putString("longitude", "");
        editor_pref_master.putString("radius", "");
        editor_pref_master.putBoolean("PushNotification", true);
        editor_pref_master.putBoolean("NotificationSound", true);
        editor_pref_master.putBoolean("Vibration", true);
        editor_pref_master.commit();
    }
}
