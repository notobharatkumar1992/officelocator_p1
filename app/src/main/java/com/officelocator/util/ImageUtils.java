package com.officelocator.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Dev51 on 10/5/2015.
 */
public class ImageUtils {

//    /**
//     * Converts the passed in drawable to Bitmap representation
//     *
//     * @throws NullPointerException If the parameter drawable is null.
//     *                              **
//     */
//    public static Bitmap drawableToBitmap(Drawable drawable) {
//
//        if (drawable == null) {
//            throw new NullPointerException("Drawable to convert should NOT be null");
//        }
//
//        if (drawable instanceof BitmapDrawable) {
//            return ((BitmapDrawable) drawable).getBitmap();
//        }
//
//        if (drawable.getIntrinsicWidth() <= 0 && drawable.getIntrinsicHeight() <= 0) {
//            return null;
//        }
//
//        Bitmap bitmap = android.graphics.Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), android.graphics.Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//        drawable.draw(canvas);
//
//        return bitmap;
//    }
//
//    /**
//     * Converts the given bitmap to {@linkplain java.io.InputStream}.
//     *
//     * @throws NullPointerException If the parameter bitmap is null.
//     *                              **
//     */
//    public static InputStream bitmapToInputStream(Bitmap bitmap) throws NullPointerException {
//
//        if (bitmap == null) {
//            throw new NullPointerException("Bitmap cannot be null");
//        }
//
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, baos);
//        InputStream inputstream = new ByteArrayInputStream(baos.toByteArray());
//
//        return inputstream;
//    }
//
//    /**
//     * @deprecated Use {@link nl.changer.android.opensource.ImageUtils#scaleDownBitmap(android.content.Context, android.graphics.Bitmap, int)} instead.
//     * <p/>
//     * <br/>
//     * <br/>
//     * Scales the image depending upon the display density of the device.
//     * <p/>
//     * When dealing with the bitmaps of bigger size, this method must be called from a
//     * non-UI thread.
//     * **
//     */
//    public static Bitmap scaleDownBitmap(Context ctx, Bitmap source, int newHeight) {
//        final float densityMultiplier = getDensityMultiplier(ctx);
//
//        Log.v(TAG, "#scaleDownBitmap Original w: " + source.getWidth() + " h: " + source.getHeight());
//
//        int h = (int) (newHeight * densityMultiplier);
//        int w = (int) (h * source.getWidth() / ((double) source.getHeight()));
//
//        Log.v(TAG, "#scaleDownBitmap Computed w: " + w + " h: " + h);
//
//        Bitmap photo = android.graphics.Bitmap.createScaledBitmap(source, w, h, true);
//
//        Log.v(TAG, "#scaleDownBitmap Final w: " + w + " h: " + h);
//
//        return photo;
//    }
//
//    /**
//     * @deprecated Use {@link nl.changer.android.opensource.ImageUtils#scaleBitmap(android.content.Context, android.graphics.Bitmap, int)} instead.
//     * <p/>
//     * <br/>
//     * <br/>
//     * Scales the image independently of the screen density of the device.
//     * <p/>
//     * When dealing with the bitmaps of bigger size, this method must be called from a
//     * non-UI thread.
//     * **
//     */
//    public static Bitmap scaleBitmap(Context ctx, Bitmap source, int newHeight) {
//
//        Log.v(TAG, "#scaleDownBitmap Original w: " + source.getWidth() + " h: " + source.getHeight());
//
//        int w = (int) (newHeight * source.getWidth() / ((double) source.getHeight()));
//
//        Log.v(TAG, "#scaleDownBitmap Computed w: " + w + " h: " + newHeight);
//
//        Bitmap photo = android.graphics.Bitmap.createScaledBitmap(source, w, newHeight, true);
//
//        Log.v(TAG, "#scaleDownBitmap Final w: " + w + " h: " + newHeight);
//
//        return photo;
//    }
//
//    /**
//     * @param uri Uri of the source bitmap
//     *            **
//     * @deprecated Use {@link nl.changer.android.opensource.ImageUtils#scaleDownBitmap(android.content.Context, android.net.Uri, int)} instead.
//     * <p/>
//     * <br/>
//     * <br/>
//     * Scales the image independently of the screen density of the device.
//     */
//    public static Bitmap scaleDownBitmap(Context ctx, Uri uri, int newHeight) throws FileNotFoundException, IOException {
//        Bitmap original = MediaStore.Images.Media.getBitmap(ctx.getContentResolver(), uri);
//        return scaleBitmap(ctx, original, newHeight);
//    }
//
//    /**
//     * Gives the device independent constant which can be used for scaling images, manipulating view
//     * sizes and changing dimension and display pixels etc.
//     * **
//     */
//    public static float getDensityMultiplier(Context ctx) {
//        return ctx.getResources().getDisplayMetrics().density;
//    }
//
//    /**
//     * This method converts device specific pixels to density independent pixels.
//     *
//     * @param px      A value in px (pixels) unit. Which we need to convert into db
//     * @param context Context to get resources and device specific display metrics
//     * @return A int value to represent dp equivalent to px value
//     */
//    public static int getDip(int px, Context context) {
//        final float scale = context.getResources().getDisplayMetrics().density;
//        return (int) (px * scale + 0.5f);
//    }
//
//    /**
//     * @param mediaContentUri Content URI pointing to a row of {@link android.provider.MediaStore.Images.Media}
//     *                        **
//     * @deprecated Use {@link nl.changer.android.opensource.Utils#getImagePathForUri(android.content.Context, android.net.Uri)}
//     * <p/>
//     * <br/>
//     * <br/>
//     * <br/>
//     * <br/>
//     * Get the file path from the MediaStore.Images.Media Content URI
//     */
//    public static String getRealPathFromURI(Context ctx, Uri mediaContentUri) {
//
//        Cursor cur = null;
//        String path = null;
//
//        try {
//            String[] proj = {Media.DATA};
//            cur = ctx.getContentResolver().query(mediaContentUri, proj, null, null, null);
//
//            if (cur != null && cur.getCount() != 0) {
//                cur.moveToFirst();
//                path = cur.getString(cur.getColumnIndexOrThrow(MediaColumns.DATA));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (cur != null && !cur.isClosed())
//                cur.close();
//        }
//
//        return path;
//    }
//
//    /**
//     * @param mediaContentUri Media content Uri.
//     *                        **
//     * @deprecated Use {@link nl.changer.android.opensource.Utils#getPathForMediaUri(android.content.Context, android.net.Uri)}
//     * <p/>
//     * <br/>
//     * <br/>
//     * Get the file path from the Media Content Uri for video, audio or images.
//     */
//    public static String getImagePathForUri(Context context, Uri mediaContentUri) {
//
//        Cursor cur = null;
//        String path = null;
//
//        try {
//            String[] projection = {MediaColumns.DATA};
//            cur = context.getContentResolver().query(mediaContentUri, projection, null, null, null);
//
//            if (cur != null && cur.getCount() != 0) {
//                cur.moveToFirst();
//                path = cur.getString(cur.getColumnIndexOrThrow(MediaColumns.DATA));
//            }
//
//            // Log.v( TAG, "#getRealPathFromURI Path: " + path );
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (cur != null && !cur.isClosed())
//                cur.close();
//        }
//
//        return path;
//    }
//
//    /**
//     * Get the file path from the Media Content Uri for video, audio or images.
//     *
//     * @param mediaContentUri Media content Uri.
//     *                        **
//     */
//    public static String getPathForMediaUri(Context context, Uri mediaContentUri) {
//
//        Cursor cur = null;
//        String path = null;
//
//        try {
//            String[] projection = {MediaColumns.DATA};
//            cur = context.getContentResolver().query(mediaContentUri, projection, null, null, null);
//
//            if (cur != null && cur.getCount() != 0) {
//                cur.moveToFirst();
//                path = cur.getString(cur.getColumnIndexOrThrow(MediaColumns.DATA));
//            }
//
//            // Log.v( TAG, "#getRealPathFromURI Path: " + path );
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (cur != null && !cur.isClosed())
//                cur.close();
//        }
//
//        return path;
//    }
//
//    /**
//     * Writes the given image to the external storage of the device. If external storage is not
//     * available, the image is written to the application private directory
//     *
//     * @return Path of the image file that has been written.
//     * **
//     */
//    public static String writeImage(Context ctx, byte[] imageData) {
//
//        // TODO: move to MediaUtils
//        final String FILE_NAME = "photograph.jpeg";
//        File dir = null;
//        String filePath = null;
//        OutputStream imageFileOS;
//
//        dir = getStorageDirectory(ctx, null);
//
//        // dir.mkdirs();
//        File f = new File(dir, FILE_NAME);
//
//        // File f = getFile( FILE_NAME );
//
//        try {
//            imageFileOS = new FileOutputStream(f);
//            imageFileOS.write(imageData);
//            imageFileOS.flush();
//            imageFileOS.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        filePath = f.getAbsolutePath();
//
//        return filePath;
//    }
//
//    /**
//     * *
//     * Inserts an image into {@link android.provider.MediaStore.Images.Media} content provider of the device.
//     *
//     * @return The media content Uri to the newly created image, or null if the image failed to be
//     * stored for any reason.
//     * **
//     */
//    public static String writeImageToMedia(Context ctx, Bitmap image, String title, String description) {
//        // TODO: move to MediaUtils
//        if (ctx == null) {
//            throw new NullPointerException("Context cannot be null");
//        }
//
//        return Media.insertImage(ctx.getContentResolver(), image, title, description);
//    }
//
//
//    /**
//     * *
//     * Returns the URL without the query string
//     * **
//     */
//    public static URL getPathFromUrl(URL url) {
//
//        if (url != null) {
//            String urlStr = url.toString();
//            String urlWithoutQueryString = urlStr.split("\\?")[0];
//            try {
//                return new URL(urlWithoutQueryString);
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return null;
//    }
//
//    /**
//     * *
//     * Converts a given bitmap to byte array
//     * **
//     */
//    public static byte[] toBytes(Bitmap bmp) {
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        return stream.toByteArray();
//    }
//
//    * {@link nl.changer.android.opensource.Utils#decodeSampledBitmapFromResource(android.content.Context, android.net.Uri, int, int)} over this method.
//    *
//            * @param sourceBitmap Bitmap to be resized
//    * @param newWidth     Width of resized bitmap
//    * @param newHeight    Height of the resized bitmap
//    *                     **
//            */
//    public static Bitmap resizeImage(Bitmap sourceBitmap, int newWidth, int newHeight, boolean filter) {
//        // TODO: move this method to ImageUtils
//        if (sourceBitmap == null) {
//            throw new NullPointerException("Bitmap to be resized cannot be null");
//        }
//
//        Bitmap resized = null;
//
//        if (sourceBitmap.getWidth() < sourceBitmap.getHeight()) {
//            // image is portrait
//            resized = Bitmap.createScaledBitmap(sourceBitmap, newHeight, newWidth, true);
//        } else
//            // image is landscape
//            resized = Bitmap.createScaledBitmap(sourceBitmap, newWidth, newHeight, true);
//
//        resized = Bitmap.createScaledBitmap(sourceBitmap, newWidth, newHeight, true);
//
//        return resized;
//    }
//
//    /**
//     * *
//     * <p/>
//     * <br/>
//     * <br/>
//     *
//     * @param compressionFactor Powers of 2 are often faster/easier for the decoder to honor
//     *                          *
//     */
//    public static Bitmap compressImage(Bitmap sourceBitmap, int compressionFactor) {
//        BitmapFactory.Options opts = new BitmapFactory.Options();
//        opts.inPreferredConfig = Config.ARGB_8888;
//        opts.inSampleSize = compressionFactor;
//
//        if (Build.VERSION.SDK_INT >= 10) {
//            opts.inPreferQualityOverSpeed = true;
//        }
//
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        sourceBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] byteArray = stream.toByteArray();
//
//        Bitmap image = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, opts);
//
//        return image;
//    }
//
//    /**
//     * *
//     *
//     * @deprecated Too many image decoding methods.
//     * **
//     */
//    public static Bitmap decodeSampledBitmapFromResource(Context ctx, Uri uri, int reqWidth, int reqHeight) throws FileNotFoundException {
//        // TODO: move this method to ImageUtils
//        // First decode with inJustDecodeBounds=true to check dimensions
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//
//        try {
//            BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), new Rect(), options);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        // Calculate inSampleSize
//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
//
//        // Decode bitmap with inSampleSize set
//        options.inJustDecodeBounds = false;
//
//        try {
//            return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), new Rect(), options);
//        } catch (FileNotFoundException e) {
//            throw new FileNotFoundException(e.getMessage());
//        }
//    }
//
//    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        // Raw height and width of image
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//
//            // Calculate ratios of height and width to requested height and
//            // width
//            final int heightRatio = Math.round((float) height / (float) reqHeight);
//            final int widthRatio = Math.round((float) width / (float) reqWidth);
//
//            // Choose the smallest ratio as inSampleSize value, this will
//            // guarantee
//            // a final image with both dimensions larger than or equal to the
//            // requested height and width.
//            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
//        }
//
//        return inSampleSize;
//    }
//
//    /**
//     * Provide the height to which the sourceImage is to be resized. This method will calculate the
//     * resultant height. Use scaleDownBitmap from {@link nl.changer.android.opensource.Utils} wherever possible
//     * *
//     */
//    public Bitmap resizeImageByHeight(int height, Bitmap sourceImage) {
//        // TODO: move this method to ImageUtils
//        int widthO = 0; // original width
//        int heightO = 0; // original height
//        int widthNew = 0;
//        int heightNew = 0;
//
//        widthO = sourceImage.getWidth();
//        heightO = sourceImage.getHeight();
//        heightNew = height;
//
//        // Maintain the aspect ratio
//        // of the original banner image.
//        widthNew = (heightNew * widthO) / heightO;
//
//        return Bitmap.createScaledBitmap(sourceImage, widthNew, heightNew, true);
//    }
//
//    /**
//     * Checks if the url is valid
//     * **
//     */
//    public static boolean isValidURL(String url) {
//
//        URL u = null;
//
//        try {
//            u = new URL(url);
//        } catch (MalformedURLException e) {
//            return false;
//        }
//        try {
//            u.toURI();
//        } catch (URISyntaxException e) {
//            return false;
//        }
//
//        return true;
//    }
//
//
//    /**
//     * *
//     * Get the media data from the one of the following media {@link android.content.ContentProvider} This method
//     * should not be called from the main thread of the application.
//     * <ul>
//     * <li>{@link android.provider.MediaStore.Images.Media}</li>
//     * <li>{@link android.provider.MediaStore.Audio.Media}</li>
//     * <li>{@link android.provider.MediaStore.Video.Media}</li>
//     * </ul>
//     *
//     * @param ctx Context object
//     * @param uri Media content uri of the image, audio or video resource
//     */
//    public static byte[] getMediaData(Context ctx, Uri uri) {
//        // TODO: move to MediaUtils
//        if (uri == null) {
//            throw new NullPointerException("Uri cannot be null");
//        }
//
//        Cursor cur = ctx.getContentResolver().query(uri, new String[]{Media.DATA}, null, null, null);
//        byte[] data = null;
//
//        try {
//            if (cur != null && cur.getCount() > 0) {
//                if (cur.moveToNext()) {
//                    String path = cur.getString(cur.getColumnIndex(Media.DATA));
//
//                    try {
//                        File f = new File(path);
//                        FileInputStream fis = new FileInputStream(f);
//                        data = NetworkManager.readStreamToBytes(fis);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    // Log.v( TAG, "#getVideoData byte.size: " + data.length );
//                } // end while
//            } else
//                Log.e(TAG, "#getMediaData cur is null or blank");
//        } finally {
//            if (cur != null && !cur.isClosed()) {
//                cur.close();
//            }
//        }
//
//        return data;
//    }
//
//    /**
//     * @param mediaUri uri to the media resource. For e.g. content://media/external/images/media/45490 or
//     *                 content://media/external/video/media/45490
//     * @return Size in bytes, -1 if error
//     * @deprecated Use {@link nl.changer.android.opensource.MediaUtils#getMediaSize(android.content.Context, android.net.Uri)}
//     * <p/>
//     * <br/><br/>
//     * Gets the size of the media resource pointed to by the paramter mediaUri.
//     */
//    public static long getMediaSize(Context ctx, Uri mediaUri) {
//        Cursor cur = ctx.getContentResolver().query(mediaUri, new String[]{Media.DATA}, null, null, null);
//        long size = -1;
//
//        try {
//            if (cur != null && cur.getCount() > 0) {
//                while (cur.moveToNext()) {
//                    // size = cur.getLong(cur.getColumnIndex(Media.DATA));
//                    String path = cur.getString(cur.getColumnIndex(Media.DATA));
//                    File f = new File(path);
//
//                    size = f.length();
//                    // for unknown reason, the image size for image was found to be 0
//                    // Log.v( TAG, "#getSize byte.size: " + size );
//
//                    if (size == 0) {
//                        DebugLog.w(" The media size was found to be 0. Reason: UNKNOWN");
//                    }
//                } // end while
//            } else if (cur.getCount() == 0) {
//                DebugLog.e(" cur size is 0. File may not exist");
//            } else
//                DebugLog.e(" cur is null");
//        } finally {
//            if (cur != null && !cur.isClosed()) {
//                cur.close();
//            }
//        }
//
//        return size;
//    }
//
//    /**
//     * @deprecated Use {@link nl.changer.android.opensource.MediaUtils#getType(android.net.Uri)}
//     * <p/>
//     * Gets media type from the Uri.
//     */
//    public static String getMediaType(Context ctx, Uri uri) {
//        if (uri == null) {
//            throw new NullPointerException("Uri cannot be null");
//        }
//
//        String uriStr = uri.toString();
//
//        if (uriStr.contains("video")) {
//            return "video";
//        } else if (uriStr.contains("audio")) {
//            return "audio";
//        } else if (uriStr.contains("image")) {
//            return "image";
//        } else {
//            return null;
//        }
//    }
//
//    /*
//     * public static String getHash(String value, String key) throws
//	 * UnsupportedEncodingException, NoSuchAlgorithmException,
//	 * InvalidKeyException { String type = "HmacSHA1"; SecretKeySpec secret =
//	 * new SecretKeySpec(key.getBytes(), type); Mac mac = Mac.getInstance(type);
//	 * mac.init(secret); byte[] bytes = mac.doFinal(value.getBytes()); return
//	 * bytesToHex(bytes); }
//	 *
//	 * private final static char[] hexArray = "0123456789abcdef".toCharArray();
//	 *
//	 * private static String bytesToHex(byte[] bytes) { char[] hexChars = new
//	 * char[bytes.length * 2]; int v; for (int j = 0; j < bytes.length; j++) { v
//	 * = bytes[j] & 0xFF; hexChars[j * 2] = hexArray[v >>> 4]; hexChars[j * 2 +
//	 * 1] = hexArray[v & 0x0F]; } return new String(hexChars); }
//	 */
//
//    /**
//     * Creates the uri to a file located on external storage or application internal storage.
//     * **
//     */
////    public static Uri createUri(Context ctx) {
////        File root = ctx.getStorageDirectory(ctx, null);
////        root.mkdirs();
////        File file = new File(root, Long.toString(new Date().getTime()));
////        Uri uri = Uri.fromFile(file);
////
////        return uri;
////    }
//
//    public static final boolean isRelativeUrl(String url) {
//
//        if (TextUtils.isEmpty(url)) {
//            throw new NullPointerException("Url cannot be null");
//        }
//
//        Uri uri = Uri.parse(url);
//
//        return uri.getScheme() == null;
//    }
//
//    /**
//     * Checks if the parameter {@link android.net.Uri} is a content uri.
//     * **
//     */
//    public static boolean isContentUri(Uri uri) {
//        if (!uri.toString().contains("content://")) {
//            Log.w(TAG, "#isContentUri The uri is not a media content uri");
//            return false;
//        } else {
//            return true;
//        }
//    }
//
//

    /**
     *  Used to encode bitmap to base64 string
     * @param image
     * @return
     */
    public static String encodeTobase64(Bitmap image)
    {
        String imageEncoded=null;
        try {
            Bitmap immagex=image;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

            Log.e("LOOK", imageEncoded);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return imageEncoded;
    }

    /**
     * Used for decode base64 string to Bitmap
     * @param input
     * @return
     */
    public static Bitmap decodeBase64(String input)
    {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

//    public static Bitmap decodeFile(File f){
//        Bitmap b = null;
//        int IMAGE_MAX_SIZE=80;
//
//        try {
//            //Decode image size
//            BitmapFactory.Options o = new BitmapFactory.Options();
//            o.inJustDecodeBounds = true;
//
//            FileInputStream fis = new FileInputStream(f);
//            BitmapFactory.decodeStream(fis, null, o);
//            fis.close();
//
//            int scale = 1;
//            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
//                scale = (int)Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
//                        (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
//            }
//
//            //Decode with inSampleSize
//            BitmapFactory.Options o2 = new BitmapFactory.Options();
//            o2.inSampleSize = scale;
//            fis = new FileInputStream(f);
//            b = BitmapFactory.decodeStream(fis, null, o2);
//            fis.close();
//        }catch (Exception e)
//        {
//            Log.e("decode bitmap err",e+"");
//            e.printStackTrace();
//            return null;
//        }
//        return b;
//    }

    /**
     * This function is used to get rotated image bitmap from filepath
     * @param filePath
     * @return
     */
    public static Bitmap decodeFile(String filePath){
        File mImageFile = new File(filePath);
        Bitmap b = null;
        int IMAGE_MAX_SIZE=150;

        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(mImageFile);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int)Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                        (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(mImageFile);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

        }catch (Exception e)
        {
            Log.e("decode bitmap err",e+"");
            e.printStackTrace();
            return null;
        }
        return ImageUtils.rotateImage(b);
    }

    /**
     * This function is used to get rotated image bitmap from filepath
     * @param filePath
     * @return
     */
    public static Bitmap decodeCapturedPicture(String filePath){
        File mImageFile = new File(filePath);
        Bitmap b = null;
        int IMAGE_MAX_SIZE= 1280;

        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(mImageFile);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int)Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(mImageFile);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

        }catch (Exception e)
        {
            Log.e("decode bitmap err",e+"");
            e.printStackTrace();
            return null;
        }
        //return ImageUtils.rotateImage(b);
        return  b;
    }

    public static Bitmap rotateImage(Bitmap bitmap) {
        if (bitmap != null) {
            Matrix matrix = new Matrix();
            //matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 80, 80, true);
            Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            return rotatedBitmap;
        }
        return bitmap;
    }

    public static File savebitmap(String filePath) {

        OutputStream outStream = null;
        Bitmap bitmap = null;
        if(!filePath.equals(""))
            bitmap = decodeCapturedPicture(filePath);
        else
            return null;

        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
            file = new File(filePath);
            Log.e("file exist", "" + file + ",Bitmap= " + filePath);
        }
        try {

            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            bitmap.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("file created", "" + file);
        return file;

    }

}
