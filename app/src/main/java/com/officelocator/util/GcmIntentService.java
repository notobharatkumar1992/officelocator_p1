package com.officelocator.util;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.officelocator.R;
import com.officelocator.activity.MainActivity;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {

    public static int count = 0;
    public static final String TAG = "GCMIntentService";
    public String message="", title ="",subtitle="",tickerText="";

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle

            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                //sendNotification("Send error: " + extras.toString());
               /* if(gcm.getMessageType(intent).equals(ERROR_SERVICE_NOT_AVAILABLE))
                {}*/
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                //sendNotification("Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                try {
                    Log.e("GCM received", extras.toString()+"=");

                    /**
                     * Check that intent has the extra message in it
                     */
                    if (intent.hasExtra("message")) {
                        Log.e("GCM received", "GCM");

                        /**
                         * Here we need to check that user enable the notification or not
                         * if enable then and then we can proceed further otherwise
                         * we need to ignore the notification
                         */
                        if (StorePreferences.getInstance().getPushNotificationShow()) {
                            WakeLocker.acquire(getBaseContext());
                            createNotification(this, intent.getExtras().getString("message"),
                                    intent.getExtras().getString("tickerText"),
                                    intent.getExtras().getString("subtitle"),
                                    intent.getExtras().getString("title"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            Log.e("message", "==");
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    /**
     * To play sound when notification created
     *
     * @param id is the reference for notification sound file stored in raw folder
     */
    private void play_sound(int id) {
        MediaPlayer mp1 = MediaPlayer.create(this, id);
        if (mp1.isPlaying()) {
            mp1.reset();
            mp1 = MediaPlayer.create(this, id);
        }
        mp1.start();
    }

    private void createNotification(Context context, String message, String tickerText, String subtitle, String title) {
        Intent ii = new Intent(context, MainActivity.class);
        ii.setData((Uri.parse("custom://" + System.currentTimeMillis())));
        ii.setAction("actionstring" + System.currentTimeMillis());
        ii.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(context, 0, ii, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification mNotification;
        NotificationCompat.Builder mSoundNotificationBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentIntent(pi)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true);

        NotificationCompat.Builder mNoSoundNotificationBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentIntent(pi)
                        .setAutoCancel(true);

        if (StorePreferences.getInstance().getNotificationSound())
            mNotification = mSoundNotificationBuilder.build();
        else
            mNotification = mNoSoundNotificationBuilder.build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
       // mNotification.setLatestEventInfo(context, title, message, pi);
        notificationManager.notify(count, mNotification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy " + "GCMintent service");
    }

    /**
     * This function will create an intent. This intent must take as parameter the "unique_name"
     * that you registered your activity with
     *
     * @param context current activity context
     * @param message message received from the GCM server
     */
    static void updateMyActivity(Context context, String message) {
        Intent intent = new Intent("updateList");
        //put whatever data you want to send, if any
        intent.putExtra("message", message);
        //send broadcast
        context.sendBroadcast(intent);
    }
}
