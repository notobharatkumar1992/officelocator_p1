package com.officelocator.service;

import android.app.ActivityManager;
import android.content.Context;

/**
 * Created by Dev51 on 3/5/2015.
 */
public class ServiceStatus {

    private Context _context;

    public ServiceStatus(Context context){
        this._context = context;
    }

    /**
     * Check if LocationUpdateService is running or not
     * If not running
     * Start location update service
     */
    public boolean isLocationUpdateServiceRunning() {
        ActivityManager manager = (ActivityManager)_context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (LocationUpdateService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
